/* read a text file example. note that the program has no
   hardcoded limit on the lenght of the file */

CONST MAXLINELEN=1000

PROC main()
  DEF fh,buf[MAXLINELEN]:ARRAY,n=0,last=NIL,s,first=NIL
  IF fh:=Open(arg,OLDFILE)
    WHILE Fgets(fh,buf,MAXLINELEN)
      IF (s:=String(StrLen(buf)))=NIL THEN Raise("MEM")
      StrCopy(s,buf,ALL)
      IF last THEN Link(last,s) ELSE first:=s
      last:=s
      INC n
    ENDWHILE
    Close(fh)
    WriteF('FILE: "\s", \d lines.\n\n',arg,n)
    s:=first
    WHILE s
      PutStr(s)
      s:=Next(s)
    ENDWHILE
    DisposeLink(first)
  ELSE
    WriteF('no file: "\s"\n',arg)
  ENDIF
ENDPROC
