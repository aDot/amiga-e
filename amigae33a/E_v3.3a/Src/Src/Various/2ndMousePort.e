-> switches mouseports

OPT OSVERSION=37

MODULE 'devices/input', 'exec/io'

PROC main()
  DEF request=NIL:PTR TO iostd,port=NIL
  IF port:=CreateMsgPort()
    IF request:=CreateIORequest(port,SIZEOF iostd)
      IF OpenDevice('input.device',0,request,0)=0
        request.command:=IND_SETMPORT
        request.data:=[1]:CHAR          -> 0 for original port, 1 for joyport
        request.length:=1
        DoIO(request)
        CloseDevice(request)
      ELSE
        PutStr('Could not open input device\n')
      ENDIF
      DeleteIORequest(request)
    ELSE
      PutStr('Could not create iorequest\n')
    ENDIF
    DeleteMsgPort(port)    
  ELSE
    PutStr('Could not open port\n')
  ENDIF
ENDPROC
