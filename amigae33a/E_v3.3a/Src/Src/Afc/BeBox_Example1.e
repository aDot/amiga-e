/*

  $VER: BeBox Example 1 - (C)Copyright Amiga Foundation Classes

  Written By:   Fabio Rotondo

  This example just shows some features of the BeBox class.

  Please, note the "mydraw" function.

*/

MODULE 'afc/explain_exception', 'afc/bebox',
       'intuition/intuition', 'intuition/screens'

PROC main() HANDLE
  DEF bb=NIL:PTR TO bebox
  DEF scr=NIL:PTR TO screen
  DEF win=NIL:PTR TO window

  NEW bb.bebox()    -> Class initialization



  IF (scr:=LockPubScreen('Workbench'))=NIL THEN Raise("scr")

  IF (win:=OpenWindowTagList(NIL,
                  [WA_LEFT,    0,
                   WA_TOP,     0,
                   WA_WIDTH, scr.width,
                   WA_HEIGHT, scr.height,
                   WA_FLAGS, WFLG_BORDERLESS OR WFLG_BACKDROP OR WFLG_NOCAREREFRESH,
->                   WA_CUSTOMSCREEN, scr,
                   0,0]))=NIL THEN Raise("win")


  bb.setattrs([BB_POSX,    10,
               BB_POSY,    10,
               BB_WIDTH,  100,
               BB_HEIGHT, 100,
               BB_RPORT,  scr.rastport,
               BB_MAXX,   scr.width-1,
               BB_MAXY,   scr.height-1,
               BB_USEREL, TRUE,
               BB_DRAW,   {mydraw},
               0,0])




  bb.draw() -> The first draw on the window.

  LOOP

    REPEAT
      Delay(3)
      IF Mouse()=2 THEN Raise("HALT")
    UNTIL (Mouse()=1 AND bb.check(MouseX(win), MouseY(win)))

    WHILE (Mouse()=1)
      bb.setattrs([BB_POSX, MouseX(win), BB_POSY, MouseY(win), 0,0])
      bb.draw()
    ENDWHILE

  ENDLOOP

  bb.draw(FALSE)

/*
  bb.setattrs([BB_MAXX,   100,
               BB_MAXY,   100,
               BB_WIDTH,   80,
               BB_HEIGHT,  10,
               BB_POSX,    50,
               BB_POSY,     0,
               NIL, NIL])
*/

EXCEPT DO
  END bb

  IF win THEN CloseWindow(win)
  IF scr THEN UnlockPubScreen (NIL, scr)

  IF exception<>"HALT" THEN explain_exception()
ENDPROC

PROC mydraw(data:PTR TO bebox_data, mode)
  SetDrMd(data.rp, 2)

  IF mode THEN IF (data.oldx=data.x) AND (data.oldy = data.y) THEN RETURN

  IF data.oldx<>-1
    Move(data.rp, data.oldx, data.oldy)
    SetAPen(data.rp, data.oldcol)

    Draw(data.rp, data.oldx + data.oldw, data.oldy)
    Draw(data.rp, data.oldx + data.oldw, data.oldy + data.oldh)
    Draw(data.rp, data.oldx, data.oldy + data.oldh)
    Draw(data.rp, data.oldx, data.oldy)

    Draw(data.rp, data.oldx + data.oldw, data.oldy + data.oldh)
    Move(data.rp, data.oldx + data.oldw, data.oldy)
    Draw(data.rp, data.oldx, data.oldy + data.oldh)
    data.oldx:=-1
  ENDIF

  IF mode
    Move(data.rp, data.x, data.y)
    SetAPen(data.rp, data.col)
    Draw(data.rp, data.x + data.w, data.y)
    Draw(data.rp, data.x + data.w, data.y + data.h)
    Draw(data.rp, data.x, data.y + data.h)
    Draw(data.rp, data.x, data.y)

    Draw(data.rp, data.x + data.w, data.y + data.h)
    Move(data.rp, data.x + data.w, data.y)
    Draw(data.rp, data.x, data.y + data.h)

  ENDIF

  SetDrMd(data.rp, 0)

ENDPROC

