/*

   $VER: Rexxer Sender Example 1.0

   Written By Fabio Rotondo

   (C)Copyright Amiga Foundation Classes

   NOTES: Run this program AFTER you have run 'Rexxer Receiver'.

          This program will communicate using ARexx with 'RECEIVE.1'.
          If the "RECEIVE.1" port is not found, the program will raise
          an exception.

*/

MODULE 'afc/rexxer', 'afc/explain_exception'

PROC main() HANDLE
  DEF rx=NIL:PTR TO rexxer


  IF FindPort('RECEIVE.1')=NIL THEN Raise("recv")
  NEW rx.rexxer('SENDER')

  rx.send('RECEIVE.1', 'QUIT')

  WriteF('RC:\d - Result:\s\n', rx.rc(), rx.result())
  WriteF('ARexx Sender port terminating...\n')
EXCEPT DO
  explain_exception()
  END rx
  CleanUp(0)
ENDPROC

