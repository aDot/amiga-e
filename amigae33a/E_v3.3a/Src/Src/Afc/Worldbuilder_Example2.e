
MODULE 'AFC/worldbuilder',
       'AFC/bitmapper',
       'AFC/explain_exception',
       'intuition/intuition',
       'intuition/screens',
       'graphics/view',
       'exec/ports'


PROC main() HANDLE
  DEF scene:PTR TO worldbuilder, scr:PTR TO screen, win:PTR TO window
  DEF vp:PTR TO viewport, ri:PTR TO rasinfo, scrbm, bm2:PTR TO bitmapper
  DEF rp2, bitmap2, port:PTR TO mp, usebm=0, rp
  DEF rrx, rry, rrz, oox, ooy, ooz

  NEW scene.worldbuilder()

  scene.addobj('A')
  scene.addobj('N')
  scene.addobj('D')
  scene.addobj('R')
  scene.addobj('E')
  scene.addobj('A2')


  scene.fastsetvert('A',[-10,0,0, -20,0,0, -20,60,0, 20,60,0, 20,0,0,
                          10,0,0, 10,30,0, -10,30,0, -10,50,0, 10,50,0,
                          10,40,0, -10,40,0]:vertex_obj)

  scene.setsurface('A',0,[0,1,2,3,4,5,6,7],3)
  scene.setsurface('A',1,[8,9,10,11],0)
  scene.setsurface('A',2,[7,6,5,4,3,2,1,0],3)
  scene.setsurface('A',3,[11,10,9,8],0)

  scene.fastsetvert('N',[-20,0,0, -20,60,0, -10,60,0, 10,10,0, 10,60,0,
                         20,60,0, 20,0,0, 10,0,0, -10,50,0, -10,0,0]:vertex_obj)

  scene.setsurface('N',0,[0,1,2,3,4,5,6,7,8,9],3)
  scene.setsurface('N',1,[9,8,7,6,5,4,3,2,1,0],3)


  scene.fastsetvert('D',[-20,0,0, -20,60,0, 10,60,0, 20,50,0, 20,10,0,
                         10,0,0, -10,10,0, -10,50,0, 0,50,0, 10,40,0,
                         10,20,0, 0,10,0]:vertex_obj)

  scene.setsurface('D',0,[0,1,2,3,4,5],3)
  scene.setsurface('D',1,[6,7,8,9,10,11],0)
  scene.setsurface('D',2,[5,4,3,2,1,0],3)
  scene.setsurface('D',3,[11,10,9,8,7,6],0)


  scene.fastsetvert('R',[-20,0,0, -20,60,0, 10,60,0, 20,50,0, 20,40,0,
                         10,30,0, 20,0,0, 10,0,0, 0,30,0, -10,30,0,
                         -10,0,0, -10,40,0, -10,50,0, 0,50,0, 0,40,0]:vertex_obj)

  scene.setsurface('R',0,[0,1,2,3,4,5,6,7,8,9,10],3)
  scene.setsurface('R',1,[11,12,13,14],0)
  scene.setsurface('R',2,[10,9,8,7,6,5,4,3,2,1,0],3)
  scene.setsurface('R',3,[14,13,12,11],0)


  scene.fastsetvert('E',[-20,0,0, -20,60,0, 20,60,0, 20,50,0, -10,50,0,
                         -10,40,0, 0,40,0, 0,30,0, -10,30,0, -10,10,0,
                         20,10,0, 20,0,0]:vertex_obj)

  scene.setsurface('E',0,[0,1,2,3,4,5,6,7,8,9,10,11],3)
  scene.setsurface('E',1,[11,10,9,8,7,6,5,4,3,2,1,0],3)

  scene.copyobj('A','A2')

  scene.position('A',-140,-30,0)
  scene.position('N',-90,-30,0)
  scene.position('D',-40,-30,0)
  scene.position('R',10,-30,0)
  scene.position('E',60,-30,0)
  scene.position('A2',110,-30,0)

  scene.applyobj('A')
  scene.applyobj('N')
  scene.applyobj('D')
  scene.applyobj('R')
  scene.applyobj('E')
  scene.applyobj('A2')

  scene.setdisplay(320,256) -> display dimensions
  scene.init3D() -> build system structures FOR filled vectors

  scene.setaftertrasl(160,128) -> origin OF the screen axes.
->  scene.setafterscale(200,200) -> scaling AFTER projection (TO fit in
                               -> the screen): 200 = double.
  scene.setobserver(0,0,-200)  -> position OF the observer

  -> we open a screen
  scr:=OpenScreenTagList(NIL,[SA_TOP,0,
                              SA_LEFT,0,
                              SA_WIDTH,320,
                              SA_HEIGHT,256,
                              SA_DEPTH,3,
                              SA_TITLE,'Routines3D',
                              SA_SHOWTITLE,FALSE,
                              0,0])
  IF scr=NIL THEN Raise("SCR")
  vp:=scr.viewport
  ri:=vp.rasinfo    -> we get the rasinfo structure FOR double buffering
  scrbm:=ri.bitmap

  -> a trivial palette
  SetRGB4(vp,3,15,0,0)
  SetRGB4(vp,4,0,15,0)
  SetRGB4(vp,5,0,0,15)
  SetRGB4(vp,7,12,12,0)

  -> we allocate a second bitmap FOR the double buffering
  NEW bm2.bitmapper()
  bm2.allocbitmap(320,256,3,TRUE)
  rp2:=bm2.rastport()
  bitmap2:=bm2.bitmap()

  -> the window IS needed TO get keypresses
  win:=OpenWindowTagList(NIL,[WA_TOP,0,
                              WA_LEFT,0,
                              WA_WIDTH,320,
                              WA_HEIGHT,256,
                              WA_TITLE,'Routines3D',
                              WA_FLAGS,WFLG_ACTIVATE OR WFLG_SMART_REFRESH OR WFLG_BACKDROP OR WFLG_BORDERLESS,
                              WA_IDCMP,IDCMP_RAWKEY,
                              WA_CUSTOMSCREEN,scr,
                              0,0])
  IF win=NIL THEN Raise("WIN")
  rp:=win.rport
  port:=win.userport

  oox:=0 ; ooy:=0 ; ooz:=-200  -> observer initial position
  rrx:=0 ; rry:=0 ; rrz:=0  -> angles OF rotation

  ClearScreen(rp)
  scene.projection()  -> project
  scene.drawfill(rp)  -> AND draw the scene
  usebm:=1-usebm

  FOR rry:=0 TO 359
    scene.setrot(0,rry,0)
    scene.projection()
    SetAPen(IF usebm=0 THEN rp ELSE rp2, 0)
    RectFill(IF usebm=0 THEN rp ELSE rp2, 0,30,320,200)
    scene.drawfill(IF usebm=0 THEN rp ELSE rp2)

    IF usebm=0
      ri.bitmap:=scrbm
    ELSE
      ri.bitmap:=bitmap2
    ENDIF
    WaitTOF()
    ScrollVPort(vp)
    usebm:=1-usebm
  ENDFOR

  REPEAT
    Delay(5)
  UNTIL Mouse()

EXCEPT DO
  IF win THEN CloseWindow(win)
  IF scr
    ri.bitmap:=scrbm
    CloseScreen(scr)
  ENDIF
  IF scene THEN END scene  -> remember TO free ALL memory
  IF bm2 THEN END bm2
  explain_exception()
  CleanUp(0)
ENDPROC

