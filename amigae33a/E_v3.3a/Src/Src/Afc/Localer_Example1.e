/*

  Localer Example 1 - (C)Copyright Amiga Foundation Classes

  Written By:   Fabio Rotondo

  Public Domain Source

*/

MODULE 'afc/localer'

PROC main()
  DEF loc:PTR TO localer

  NEW loc.localer()

  WriteF('System Language:\s\n', loc.language())

/*

  Since we don't have any ".catalog" provided with this
  example, the loading will fail AND the get() method
  will RETURN DEFAULT messages

*/

  loc.load('test.catalog')


  WriteF('\s - \s\n', loc.get(0, 'Test'), loc.get(1, 'Message!'))

  END loc
ENDPROC

