
/*
  $VER: Displayer_Example 2 - (C)Copyright Amiga Foundation Classes

  Written By: Andrea Galimberti

  This IS a complete example that exploits the following modules belonging
  TO the Amiga Foundation Classes: Displayer, Mousepointer (AND so Hardsprite).

  See how the mouse pointer automatically rescales its movements TO fit
  the changing resolution; the pointer sprite IS bounded TO the
  dimensions OF the viewports it moves into.

  You can scroll the topmost bitmap because it's bigger than the viewport
  it's displayed in.
*/

MODULE 'graphics/modeid',
       'intuition/intuition',
       'AFC/displayer',
       'AFC/mousepointer',
       'AFC/explain_exception'


PROC main() HANDLE
  DEF myview=NIL:PTR TO displayer
  DEF vp1=NIL, vp2=NIL
  DEF rp1=NIL, rp2=NIL
  DEF win=NIL:PTR TO window, poi:PTR TO INT
  DEF topo:PTR TO mousepointer, vp
  DEF rx=0

  NEW topo.mousepointer(2)

  topo.image(11,[$0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $1F7C,$1F7C,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080]:INT)

  topo.hotspot(18,6)  -> the mouse starts from the topmost viewport, so
                      -> its hotspot it's SET in high resolution: the
                      -> auto() method OF mousepointer rescales it TO adapt TO
                      -> the changing resolution.

  NEW myview.displayer()

  myview.add([VPORT_TOP,0,
              VPORT_LEFT,0,
              VPORT_WIDTH,640,
              VPORT_HEIGHT,100,
              VPORT_DEPTH,3,
              VPORT_MODE,HIRES_KEY,
              VPORT_BMWIDTH,800,    -> bitmap IS wider than viewport
              VPORT_SPRITES,TRUE,
              NIL])

  myview.add([VPORT_TOP,110,
              VPORT_LEFT,0,
              VPORT_WIDTH,320,
              VPORT_HEIGHT,50,
              VPORT_DEPTH,3,
              VPORT_MODE,LORES_KEY,
              VPORT_SPRITES,TRUE,
              NIL])

  myview.add([VPORT_TOP,170,
              VPORT_LEFT,0,
              VPORT_WIDTH,640,
              VPORT_HEIGHT,80,
              VPORT_DEPTH,3,
              VPORT_MODE,HIRES_KEY,
              VPORT_SPRITES,TRUE,
              NIL])

  myview.setup(DEFAULT_MONITOR_ID)  -> choose your monitor database

-> this window IS opened only TO clear the standard mouse sprite
  win:=OpenWindowTagList(NIL,[WA_LEFT,0,
                              WA_TOP,0,
                              WA_WIDTH,320,
                              WA_HEIGHT,128,
                              WA_TITLE,'Wow',
                              WA_FLAGS,WFLG_DRAGBAR OR WFLG_CLOSEGADGET OR WFLG_ACTIVATE OR WFLG_SIMPLE_REFRESH,
                              WA_IDCMP,IDCMP_CLOSEWINDOW OR IDCMP_ACTIVEWINDOW,
                              NIL,NIL])
  IF win=NIL THEN Raise("win")
  poi:=[0,0]:INT
  SetPointer(win,poi,1,1,0,0)

-> here begins the show
  Forbid()       -> forbid task switching
  myview.show()

  rp1:=myview.rastport(0)
  rp2:=myview.rastport(1)
  vp1:=myview.viewport(0)
  vp2:=myview.viewport(1)

  SetRast(rp1,3)
  SetRast(rp2,4)
  SetRast(myview.rastport(2),2)

  SetRGB4(vp1,2,15,15,15)
  SetAPen(rp1,2)
  Move(rp1,10,10)
  Text(rp1,'First (Hires) ViewPort',22)
  SetRGB4(vp2,2,0,0,15)
  SetAPen(rp2,2)
  Move(rp2,10,10)
  Text(rp2,'Second (Lowres) ViewPort',24)

  vp:=vp1
  topo.changeImage(vp1)   -> install NEW image
  topo.move(vp1,100,50)   -> start from topmost viewport
  REPEAT
    topo.auto(myview)  -> auto move
    IF topo.vp()=0        -> IF topmost viewport
      IF topo.x()>=640       -> IF mouse hits the boundaries THEN scroll
        rx:=rx+1
        IF rx>80 THEN rx:=80
        myview.scroll(0,rx,0)
      ENDIF
      IF topo.x()<=0
        rx:=rx-1
        IF rx<0 THEN rx:=0
        myview.scroll(0,rx,0)
      ENDIF
    ENDIF
  UNTIL Mouse()

  Permit()  -> permit task switching
  myview.showOldView()   -> restore old view

EXCEPT DO
  END topo
  END myview
  IF win THEN CloseWindow(win)
  explain_exception()
ENDPROC

