/*

  $VER: ReqTooller Example 1 - (C)Copyright Amiga Foundation Classes

  Written By: Fabio Rotondo

  This code is Public Domain


*/

MODULE 'afc/reqtooller', 'afc/explain_exception'

PROC main() HANDLE
  DEF rt=NIL:PTR TO reqtooller
  DEF s:PTR TO CHAR

  NEW rt.reqtooller()          -> First of all, we init the class.

  -> Here we set the default title and path
  rt.setattrs([RT_TITLE, 'reqtooller Example',
               RT_PATH, 'Ram:',
               NIL, NIL])



  /*
      Here we open a SAVE requester
      Please, note the RT_FULLNAME set to TRUE:
      when we'll do a get(RT_FILENAME) the filename returned will contain
      also the path.
  */
  rt.req(RTREQ_SAVE, [RT_OKGAD, '_Save', RT_FULLNAME, TRUE, NIL])
  WriteF('Path:\s\n', rt.get(RT_PATH))
  WriteF('FullName:\s\n', rt.get(RT_FILENAME))


  -> Hey! Here there is a path requester!
  -> (Here we change the OK gadget, because it would remain "Save"...)
  rt.req(RTREQ_PATH, [RT_OKGAD, 'Ok', NIL, NIL])
  WriteF('NEW Path:\s\n', rt.get(RT_PATH))


  -> And here a Volumes requester!
  rt.req(RTREQ_VOLUMES, NIL)


  -> And what about a MultiFile requester?
  rt.req(RTREQ_MULTI, NIL)

  /*
      This is the right routine to scan throught all
      the files selected.
      Please, note that we have still the RT_FULLNAME flag set.
      So the names returned will contain full path.
  */
  WHILE (s:=rt.get(RT_MULTINEXT))
    WriteF('Name:\s\n', s)
  ENDWHILE


  -> Wow a font requester
  rt.req(RTREQ_FONT, NIL)


  -> And a font requester supporting color fonts :-)
  rt.req(RTREQ_COLFONT, [RT_FONTNAME, 'XHelvetica.font', NIL])
  WriteF('FontName:\s\n', rt.get(RT_FONTNAME))

  -> Look: the EasyReq returns a value, we get it!
  WriteF('EasyReq result:\d\n', rt.req(RTREQ_EASY, [RT_TEXT,    'Do You Like This Requester?',
                                                    RT_GADS,    'Yes!|Hmmm...|No',
                                                    RT_CHOICE,  2,
                                                    NIL, NIL]))


  -> A Standard Screen Requester
  rt.req(RTREQ_SCREEN)
  WriteF('ModeID:\h\n', rt.get(RT_SCRID))

  -> And a ALL resolutions screen requester
  rt.req(RTREQ_ALLSCREEN)

  -> A String Requester (also this requester returns a value...
  WriteF('STRING Result:\d\n', rt.req(RTREQ_STRING, [RT_DEFSTR, 'Hello!',
                        RT_MAXCHARS,   10,
                        RT_TEXT, 'Insert Your Name',
                        RT_GADS, 'This is|I do not want',
                       NIL]))

  -> A number requester
  rt.req(RTREQ_NUMBER, [RT_MINVAL, 10, RT_MAXVAL, 20, RT_DEFVAL, 11, NIL])


  -> An example of EasyRequester with arguments.
  rt.req(RTREQ_EASY, [RT_TEXT, 'My name is:\s \s.',
                      RT_ARGS, ['Fabio', 'Rotondo'],
                      RT_GADS, 'Ok',
                      NIL])

  -> A Message requester...
  rt.req(RTREQ_MESSAGE, NIL)

  -> That will be there for 1 second.
  Delay(50)

EXCEPT DO

  explain_exception()
  END rt
  CleanUp(0)
ENDPROC

