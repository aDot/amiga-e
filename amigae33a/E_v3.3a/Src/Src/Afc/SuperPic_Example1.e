/*

    $VER: Super Picture Example no. 1 (V1.00) - By Fabio Rotondo

    (C)Copyright Amiga Foundation Classes

    This example is Public Domain.

*/

MODULE 'afc/super_picture', 'afc/explain_exception',
       'dos/rdargs',
       'intuition/screens'

PROC main() HANDLE
  DEF myargs:PTR TO LONG, rda=NIL
  DEF pic=NIL:PTR TO super_picture            -> The SuperPic OBJECT
  DEF scr=NIL:PTR TO screen, w, h, sw, sh

  WriteF('PView V1.00 - By Fabio Rotondo.\n')

  myargs:=[0,0,0,0,0,0]:LONG
  IF (rda:=ReadArgs('PICNAME/A,VERBOSE/S', myargs, NIL))=NIL THEN Raise("args")

  NEW pic.super_picture()                    -> Here we init the class

  pic.load(myargs[0])                        -> Just a load command (Supports DataTypes!) 8-)

  w:=pic.width()           -> Here we obtain pic's width and height
  h:=pic.height()

  IF w<320 THEN sw:=320 ELSE sw:=w  -> And here we compute screen sizes
  IF h<256 THEN sh:=256 ELSE sh:=h

  -> If the user set the VERBOSE flag in the Shell parameters, we just show
  -> some more info about the picture before displaying it.
  IF myargs[1]
    WriteF('Picture:\s\nWidth: \d\nHeight: \d\nDepth:\d\nModeID: $\h\n',
                                                          myargs[0],
                                                          w,h,pic.depth(),
                                                          pic.modeid())
  ENDIF

  -> Here we open a screen
  IF (scr:=OpenScreenTagList(NIL,
                            [SA_WIDTH,     sw,
                             SA_HEIGHT,    sh,
                             SA_DEPTH,     pic.depth(),
                             SA_DISPLAYID, pic.modeid(),
                            0,0]))=NIL THEN Raise("scr")

  pic.paltoscr(scr)       -> Just to set the right palette


  -> We blit it all!
  BltBitMapRastPort(pic.bitmap(), 0,0, scr.rastport, 0,0, w, h, $C0)


  -> Waiting for a mouse click...
  REPEAT
    Delay(5)
  UNTIL Mouse()

EXCEPT DO
  SELECT exception
    CASE "args"
      WriteF('Bad args!\n')
    CASE "scr"
      WriteF('Could not open screen!\n')
    DEFAULT
      explain_exception()
  ENDSELECT

  IF scr THEN CloseScreen(scr)   -> Close the screen

  IF rda THEN FreeArgs(rda)      -> Free ReadArgs structure

  END pic                        -> Close the object

  CleanUp(0)                     -> General cleaning up...
ENDPROC
