OPT OSVERSION=37
OPT LARGE

MODULE 'afc/mgui',
       'afc/explain_exception',
       'tools/easyGUI',
       'exec/ports',
       'intuition/intuition'

DEF times=0

PROC main() HANDLE
  DEF mg=NIL:PTR TO mgui
  DEF win=NIL:PTR TO window
  DEF sig

  NEW mg.mgui()

  mg.addA('MGUI Main', [ROWS,
                          [SBUTTON, {kill_all}, 'Kill ALL'],
                          [SBUTTON, {newgui}, 'NEW!'],
                          [SBUTTON, {hideall}, 'Hide'],
                          [SBUTTON, {showall}, 'Show']
                       ],
                       [MGUI_MAIN, TRUE,
                        NIL, NIL]
          )

  test(mg)
  test(mg)
  test(mg)

  IF (win:=OpenWindowTagList(NIL, [WA_WIDTH, 200,
                               WA_HEIGHT, 100,
                               WA_TITLE, 'Click Inside!',
                               WA_IDCMP, IDCMP_MOUSEBUTTONS,
                               NIL, NIL]))=NIL THEN Raise("win")

  sig:=win.userport::mp.sigbit
  sig:=Shl(1, sig)

  mg.setattrs([MGUI_ADDEXTERNAL, [{click}, sig, [win, win.userport, mg, sig, {win}]],
             0,0])

  WHILE (mg.empty() = FALSE)
    mg.message()
  ENDWHILE

EXCEPT DO
  IF win THEN CloseWindow(win)
  WriteF('Exiting...\n')
  explain_exception()

  END mg
  CleanUp(0)
ENDPROC

PROC hideall(gui:PTR TO gui_obj)
  gui.mg.setattrs([MGUI_HIDEALL, TRUE, NIL, NIL])
  gui.mg.first()
  gui.mg.hide(FALSE)
ENDPROC

PROC showall(gui:PTR TO gui_obj) IS gui.mg.setattrs([MGUI_HIDEALL, FALSE, NIL, NIL])

PROC newgui(gui:PTR TO gui_obj) IS test(gui.mg)

PROC kill_all(gui:PTR TO gui_obj) IS gui.mg.clearguis()

PROC msg() IS WriteF('Message!\n')


PROC test(mg:PTR TO mgui)
  DEF gui:PTR TO LONG

  gui:=NEW [ROWS,
       NEW   [SBUTTON, {msg}, 'Msg'],
       NEW   [SBUTTON, 1, 'End']
           ]

  mg.addA('MGui Power!', gui)

ENDPROC

PROC click(t:PTR TO LONG)
  DEF x:PTR TO LONG

  x:=GetMsg(t[1])
  ReplyMsg(x)
  WriteF('Win Clicked: \d Times!\n', times++)
ENDPROC

