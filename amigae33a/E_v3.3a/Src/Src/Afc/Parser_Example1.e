/*

  $VER: Parser Example 1 - (C)Copyright Amiga Foundation Classes

  Written By: Fabio Rotondo

  This code is Public Domain

*/

MODULE 'afc/parser', 'afc/explain_exception'

PROC main() HANDLE
  DEF par=NIL:PTR TO parser

  NEW par.parser()

  WriteF('Template: FILE/A,COLOR/K,NAME/K\n')
  WriteF('STRING:   NAME="Amiga Foundation Classes" COLOR="Blue" Parser.e\n')

  par.parse('FILE/A,COLOR/K,NAME/K', 'NAME="Amiga Foundation Classes" COLOR="Blue" Parser.e')

  WriteF('Results:\nArg 0 (File)   - \s\nArg 1 (Color)  - \s\nArg 2 (Name)   - \s\n', par.arg(0), par.arg(1), par.arg(2))

EXCEPT DO
  explain_exception()
  END par
ENDPROC

