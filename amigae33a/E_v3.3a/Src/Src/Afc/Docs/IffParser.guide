@database IffParser.guide

@$VER: IFFParser V1.20 - By Fabio Rotondo (C)Amiga Foundation Classes

This guide file has been created using AutoGuide V1.05 - By Fabio Rotondo (fsoft@intercom.it).

@node "Main" "IFFParser Guide"

                  ** IFFParser - Written By Fabio Rotondo **

                    * Part of the Amiga Foundation Classes *


     @{" Introduction " link "Introduction"}            @{" Author's Info " link "Author"}         @{" Amiga Foundation Classes " link "AFC.guide/Main"}

                   @{" SAS/C and StormC Notes " LINK "IFFParser_CNotes"}   @{" C-Only Functions " LINK "IFFParser_COnly"}

    Requires: Nothing.

    Class Base: $00080000

    COMMANDS                            BRIEF DESCRIPTION
    -------------------------------------------------------------------------- -
    @{" iffparser()                    " link "IFFParser_iffparser"}    Inits the iffparser object.
    @{" addr()                         " link "IFFParser_addr"}    Returns current data position in mem.
    @{" close()                        " link "IFFParser_close"}    Closes currently IFF session.
    @{" closechunk()                   " link "IFFParser_closechunk"}    Closes last created chunk.
    @{" createchunk(type, id, size)    " link "IFFParser_createchunk"}    Inits a new chunk to write.
    @{" error()                        " link "IFFParser_error"}    Returns last error raised.
    @{" exit(type, id)                 " link "IFFParser_exit"}    Defines when scan() must stop.
    @{" first(type, id)                " link "IFFParser_first"}    Prepares data examination.
    @{" getheader(string, filename)    " link "IFFParser_getheader"}    Returns header of a file.
    @{" load(filename)                 " link "IFFParser_load"}    Inits an IFF Load session.
    @{" save(filename)                 " link "IFFParser_save"}    Inits an IFF SAVE session.
    @{" scan()                         " link "IFFParser_scan"}    Begins scanning an IFF file.
    @{" setscan(type, id)              " link "IFFParser_setscan"}    Defines type of chunk to scan for.
    @{" succ()                         " link "IFFParser_succ"}    Gets next memory data location.
    @{" size()                         " link "IFFParser_size"}    Returns current data size.
    @{" writechunk(datas, datalen)     " link "IFFParser_writechunk"}    Writes some datas to a chunk.
    @{" version()                      " link "IFFParser_version"}    Returns IFFParser class version and revision.


    @{" Error Table " link "IFFParser_ErrorTable"}
@endnode

@node "IFFParser_CNotes" "SAS/C and StormC Notes"

  SAS/C and StormC Notes

  We provide ".o" files for both SAS/C and StormC compilers.


  NOTE:
    The StormC compiler we are using is a _demo_ version, so we cannot create
    optimized version of the class.

    We have asked to Haage & Partner a FREE version of StormC, but we receive
    no answer at all. We are still waiting.
    We are not going to buy it because we usually write code using AmigaE and
    furthermore, we have already bought SAS/C compiler.

  StormC supports exceptions, while SAS/C doesn't. So we have been forced to
  find a "work around" for SAS/C compiler.

  We have introduced the error() method which will return the last error
  encountered during program execution.

  So keep in mind this: when an error occurs, StormC version will raise an
  exception, while SAS/C version will return an error code or NULL and you
  will have to check error() status.


@endnode

@node "IFFParser_COnly" "IFFParser C-Only Functions"

  C-Only Functions

  We have created this function:

         NAME: id(idname)

     SYNOPSIS: ULONG id(STRPTR idname)

  DESCRIPTION: This function is a short cut to allow you to easily create "chunks" name.
               As you may now, chunks name are ULONG values. Usually you have to use
               the MAKE_ID() macro, but the id() function is faster.

        INPUT: idname       - a string, for example "ILBM", "BODY"....

      RETURNS: the corrisponding ULONG value.


  We hope you'll find it useful

@endnode

@node "Introduction" "Introduction "


    IFFParser is an Object to easily read/write IFF files.

    It  is not so powerful as IFFParse.library:  it misses some features, but I
have  written  it  to  match  all my needs, and it does.  I hope you will enjoy
using. Look at the examples for working programs.


@endnode

@node "Author" "Author's Info"
    Original By:  Fabio Rotondo  (fsoft@intercom.it)

    E Version By: Fabio Rotondo

    C++ Version By: Massimo Tantignone (tanti@intercom.it)

    Address:

            Fabio Rotondo
            C.so Vercelli 9
            28100 Novara
            ITALY

            e-mail: fsoft@intercom.it
                    Fabio.Rotondo@deagostini.it

            Phone:  (ITA) - (0)321 -   459676  (home)
                    (ITA) - (0)2   - 38086520  (office)
                    (ITA) - (0)338 -  7336477  (GSM Phone)

            Fax:    (ITA) - (0)2   - 38086278

            Web:    http://www.intercom.it/~fsoft               (my home page)

                    http://www.intercom.it/~fsoft/ablast.html   (Amiga Blast Home Page)


@endnode

@node "IFFParser_ErrorTable" "Amiga Foundation Classes Module: IFFParser / Error Table"
 VALUE           | DESCRIPTION
-----------------+--------------------------------------------------------
 $0000           | No Memory.
 $0001           | Could not open iffparse.library.
 $0002           | Could not AllocIFF().
 $0003           | Could not create filename with Open(fname, MODE_NEWFILE).
 $0004           | Could not OpenIFF().
 $0005           | IFFParser object not initialized with load() or save().
 $0006           | Could not Push current chunk.
 $0007           | WriteChunkBytes() failed.
 $0008           | Could not Pop current chunk.
 $0009           | Could not Open() file for reading.
 $000A           | Could not do a CollectionChunk() call.
 $000B           | Could not assign StopOnExit.
@endnode

@node "IFFParser_iffparser" "Amiga Foundation Classes Module: IFFParser / iffparser()                     "

           NAME: iffparser()

       SYNOPSIS: iffparser(void)

    DESCRIPTION: This method will initialize the iffparser object.

          INPUT: NONE.

        RESULTS: NONE.

       SEE ALSO:
@endnode

@node "IFFParser_close" "Amiga Foundation Classes Module: IFFParser / close()                         "

           NAME: close()

       SYNOPSIS: void close(void)

    DESCRIPTION: This method will close curent IFF session.

          INPUT: NONE.

        RESULTS: NONE.

       SEE ALSO: @{" load() " link "IFFParser_load"}
                 @{" save() " link "IFFParser_save"}
@endnode

@node "IFFParser_save" "Amiga Foundation Classes Module: IFFParser / save(filename)                  "

           NAME: save(filename:PTR TO CHAR)

       SYNOPSIS: LONG save(STRPTR fname)

    DESCRIPTION: Use This method to create a new IFF file and to begin
                 a IFF save session.

          INPUT: filename   - Name of the file to open / load.

        RESULTS: NONE.


       SEE ALSO: @{" createchunk() " link "IFFParser_createchunk"}
                 @{" writechunk()  " link "IFFParser_writechunk"}
                 @{" closechunk()  " link "IFFParser_closechunk"}
                 @{" close()       " link "IFFParser_close"}
@endnode

@node "IFFParser_createchunk" "Amiga Foundation Classes Module: IFFParser / createchunk(type, id, size)     "

           NAME: createchunk(type, id, size=IFFSIZE_UNKNOWN)

       SYNOPSIS: LONG createchunk(ULONG type, ULONG id, ULONG size = IFFSIZE_UNKNOWN)

    DESCRIPTION: This method will create a new chunk where to write in.

          INPUT: type       - (LONG) type of the chunk. (ex. "ILBM")
                 id         - (LONG) id of the chunk. (ex. "FORM")

        RESULTS: NONE.


       SEE ALSO: @{" closechunk() " link "IFFParser_closechunk"}
                 @{" writechunk() " link "IFFParser_writechunk"}
                 @{" save()       " link "IFFParser_save"}
@endnode

@node "IFFParser_writechunk" "Amiga Foundation Classes Module: IFFParser / writechunk(datas, datalen)      "

           NAME: writechunk(data, datalen)

       SYNOPSIS: LONG writechunk(STRPTR data, ULONG datalen)

    DESCRIPTION: Use This method to write some data into a chunk.

          INPUT: data    - (PTR TO LONG) memory location of your datas.
                 datalen - (LONG) length in bytes of your data.

        RESULTS: NONE.


       SEE ALSO: @{" save()        " link "IFFParser_save"}
                 @{" createchunk() " link "IFFParser_createchunk"}
                 @{" closechunk()  " link "IFFParser_closechunk"}
@endnode

@node "IFFParser_closechunk" "Amiga Foundation Classes Module: IFFParser / closechunk()                    "

           NAME: closechunk()

       SYNOPSIS: LONG closechunk(void)

    DESCRIPTION: This method will close current chunk.

          INPUT: NONE.

        RESULTS: NONE.


       SEE ALSO: @{" createchunk() " link "IFFParser_createchunk"}
                 @{" writechunk()  " link "IFFParser_writechunk"}
                 @{" save()        " link "IFFParser_save"}
@endnode

@node "IFFParser_load" "Amiga Foundation Classes Module: IFFParser / load(filename)                  "

           NAME: load(filename:PTR TO CHAR)

       SYNOPSIS: LONG load(STRPTR filename)

    DESCRIPTION: Use This method to open an already existing IFF file and
                 to begin a IFF load session.

          INPUT: filename   - Name of the file to open / load.

        RESULTS: NONE.


       SEE ALSO: @{" setscan() " link "IFFParser_setscan"}
                 @{" scan()    " link "IFFParser_scan"}
                 @{" close()   " link "IFFParser_close"}
@endnode

@node "IFFParser_setscan" "Amiga Foundation Classes Module: IFFParser / setscan(type, id)               "

           NAME: setscan(type, id)

       SYNOPSIS: LONG setscan(ULONG type, ULONG id)

    DESCRIPTION: Use This method to set chunk that will be loaded by
                 scan().

          INPUT: type       - (LONG) type of chunk. (ex. "ILBM")
                 id         - (LONG) id of chunk.   (ex. "FORM")

        RESULTS: NONE.


          NOTES: You can do more than a single setscan() before scan()ing the
                 IFF file. This is the biggie ;)
                 In this way you can load up in memory all you need in a single
                 file access.

       SEE ALSO: @{" scan() " link "IFFParser_scan"}
                 @{" load() " link "IFFParser_load"}
@endnode

@node "IFFParser_first" "Amiga Foundation Classes Module: IFFParser / first(type, id)                 "

           NAME: first(type, id)

       SYNOPSIS: APTR first(ULONG type, ULONG id)

    DESCRIPTION: Use This method to position IFFParser object to the FIRST
                 memory data location of a specific kind.

          INPUT: type       - (LONG) type of the chunk. (ex. "ILBM")
                 id         - (LONG) id of the chunk. (ex. "FORM")

        RESULTS: a PTR TO LONG memory location of the data (may be NIL)


       SEE ALSO: @{" load()    " link "IFFParser_load"}
                 @{" scan()    " link "IFFParser_scan"}
                 @{" setscan() " link "IFFParser_setscan"}
                 @{" succ()    " link "IFFParser_succ"}
@endnode

@node "IFFParser_addr" "Amiga Foundation Classes Module: IFFParser / addr()                          "

           NAME: addr()

       SYNOPSIS: APTR addr(void)

    DESCRIPTION: This method will return current memory data address.

          INPUT: NONE.

        RESULTS: a PTR TO LONG specifying memory data address (may be NIL)

       SEE ALSO: @{" size()  " link "IFFParser_size"}
                 @{" first() " link "IFFParser_first"}
                 @{" succ()  " link "IFFParser_succ"}
@endnode

@node "IFFParser_size" "Amiga Foundation Classes Module: IFFParser / size()                          "

           NAME: size()

       SYNOPSIS: LONG size(void)

    DESCRIPTION: Use This method to get size of current memory data.

          INPUT: NONE.

        RESULTS: size   - (LONG) size of the memory data. (May be NIL)


       SEE ALSO: @{" first() " link "IFFParser_first"}
                 @{" succ()  " link "IFFParser_succ"}
@endnode

@node "IFFParser_succ" "Amiga Foundation Classes Module: IFFParser / succ()      "

           NAME: succ()

       SYNOPSIS: APTR succ(void)

    DESCRIPTION: Use This method to position to the next memory data.

          INPUT: NONE.

        RESULTS: a PTR TO LONG to the new location of memory data. (May be NIL)


       SEE ALSO: @{" load()  " link "IFFParser_load"}
                 @{" first() " link "IFFParser_first"}
@endnode

@node "IFFParser_scan" "Amiga Foundation Classes Module: IFFParser / scan()                          "

           NAME: scan()

       SYNOPSIS: LONG scan(void)

    DESCRIPTION: Use This method to begin scanning an IFF file.

          INPUT: NONE.

        RESULTS: NONE.


       SEE ALSO: @{" setscan() " link "IFFParser_setscan"}
                 @{" load()    " link "IFFParser_load"}
@endnode

@node "IFFParser_getheader" "Amiga Foundation Classes Module: IFFParser / getheader(string, filename)     "

           NAME: getheader(string:PTR TO CHAR, filename:PTR TO CHAR)

       SYNOPSIS: LONG getheader(STRPTR s, STRPTR filename)

    DESCRIPTION: Use This method to determinate the kind of a file.

          INPUT: string     - A _VALID_ Estring already initialized.
                 filename   - Name of the file to examine.

        RESULTS: string     - your _VALID_ Estring will be filled with the
                              file header (ex. "ILBM")

           NOTE: This method returns a STRING not a LONG!


       SEE ALSO:
@endnode

@node "IFFParser_exit" "Amiga Foundation Classes Module: IFFParser / exit(type, id)                  "

           NAME: exit(type, id)

       SYNOPSIS: LONG exit(ULONG type, ULONG id)

    DESCRIPTION: Use This method to determinate WHEN scan() should stop.

          INPUT: type       - (LONG) type of the chunk. (ex. "ILBM")
                 id         - (LONG) id of the chunk. (ex. "FORM")

        RESULTS: NONE.


       SEE ALSO: @{" load()   " link "IFFParser_load"}
                 @{" scan()   " link "IFFParser_scan"}
                 @{" setscan()" link "IFFParser_setscan"}
@endnode

@node "IFFParser_version" "Amiga Foundation Classes Module: IFFParser / version()      "

           NAME: version()

       SYNOPSIS: LONG version(BOOL rev = FALSE)

    DESCRIPTION: This method returns version and revision of the class.

          INPUT: NONE.

        RESULTS: this method will return TWO values: version and revision.

  PORTING NOTES: The C++ class version behaves differently, since C++ cannot
                 return two values at the same time.

                 So, if you call just version(), you'll get the VERSION value,
                 while calling version(TRUE), you'll get the REVISION value.
                 This is just a quick and dirty workaround...

       SEE ALSO:
@endnode

@node "IFFParser_error" "Amiga Foundation Classes Module: IFFParser / error()      "

           NAME: error()

       SYNOPSIS: ULONG error(void)

    DESCRIPTION: This method returns the last error raised.

          INPUT: NONE.

        RESULTS: this method will return the last error code raised.

       SEE ALSO:
@endnode

