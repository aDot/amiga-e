@database parser.guide

@$VER: Parser V1.30 - By Fabio Rotondo (C)Brighting Brain Brothers

This guide file has been created using AutoGuide V1.05 - By Fabio Rotondo (fsoft@intercom.it).

@node Main "Amiga Foundation Classes: Parser"



                       ** Parser  - Original By Fabio Rotondo **

                         Part of the Amiga Foundation Classes

      @{" Introduction " link "Parser_introduction"}  @{" Author's Info " link "author"}  @{" Amiga Foundation Classes " LINK "AFC.Guide/main"}  @{" History " LINK "Parser_History"}

   Requires: Nothing.

   Class Base: $00090000

   NEW COMMANDS                          BRIEF DESCRIPTION
   -------------------------------------------------------------------------
   @{" parser(numelems=25)                 " link "Parser_Parser"} Initailizes the class.
   @{" arg(n)                              " Link "Parser_Arg"} Returns n.th argument.
   @{" parse(template, string)             " Link "Parser_Parse"} Parses a string.
   @{" version()                           " link "Parser_version"} Returns Parser version.

   @{" Error Table " LINK Parser_ErrorTable}
@endnode

@node "Parser_Introduction" "Introduction"

  INTRODUCTION

  Parser is a class which relies on AmigaDOS ReadArgs() method to parse strings.
  This is a very smart approach to, for example, ARexx argument parsing and can
  lead to great results and incredibly flexible implementations.

  With Parser class you can use all standard AmigaDOS templates to parse a string.


@endnode

@node "author" "Author's Info"

    Original By:    Fabio Rotondo  (fsoft@intercom.it)

    E Version By:   Fabio Rotondo

    C++ Version By: Massimo Tantignone (tanti@intercom.it)


    Address:

            Fabio Rotondo
            C.so Vercelli 9
            28100 Novara
            ITALY

            e-mail: fsoft@intercom.it
                    Fabio.Rotondo@deagostini.it

            Phone:  (ITA) - (0)321 -   459676  (home)
                    (ITA) - (0)2   - 38086520  (office)
                    (ITA) - (0)338 -  7336477  (GSM Phone)

            Fax:    (ITA) - (0)2   - 38086278

            Web:    http://www.intercom.it/~fsoft               (my home page)

                    http://www.intercom.it/~fsoft/ablast.html   (Amiga Blast Home Page)

@endnode

@node Parser_ErrorTable "Parser / Error Table "

VALUE           | DESCRIPTION
----------------+----------------------------------
$0000           | No Memory.
@endnode

@node "Parser_Parser" "Amiga Foundation Classes: Parser/Parser()"

           NAME: parser(numelems=25)

       SYNOPSIS: VOID parser(ULONG numelems=25)

    DESCRIPTION: This is the class constructor.
                 You should pass the maximum number of elements to parse
                 at every call. Default value is 25 elements which should be
                 enought for everyday use.

          INPUT: numelems   - (Optional) Number of elements to parse.

        RESULTS:

       SEE ALSO:
@endnode

@node "Parser_Parse" "Amiga Foundation Classes: Parser/Parse()"

           NAME: parser(template:PTR TO CHAR, str:PTR TO CHAR)

       SYNOPSIS: RDArgs * parse(STRPTR template, STRPTR str)

    DESCRIPTION: This method parses a string using the provided template.
                 Templates are standard AmigaDOS templates, such as:

                 "FILE/A,NAME/K,DELETE/S"

          INPUT: template   - String parsing template.

                 str        - String to parse.

        RESULTS: a pointer to the resulting RDArgs structure.
                 Just check it against NIL (or NULL) which means "error".

           NOTE: The template string CANNOT contain white spaces.

       SEE ALSO: @{" arg() " LINK "Parser_arg"}
@endnode

@node "Parser_arg" "Amiga Foundation Classes: Parser/arg()"

           NAME: arg(ULONG elem)

       SYNOPSIS: APTR arg(ULONG elem)

    DESCRIPTION: This method returns the argument 'elem'.
                 You MUST have parsed something to get reliable results.

          INPUT: elem     - The (ordinal) number of the argument you wish to get.
                            The number starts from 0 and increments until the last
                            argument present in the template you provided to
                            @{" parse() " LINK "Parser_parse"} method.
                            Ex:

                            "FILE/A,NAME/K,DELETE/S"

                                0     1       2

        RESULTS: the pointer to the argument selected.

       SEE ALSO: @{" parse() " LINK "Parser_parse"}
@endnode

@node "Parser_version" "Amiga Foundation Classes: Parser/version()"

           NAME: version()

       SYNOPSIS: ULONG version(BOOL revision = FALSE)

    DESCRIPTION: This method returns class version and revision.

          INPUT: NONE.

        RESULTS: this method returns TWO values: VERSION and REVISION.

       SEE ALSO:
@endnode

