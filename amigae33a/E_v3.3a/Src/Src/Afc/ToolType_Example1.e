/*

  ToolType Example 1 - (C)Copyright Amiga Foundation Classes

  Written By: Fabio Rotondo

  Public Domain Source


  NOTE: This program will happily fail, raising an exception
        if you don't provide a _valid_ icon for it and don't
        modify the tooltypes... see docs!

*/

MODULE 'afc/tooltype', 'afc/explain_exception'

PROC main() HANDLE
  DEF ttype:PTR TO tooltype
  DEF s

  WriteF('This is a little try of tooltype\n')

  NEW ttype.tooltype(TRUE)

  WriteF('Everything Is Fine\n')
  s:=ttype.get('MYTRY')
  IF s THEN WriteF('Get() Result:\s\n', s)

  s:=ttype.match('MYTRY','FABIO')
  WriteF('Match Result:\d\n', s)

EXCEPT DO
  explain_exception()
  END ttype
ENDPROC

