/*

  $VER: MousePointer Example 1 - (C)Amiga Foundation Classes

  Written By: Andrea Galimberti

  This code is Public Domain

*/

MODULE 'AFC/mousepointer', 'AFC/explain_exception',
       'intuition/screens'


PROC main() HANDLE
  DEF scr=NIL:PTR TO screen
  DEF vp
  DEF topo:PTR TO mousepointer

  NEW topo.mousepointer(2)  -> try TO get hold OF sprite number 2

  topo.image(11,[$0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $1F7C,$1F7C,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080,
                 $0080,$0080]:INT)  -> the LIST must be OF INT type

  topo.hotspot(9,6)  -> we SET the hotspot

  scr:=OpenScreenTagList(NIL,[SA_TOP,0,
                              SA_LEFT,0,
                              SA_WIDTH,320,
                              SA_HEIGHT,256,
                              SA_DEPTH,5,
                              SA_TITLE,'Mouse Test',
                              NIL,NIL]
                        )
  IF scr=NIL THEN Raise("scr")
  vp:=scr.viewport

  topo.changeImage(vp)  -> the NEW image IS bound TO the viewport
  topo.move(vp,10,10)
  REPEAT
    topo.update(vp)  -> read mouse movements from hardware registers
    WaitTOF()
  UNTIL Mouse()

EXCEPT DO
  END topo
  IF scr THEN CloseScreen(scr)
  explain_exception()
ENDPROC

