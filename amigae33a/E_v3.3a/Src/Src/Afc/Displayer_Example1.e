/*

  $VER: Displayer Example 1 - (C)Amiga Foundation Classes

  Written By: Andrea Galimberti

  This code is Public Domain

*/


MODULE 'AFC/displayer','afc/explain_exception',
       'graphics/modeid'


PROC main() HANDLE
  DEF myview=NIL:PTR TO displayer
  DEF vp1, vp2
  DEF rp1=NIL, rp2=NIL

  NEW myview.displayer()  -> create the OBJECT

  myview.add([VPORT_TOP,0,             -> add viewports
              VPORT_LEFT,0,
              VPORT_WIDTH,640,
              VPORT_HEIGHT,128,
              VPORT_DEPTH,3,
              VPORT_MODE,HIRES_KEY,
              NIL])

  myview.add([VPORT_TOP,140,
              VPORT_LEFT,0,
              VPORT_WIDTH,320,
              VPORT_HEIGHT,100,
              VPORT_DEPTH,3,
              VPORT_MODE,LORES_KEY,
              NIL])

  myview.setup(DEFAULT_MONITOR_ID)   -> build system structures

  myview.show()   -> show NEW view (automatically stores the old view)

  rp1:=myview.rastport(0)  -> get PTR TO rastport AND viewport system
  rp2:=myview.rastport(1)  -> structures OF the two viewports.
  vp1:=myview.viewport(0)  -> Indices refer TO the order in which the two
  vp2:=myview.viewport(1)  -> viewports have been added.

  SetRast(rp1,3)
  SetRast(rp2,4)

  SetRGB4(vp1,2,15,15,15)
  SetAPen(rp1,2)
  Move(rp1,10,10)
  Text(rp1,'First (Hires) ViewPort',22)
  SetRGB4(vp2,2,0,0,15)
  SetAPen(rp2,2)
  Move(rp2,10,10)
  Text(rp2,'Second (Lowres) ViewPort',24)

  REPEAT
    Delay(5)
  UNTIL Mouse()
  myview.showOldView()  -> restore old view

EXCEPT DO
  END myview
  explain_exception()
ENDPROC

