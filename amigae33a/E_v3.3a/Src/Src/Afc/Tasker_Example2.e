
MODULE 'AFC/tasker', 'AFC/explain_exception',
       'exec/ports','exec/memory','exec/nodes'


-> a complex message
OBJECT complex
  mnode:mn  ->BE CAREFUL: This IS the structure, NOT the pointer
  wow:LONG
ENDOBJECT

-> the tasker structure IS DEFined global because we use it also in the
-> process' body.
DEF myp:PTR TO tasker

PROC thread()
  DEF message:PTR TO complex

-> get the global data pointer, previously stored by the main process.
-> IMPORTANT: DO this before using global variables OR functioncalls.
  geta4()

/*
  IF you DEF'd your tasker ('myp' in this CASE) as a global variable,
  you can access its port by the dosport() method. IF you need TO make it a
  local variable THEN, TO get the port associated TO the process, you
  follow the standard way:

    DEF thisthread:PTR TO process
    thisthread:=FindTask(0)
    dosport:=thisthread.msgport

  Warning: the use OF this port IS reserved TO AmigaDOS, so create your
           own port IF you need one.
*/

  -> create a port associated TO the process (this method must be called
  -> from the task that needs the port, not by the main program).
  myp.buildport()

  WaitPort(myp.port())
  message:=GetMsg(myp.port())

  PrintF('Hello, I''m a NEW thread. Wow IS:\d\n', message.wow)
  Delay(50)

-> make sure there IS no taskswitching after we replied the message
  Forbid()
  ReplyMsg(message)
  myp.endport()  -> remember TO close the port before ending.
ENDPROC

PROC main() HANDLE
  DEF mainp=NIL, mess=NIL:PTR TO complex

-> Store the global data pointer: DO this BEFORE any process IS started.
-> Remember TO geta4() in the process before using any global variable OR
-> functioncall!
  storea4()

  NEW myp.tasker('simpleprocess')  -> name AND initialization
  myp.code({thread})   -> the code TO be used

  IF (mainp:=buildPort())  -> we need a port in the main() process TO
                           -> communicate with thread()

    WriteF('Starting the process...\n')
    myp.process()  -> starting the process

    mess:=NewM(SIZEOF complex, MEMF_PUBLIC OR MEMF_CLEAR) -> alloc AND
    setupMsg(mess, SIZEOF complex, mainp)             -> setup the message
    mess.wow:=10    -> fill in your additional fields

    WriteF('Sending the message...\n')
    REPEAT              -> wait FOR the process TO create its own port
      Delay(5)          -> before sending anything TO it.
    UNTIL myp.port()

    myp.send(mess)  -> send the message TO thread()

    WaitPort(mainp)  -> wait FOR an answer.
-> IF process doesn't reply we can't Dispose the message memory 'cause
-> the thread may be still using it.
  ELSE
    WriteF('well, no port!\n')
  ENDIF

EXCEPT DO
  IF mess THEN Dispose(mess)  -> remember TO free the memory
  IF mainp THEN endPort(mainp)
  END myp
  explain_exception()
ENDPROC

