
MODULE 'AFC/tasker', 'afc/explain_exception'

-> tasks can only access global variables
DEF sharedvar

PROC simpletask()
  WHILE sharedvar<$8000000 DO sharedvar++
  Wait(NIL)  -> that IS TO say: wait TO be killed
ENDPROC


PROC main() HANDLE
  DEF myt:PTR TO tasker

  NEW myt.tasker('simpletask')  -> name OF the task

  myt.code({simpletask})  -> what code TO use
  WriteF('starting task...(press RETURN TO end)\n')
  myt.start()  -> starting task

  Inp(IF stdin THEN stdin ELSE stdout)
  WriteF('The shared variable now equals \d\n', sharedvar)

EXCEPT DO
  END myt  -> killing task (you can stop() it IF you don't need TO END
           -> the whole OBJECT (maybe TO restart it again later))
  explain_exception()
ENDPROC

