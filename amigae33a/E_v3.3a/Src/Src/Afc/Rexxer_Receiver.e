/*

   $VER: Rexxer Receiver Example 1.0

   Written By Fabio Rotondo

   (C)Copyright Amiga Foundation Classes

   NOTES: Run this program BEFORE Rexxer Sender.

          The Rexxer Receiver will handle messages
          arriving from Rexxer Sender.

          Even if it is possible to run multiple instances
          of this program, only the 'RECEIVE.1' port will
          be addressed by Rexxer Sender, so be careful!

*/

MODULE 'afc/rexxer', 'afc/explain_exception'

PROC main() HANDLE
  DEF rx=NIL:PTR TO rexxer

  NEW rx.rexxer('RECEIVE')

  REPEAT
    Wait(rx.sigbit())
  UNTIL rx.get({parse})

  WriteF('Rexxer Receiver is terminating...\n')

EXCEPT DO
  explain_exception()
  END rx
  CleanUp(0)
ENDPROC

PROC parse(s, mes=NIL)
  WriteF('Message arrived:\s\n', s)

  mes:=NIL
ENDPROC StrCmp('QUIT', s), 0, 'Hello World!'

