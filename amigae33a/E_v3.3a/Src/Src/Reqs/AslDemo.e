/* Some demo to use other, kick2 only librarys from E.
   We'll pop up a filerequester from the asl.library.   */

MODULE 'Asl', 'libraries/Asl'

DEF req:PTR TO filerequester

PROC main()
  IF aslbase:=OpenLibrary('asl.library',37)
    IF req:=AllocFileRequest()
      WriteF('Pick a file:\n')
      IF RequestFile(req)
        WriteF('Guess what! you picked "\s" in "\s" !\n',req.file,req.drawer)
      ELSE
        WriteF('Hard eh?\n')
      ENDIF
      FreeFileRequest(req)
    ELSE
      WriteF('Could not open filerequester!\n')
    ENDIF
    CloseLibrary(aslbase)
  ELSE
    WriteF('Could not open asl.library!\n')
  ENDIF
ENDPROC
