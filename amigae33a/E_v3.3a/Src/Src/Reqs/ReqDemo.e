/* Another demo how to use other librarys from E.
   a requester from the req.library.                  */

MODULE 'Req'

PROC main()
  IF reqbase:=OpenLibrary('req.library',2)
    IF request('This is some standard requester ...','Positive','Negative')
      request('I guessed that ...','Sure!','Get Real!')
    ELSE
      request('Try to be more positive!','I will','Why should I?')
    ENDIF
    CloseLibrary(reqbase)
  ELSE
    WriteF('Could not open req.library!\n')
  ENDIF
ENDPROC

PROC request(messy,yes,no)
ENDPROC TextRequest([messy,0,0,0,yes,no,'Huh?',0,0,$2FFFF,0,0])
