/* Yet another demo to use other librarys from E.
   Now we do some requesters from the reqtools.library.   */

MODULE 'ReqTools'

CONST FILEREQ=0,REQINFO=1

DEF colour,num=10,buf[120]:STRING,req

PROC main()
  IF reqtoolsbase:=OpenLibrary('reqtools.library',37)
    RtEZRequestA('Lets test that handy ReqTools library, shall we ?',
      'Why?|Not again!|Yes! lets see that!',0,0,0)
    IF (colour:=RtPaletteRequestA('Pick your colour:',0,0))=-1
      RtEZRequestA('Hard to choose eh?','Damn right.',0,0,0)     /* colreq */
    ELSE
      RtEZRequestA('Did you really like colour \d ?','Nope|Zure!',0,[colour],0)
    ENDIF
    RtEZRequestA('Now for some input ...','What?',0,0,0)
    RtGetLongA({num},'Enter a number:',0,0)                      /* longreq */
    StrCopy(buf,'bla',ALL)
    RtGetStringA(buf,100,'Enter something:',0,0)                 /* stringreq */
    RtEZRequestA('Lets have that nice filerequester ...',
      'More!more!|Boring!',0,0,0)
    IF req:=RtAllocRequestA(FILEREQ,0)
      buf[0]:=0
      RtFileRequestA(req,buf,'Pick your file:',0)
      RtFreeRequest(req)
    ENDIF
    RtEZRequestA('That\as it.','Phew.',0,0,0)
    CloseLibrary(reqtoolsbase)
  ELSE
    WriteF('Could not open reqtools.library!\n')
  ENDIF
ENDPROC
