-> show a textfile in a nice juicy scrolling window

MODULE 'tools/file', 'tools/exceptions', 'class/sctext'

PROC main() HANDLE
  DEF m,l,n,list,sc=NIL:PTR TO scrolltext
  m,l:=readfile(arg)
  list:=stringsinfile(m,l,n:=countstrings(m,l))
  NEW sc.settext(list,100)
  sc.open('TextView v0.1',20,20,300,150)
  WHILE sc.handle()=FALSE DO Wait(-1)
EXCEPT DO
  END sc
  report_exception()
ENDPROC
