-> lets use a STACK!

MODULE 'class/stack'

PROC main() HANDLE
  DEF s=NIL:PTR TO stack,a
  NEW s.stack()				-> amaaazing! :-)
  FOR a:=1 TO 10 DO s.push(a)
  FOR a:=1 TO 11 DO WriteF('element = \d\n',s.pop())
EXCEPT DO
  END s
  IF exception="estk" THEN WriteF('You underflowed the stack!\n')
ENDPROC
