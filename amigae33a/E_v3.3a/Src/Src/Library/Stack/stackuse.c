/* This uses an E stack-class from C
   the pragmas were generated from the .fd file outputted by
   `ShowModule -c stack.m'
   (finally true OO programming for C! ;-)
*/

#include "stack_pragmas.h"
#include <stdio.h>

int main() {
  int s,a,stackbase;
  if(stackbase=OpenLibrary("stack.library",0)) {
    if(s=Stack()) {
      for(a=1;a<=12;a++) Push(s,a);
      while(!Isempty(s)) printf("element = %ld\n",Pop(s));
      End(s);
    };
    CloseLibrary(stackbase);
  };
  return 0;
};
