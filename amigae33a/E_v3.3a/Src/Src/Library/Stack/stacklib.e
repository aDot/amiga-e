/* A stack library. uses the stack class. */

LIBRARY 'stack.library',1,1,'bla' IS stack,end,push,pop,isempty

MODULE 'class/stack'

PROC stack() HANDLE
  DEF x=NIL:PTR TO stack
  NEW x.stack()
EXCEPT DO			-> prevent "NEW" exceptions being thrown out of the library
ENDPROC x

PROC end(st:PTR TO stack)
  END st
ENDPROC

PROC push(st:PTR TO stack,v) HANDLE
  st.push(v)
EXCEPT DO
ENDPROC

PROC pop(st:PTR TO stack) IS st.pop()
PROC isempty(st:PTR TO stack) IS st.is_empty()

PROC main()
ENDPROC
