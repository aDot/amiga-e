OPT MODULE
OPT EXPORT

CONST POINTERA_DUMMY=$80039000,
      POINTERA_BITMAP=$80039001,
      POINTERA_XOFFSET=$80039002,
      POINTERA_YOFFSET=$80039003,
      POINTERA_WORDWIDTH=$80039004,
      POINTERA_XRESOLUTION=$80039005,
      POINTERA_YRESOLUTION=$80039006,
      POINTERXRESN_DEFAULT=0,
      POINTERXRESN_140NS=1,
      POINTERXRESN_70NS=2,
      POINTERXRESN_35NS=3,
      POINTERXRESN_SCREENRES=4,
      POINTERXRESN_LORES=5,
      POINTERXRESN_HIRES=6,
      POINTERYRESN_DEFAULT=0,
      POINTERYRESN_HIGH=2,
      POINTERYRESN_HIGHASPECT=3,
      POINTERYRESN_SCREENRES=4,
      POINTERYRESN_SCREENRESASPECT=5

