/* big_host.rexx */

say "love( 69, amor ) returns"			"'" || love( 69, amor ) || "'"

say "hate( you, 666, odio ) returns"	"'" || hate( you, 666, odio ) || "'"

say "displaybeep( 20 ) returns"			"'" || displaybeep( 20 ) || "'"

say "glow() returns"					glow()

say "rats() returns"					"'" || rats() || "'"

say WBenchToBack() WBenchToFront() WBenchToBack() WBenchToFront()


address 'big_host' 'bye'

/* or like this (which is slightly crazy): */

/* call go_away() */
