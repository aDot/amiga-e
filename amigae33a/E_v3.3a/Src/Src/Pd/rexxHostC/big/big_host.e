->
->  big_host.e
->
->  ARexx host (i.e. server) with bunch of totally useless functions...
->  written in E v3.0 as a demo showing limitless possiblities of the...
->  rexxHostC class
->
->  Public Domain  �  by Piotr Obminski  �  27-Dec-94 20:34:44
->
->
->                   !   A M I G A    F O R E V E R   !
->
->

MODULE  '*rexxHostC'


DEF rx_obj  : PTR TO rexxHostC


PROC main() HANDLE
    DEF cb_list : PTR TO LONG

    cb_list := [    'LOVE',         { love },
                    'HATE',         { hate },
                    'SAY_PIPI',     { beep },
                    'DISPLAYBEEP',  { beep },
                    'GLOW',         { glow },
                    'NOTHING',      NIL,
                    'WBENCHTOBACK', `WbenchToBack() BUT 'OK',
                    'WBENCHTOFRONT', `WbenchToFront() BUT 'OK',
                    'GO_AWAY',      { go_away },
                    'RATS',         `'BIG RODENTS!',    -> <-- see this?!
                    'ODD',          { odd } ]


    NEW rx_obj.rexxHostC( cb_list, 'big_host', -51, 
							`PrintF( 'Setup completed, now try me!\n' )  )


EXCEPT DO

    SELECT exception
        CASE 0
            PrintF( 'You\ave aborted...\n' )
        CASE ERR_NOTUNIQUE
            PrintF( 'It seems that I\am ALREADY RUNNING...\n' )
        CASE ERR_LISTNIL
            PrintF( 'I\ave got NIL for CALLBACK LIST!\n' )
        CASE ERR_NOTADDED
            PrintF( 'HOST could NOT be ADDED!\n' )
        CASE ERR_BADLIST
            PrintF( 'Check your CALLBACK LIST!\n' )
        CASE "MEM"
            PrintF( 'Memory!\n' )
        CASE "REXX"
            PrintF( 'I need rexxsyslib.library!\n' )
        DEFAULT
            PrintF( '? ? ?\n' )         -> this should never happen
    ENDSELECT
ENDPROC

-> ------------------------ callbacks -----------------------------

->
-> just says how many arguments it got and displays them, to do that
-> it calls another callback
->
PROC love()
    PrintF( 'love() here calling hate()...\n' )
    hate()                                  -> just call another callback
ENDPROC 'it''s "k�rlek" in Swedish'


->
-> just says how many arguments it got and displays them
->
PROC hate()
    DEF num, i

    num := rx_obj.getNumArgs()

    PrintF( 'hate() here, I got \d args!\n', num )

    FOR i := 1 TO num
        PrintF( 'arg #\d = \s\n', i, rx_obj.getStr( i ) )
    ENDFOR
ENDPROC 'it''s "hat" in Swedish'


->
-> Intuition support -- wow!
->
PROC beep()
    DEF i, how_many = 3, fGotArg = FALSE

    IF rx_obj.getNumArgs() > 0 THEN how_many, fGotArg := rx_obj.getNum( 1 )

    FOR i := 1 TO 3
        DisplayBeep( NIL )
        Delay( 10 )
    ENDFOR

    IF ( fGotArg = TRUE ) AND ( how_many = 3 )
        RETURN 'here you are: as many beeps as you requested!'
    ELSE
        RETURN 'you\ave got default number of beeps which is 3!'
    ENDIF
ENDPROC


->
-> some hardware-generated colors in inline assembly
->
PROC glow()
    DEF i

    FOR i := 0 TO 100000
        MOVE.W  $DFF006, $DFF180
        BCHG    #1, $BFE001
    ENDFOR
ENDPROC rx_obj.longToStr( i )	-> because Rexx wants strings!


->
-> E's own Odd() for ARexx, it returns '1' for TRUE and '0' for FALSE
-> (which ARexx will see as numbers anyway!), it returns ARexx error
-> RX_WARN (i.e. 5) for bad number of arguments
->
-> NOTE THAT THEY MUST BE RETURNED AS STRINGS FOR AREXX! (which will then
-> treat them as numbers)
->
PROC odd()
    IF rx_obj.getNumArgs() <> 1 THEN RETURN 5, 0
ENDPROC IF Odd( rx_obj.getNum( 1 ) ) THEN '1' ELSE '0'


->
-> this aborts rexxHostC object (we have the internal COMMAND 'BYE',
-> but let's define a FUNCTION doing exactly the same)
->
PROC go_away()
    rx_obj.break()              -> that' how our method break() is used
ENDPROC 'if you must...'
