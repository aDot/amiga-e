/*

fd2module V1.0
Alex McCracken Mar 1994

This program is heavily based on Wouter van Oortmerssen's pragma2module.
In fact about 90% of the code belongs to Wouter, so I claim no credit for
this. However, since Wouter's praga2module works very well for files in
the correct format, I must state that if this fails it is most probably my
fault.  You may use this program as you see fit, however should it fail and
eat your dog, cause your telly to explode, or cause any problems
what-so-ever, I will not be held responsible.  In other word use this at
your own risk.  I have made every effort to ensure it works, but I cannot
guarantee to have found all the niggly little ones that plauge almost all
programs.

Usage

The program in invoked by typing (CLI only):

	fd2module <libname>

where libname is the name of the fd file minus the _lib.fd extension.
This will produce a file <libname>.m .  At the moment the program echos the
fd file as it reads it, but this may change in a future release.  You will
need to give the program the name of the library explicitly, again this may
change.

Distribution

This may be distributed by any means fit. However I retain the right to update
 the package without informing anyone.  This distribution should contain:
	fd2module		The executable
	fd2module.doc		This document

Reaching me

I can be reached in the following ways:

Snail Mail:
	Alex McCracken
	11 Charles Street
	Kilmarnock
	Ayrshire
	KA1 2DX
	Scotland

Internet email:
	mccracal@dcs.gla.ac.uk

I only use my email account during term time so over the summer it is probally
best to write to me by snail mail.  The email address should remain valid until
summer '95.

*/

/* FD2Module
   convert a library fd file to an E module.
   Usage: fd2module <file>
   converts <file_lib.fd> to <file.m>                                  */
   
ENUM INPUT_ERROR=10,OUTPUT_ERROR,FORMAT_ERROR

DEF cfh,efh,eof,done,
    gotbase=FALSE,
    public=TRUE,
    offset=30,
    cfile[200]:STRING,
    efile[200]:STRING,
    cstring[200]:STRING

PROC main()
  StrCopy(cfile,arg,ALL)
  StrAdd(cfile,'_lib.fd',ALL)
  StrCopy(efile,arg,ALL)
  StrAdd(efile,'.m',ALL)
  WriteF('Amiga E FD2Module\nconverting: "\s" to "\s"\n',cfile,efile)
  IF (cfh:=Open(cfile,OLDFILE))=0 THEN closeall(INPUT_ERROR)
  IF (efh:=Open(efile,NEWFILE))=0 THEN closeall(OUTPUT_ERROR)
  REPEAT
    eof:=ReadStr(cfh,cstring)
    done:=convert(cstring)
  UNTIL eof OR done
  WriteF('last offset: -\d\n',offset)
  Out(efh,$FF)
  WriteF('Done.\n')
  closeall(0)
ENDPROC

PROC closeall(er)
  IF cfh<>0 THEN Close(cfh)
  IF efh<>0 THEN Close(efh)
  SELECT er
    CASE INPUT_ERROR;  WriteF('Could not open input file!\n')
    CASE OUTPUT_ERROR; WriteF('Could not open output file!\n')
    CASE FORMAT_ERROR; WriteF('Function definition file format error!\n')
  ENDSELECT
  CleanUp(er)
ENDPROC

/* format of line to convert:
   ##base _<Basename>
     or
   ##bias <offset>
     or
   ##public
     or
   ##private
     or
   ##end
     or
   * <comment>
     or
   <funcname>(<paramlist>)(<reglist>)*/

PROC convert(str)
DEF	pos,pos2,off2,len,narg,a,empty,dstr[50]:STRING,basestr[50]:STRING,
	funcstr[50]:STRING,regstr[20]:STRING,libstr[50]:STRING,
	tstr[80]:STRING,t2str[80]:STRING,t3str[80]:STRING,reg,check
  MidStr(tstr,str,TrimStr(str)-str,ALL)
  LowerStr(tstr)
  WriteF('\s\n',str)
  IF StrCmp(tstr,'##base ',STRLEN) OR StrCmp(tstr,'##base\t',STRLEN)
    pos:=STRLEN
    pos2:=InStr(tstr,'_',0)
    IF pos2=-1 THEN closeall(FORMAT_ERROR)
    IF gotbase=FALSE
      gotbase:=TRUE
      MidStr(basestr,str,(pos2+1),ALL)
      LowerStr(basestr)
      WriteF('Base will be: \s\n',basestr)
      WriteF('Correct name of this library (with the ".library" or ".device"):\n>')
      ReadStr(stdout,libstr)
      Write(efh,["EM","OD",6]:INT,6)
      Write(efh,libstr,EstrLen(libstr)+1)
      Write(efh,basestr,EstrLen(basestr)+1)
    ENDIF
  ELSEIF StrCmp(tstr,'##bias ',STRLEN) OR StrCmp(tstr,'##bias\t',STRLEN)
    pos:=STRLEN
    MidStr(t2str,tstr,pos,ALL)
    pos2:=TrimStr(t2str)
    MidStr(t3str,t2str,pos2-t2str,ALL)
    off2:=Val(t3str,NIL)
    IF off2=0 THEN closeall(FORMAT_ERROR)
    WHILE off2<>offset
      Write(efh,'Dum',3)                     /* "empty function slots" */
      Out(efh,16)
      IF offset>off2 THEN closeall(FORMAT_ERROR)
      offset:=offset+6
    ENDWHILE
  ELSEIF StrCmp(tstr,'##private',ALL)
    public:=FALSE
  ELSEIF StrCmp(tstr,'##public',ALL)
    public:=TRUE
  ELSEIF StrCmp(tstr,'##end',ALL)
    RETURN TRUE
  ELSEIF StrCmp(tstr,'*',STRLEN)
    NOP
  ELSE
    IF public
      pos:=0
      pos2:=InStr(str,'(',pos)
      IF pos2=-1 THEN closeall(FORMAT_ERROR)
      MidStr(funcstr,str,pos,pos2-pos)
      IF funcstr[0]>="a" THEN funcstr[0]:=funcstr[0]-32
      IF funcstr[1]<"a" THEN funcstr[1]:=funcstr[1]+32
      Write(efh,funcstr,EstrLen(funcstr))
      pos:=pos2+1
      pos2:=InStr(str,'(',pos)
      IF pos2=-1 THEN closeall(FORMAT_ERROR)
      narg:=0
      MidStr(dstr,str,pos2+1,ALL)
      UpperStr(dstr)
      WHILE StrCmp(dstr,')',1)=FALSE
        IF EstrLen(dstr)<2 THEN closeall(FORMAT_ERROR)
        MidStr(regstr,dstr,0,2)
        IF StrCmp(regstr,'D',1) OR StrCmp(regstr,'A',1)
          IF StrCmp(regstr,'D',1)
            reg:=0
          ELSEIF StrCmp(regstr,'A',1)
            reg:=8
          ENDIF
          MidStr(regstr,regstr,1,ALL)
          reg:=reg+Val(regstr,{check})
          IF check<1 THEN closeall(FORMAT_ERROR)
        ELSE
          closeall(FORMAT_ERROR)
        ENDIF
        MidStr(dstr,dstr,2,ALL)
        IF StrCmp(dstr,',',1) OR StrCmp(dstr,'/',1)
          MidStr(dstr,dstr,1,ALL)
        ENDIF
        Out(efh,reg)
        INC narg
      ENDWHILE
      IF narg=0 THEN Out(efh,16)
      offset:=offset+6
    ELSE
      Write(efh,'Dum',3)
      Out(efh,16)
      offset:=offset+6
    ENDIF
  ENDIF
ENDPROC FALSE
