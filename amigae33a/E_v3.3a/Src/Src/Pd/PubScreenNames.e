/* PubScreenNames.e
 * Prints the names (and some other attributes) of the Public Screens actually
 * opened. Also, tests for the presence of a particular screen.
 *
 * Public Domain by Diego Caravana.
 *
 *
 * Thanks to Wouter van Oortmerssen for his VERY GOOD work, hoping that he
 * will continue to improve E language !
 *
 * Hey, consider to learn E !!!
 *
 */

/* TAB=4 */

OPT OSVERSION=37

/* Here is OBJECT we use for copying data inherent to Public Screen. Copying
 * data is necessary because the locking of PubScreen List must be as short as
 * possible (see Commodore Autodocs). This is due to the fact that the list
 * cannot be modified (i.e. no public screen can be opened/closed) while it is
 * locked by a task.
 */

OBJECT mypubscreen
	name, flags:INT, visitorcnt:INT, screen, task, next
ENDOBJECT

MODULE 'intuition/screens', 'exec/lists', 'exec/nodes'

CONST ARGS_NUM=2
ENUM  ARG_FULL, ARG_EXISTS

ENUM ERR_NONE=0, ERR_PUBLOCK, ERR_MEM, ERR_WRONGARGS, OK_FOUND, OK_NOTFOUND

DEF	localpslist:mypubscreen, strflags[25]:STRING, args[ARGS_NUM]:ARRAY OF LONG

RAISE ERR_MEM		IF String()=NIL,
	  ERR_PUBLOCK	IF LockPubScreenList()=NIL,
	  ERR_WRONGARGS IF ReadArgs()=NIL,
	  ERR_MEM		IF New()=NIL

PROC main() HANDLE
DEF pslist:PTR TO lh, psnode:PTR TO ln, i=0, localpsnode:PTR TO mypubscreen,
	pubscreen:PTR TO pubscreennode, publock=NIL, rdargs=NIL

	VOID '$VER: PubScreenNames 1.1 (13.02.93) by Diego Caravana'

	rdargs := ReadArgs('FULL/S,EXISTS/K', args, 0)

	/* get the Public Screen List; it will not change while we are reading
	 */
	publock := LockPubScreenList()

	/* the first loop: copy all data we need as fast as possible
     */
	pslist := publock
	psnode := pslist.head
	localpsnode := localpslist

	WHILE psnode.succ <> NIL

		/* test if EXISTS a screen with a specified name
		 */
		IF StrCmp(psnode.name, args[ARG_EXISTS], ALL) THEN Raise(OK_FOUND)

		/* allocate a estring dinamically to reduce memory usegand because
		 * STRINGs are not permitted (actually, I hope :) in OBJECTs
		 */
		localpsnode.name:=String(StrLen(psnode.name)+2)
		StrCopy(localpsnode.name, psnode.name, ALL)
		IF args[ARG_FULL]=-1	/* copy only if needed */
			pubscreen:=psnode
			localpsnode.flags:=pubscreen.flags
			localpsnode.visitorcnt:=pubscreen.visitorcount
			localpsnode.task:=pubscreen.sigtask
			localpsnode.screen:=pubscreen.screen
		ENDIF

		/* allocate a OBJECT to contain the informations about the next
		 * screen
		 */
		localpsnode.next:=New(SIZEOF mypubscreen)

		/* change the pointers to examine the chain
		 */
		localpsnode:=localpsnode.next
 		psnode:=psnode.succ
	ENDWHILE

	UnlockPubScreenList()
	publock:=NIL	/* to know that we have released the lock */

	/* at this point, if a screen name was specified with EXISTS, it has not
	 * been found as a Public Screen in the previous loop, so we can  surely
	 * exit with a null return code
	 */
	IF StrLen(args[ARG_EXISTS]) <> 0 THEN Raise(OK_NOTFOUND)

	/* print header with description of fields
     */
	IF args[ARG_FULL]=-1	/* print the right one! */
		WriteF('\n N. Name               Visitors Screen     Task       Flags\n')
		WriteF(  ' ---------------------------------------------------------------------------\n')
	ELSE
		WriteF('\n N. Name\n')
		WriteF(  ' ----------------------\n')
	ENDIF

	/* the second loop: print all the data
	 */
	localpsnode:=localpslist
	WHILE localpsnode.next <> NIL
		i++

		IF args[ARG_FULL]=-1	/* choose the infos to print */

			/* not-so-simple code: the two flags are independent by one another
			 * and also we want a "|" (OR in C) put between them; then, there
			 * is a default string which is used when no flag is set
			 */
			StrCopy(strflags,'<No Flags Set>',ALL)
			IF localpsnode.flags AND SHANGHAI
				StrCopy(strflags,'SHANGHAI',ALL)
				IF localpsnode.flags AND POPPUBSCREEN
					StrAdd(strflags,'|POPPUBSCREEN',ALL)
				ENDIF
			ELSE
				IF localpsnode.flags AND POPPUBSCREEN
					StrCopy(strflags,'POPPUBSCREEN',ALL)
				ENDIF
			ENDIF

			WriteF(' \l\d[2] \l\s[18]    \l\d[3]   $\z\h[8]  $\z\h[8]  \l\s\n',
				i, localpsnode.name, localpsnode.visitorcnt,
				localpsnode.screen, localpsnode.task, strflags )
		ELSE
			WriteF(' \l\d[2] \l\s[18]\n', i, localpsnode.name)
		ENDIF

		localpsnode:=localpsnode.next
	ENDWHILE
	WriteF('\n Found \d Public Screen(s)\n\n', i)

	Raise(ERR_NONE)

EXCEPT
	IF publock THEN UnlockPubScreenList()
    IF rdargs THEN FreeArgs(rdargs)

	SELECT exception
		CASE ERR_NONE;
		CASE OK_FOUND;		CleanUp(5)		/* if WARN will be true */
		CASE OK_NOTFOUND;	CleanUp(0)
		CASE ERR_PUBLOCK;	WriteF('*** cannot obtain PubScreen infos!\n')
		CASE ERR_MEM;		WriteF('*** no memory!\n')
		DEFAULT;			PrintFault(IoErr(), '*** Error')
	ENDSELECT

	IF exception THEN CleanUp(10)
	CleanUp(0)

ENDPROC
