;/* MakeKickFile.e - Execute me to compile with Amiga E v2.1
ec MakeKickFile
quit
*/
/*
**      $Filename: MakeKickFile.e $
**      $Release: 1.1 $
**
**      (C) Copyright 1991-1993 Jaba Development.
**          Written by Jan van den Baard
**
**      Create a rom-kick file of the machine it is run on.
**      It can create 256K and 512K ROM files.
**
**      This is a direct translation from my original C code
**      and the first thing I wrote in Amiga E. Therefore it
**      might not make full use of E it's special features.
**      Please forgive me for that.
**
**      The usage is simple. You pass this program a filename
**      and, if the rom size is known, it will write the ROM
**      of the machine to the file. It writes the ROM, *NOT*
**      a soft-kicked rom.
**
**      The output can be used by setcpu or skick to boot up
**      a machine with the ROM file.
**
**      I have successfully tested it with an A1200 ROM on
**      a A2500.
**
**      No testing has been made with a 1.2/1.3 ROM file.
**      I don't have access to a 1.2/1.3 machine.
**
**      This really is a hack...but a hack that works!
**/

MODULE  'dos/dos'

/* ROM size constants and magic cookies */
CONST   SMALLROM = $00040000, SMALLMAGIC = $11114EF9,
        BIGROM   = $00080000, BIGMAGIC   = $11144EF9

/* C= RomFileHeader structure */
OBJECT  romfileheader
    alwaysnil :  LONG       /* like it says -: always NIL */
    romsize   :  LONG       /* the size of the ROM */
ENDOBJECT

/* only three excpetion errors... */
ENUM    ER_USAGE=1, ER_UNKNOWN, ER_IO

/* here we go... */
PROC main() HANDLE
    DEF outfile = NIL,
        base    = NIL : PTR TO LONG,
        size    = NIL,
        len     = NIL,
        rk, rfh       : romfileheader

    WriteF( '\e[1mMakeKickFile version 1.1 - (C) 1991-1993 Jaba Development\e[0m\n' );

    /* no args or "?" as arg then print the usage */
    IF StrCmp( arg, '', 1 ) OR StrCmp( arg, '?', 2 ) THEN Raise( ER_USAGE )

    /* first try for a 512 KB ROM */
    base := $00F80000

    /* if the magic cookie isn't BIGMAGIC then it must be a 256 KB ROM */
    IF ( base[ 0 ] <> BIGMAGIC ) THEN base := $00FC0000

    /* let's see what ROM we are dealing with */
    rk := base[ 0 ]

    SELECT rk
        CASE    SMALLMAGIC; size := SMALLROM        /* 256 KB */
        CASE    BIGMAGIC;   size := BIGROM          /* 512 KB */
        DEFAULT;            Raise( ER_UNKNOWN )     /* unknown... */
    ENDSELECT

    /* show the ROM size */
    WriteF( 'ROM Size = \dKByte.\n', size / 1024 )

    /* open the kick-file */
    IF ( outfile := Open( arg, MODE_NEWFILE ))
        WriteF( 'Writing KickFile image...\n' )

        rfh.alwaysnil := NIL        /* set this field to NIL */
        rfh.romsize   := size       /* put the ROM size here */

        len := Write( outfile, rfh, 8 ) /* write the romfileheader */
        len := len + Write( outfile, base, size )   /* write the rom */

        Close( outfile ) /* close the kick-file */

        /* was the writing successfull? */
        IF ( ( len <> ( size + 8 )) OR IoErr() ) THEN Raise( ER_IO )
    ELSE
        /* could not open the file! */
        WriteF( 'Unable to open the output file!\n' )
    ENDIF
EXCEPT
    /* exception handler for when no output name was given
       or the ROM is not recognized or an IO error. */
    SELECT exception
        CASE    ER_USAGE;   WriteF( 'Usage: MakeKickFile <romfilename>\n' )
        CASE    ER_UNKNOWN; WriteF( 'Unknown ROM! Exiting...\n' )
        DEFAULT;            PrintFault( IoErr(), 'Error -' )
    ENDSELECT
ENDPROC IoErr()
