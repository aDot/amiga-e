/* example of using built-in font for images. 'paint.m' converted
   from font-file with font2obj (aminet?) and o2m
*/

OPT OSVERSION=37

MODULE 'tools/clonescreen', '*paint', 'libraries/diskfont'

PROC main() HANDLE
  DEF screen=NIL,font=NIL,win=NIL,tf:PTR TO diskfontheader
  tf:={paintf}; tf:=tf.tf
  screen,font:=openclonescreen('Workbench','bla')
  win:=backdropwindow(screen)
  SetFont(stdrast:=screen+84,tf)
  TextF(200,40,'A B C D E F G H ')
  EasyRequestArgs(win,[20,0,'um','uhhh','zure'],0,NIL)
EXCEPT DO
  closeclonescreen(screen,font,win)
  SELECT exception
    CASE "SCR"; WriteF('no screen!\n')
    CASE "WIN"; WriteF('no window!\n')
  ENDSELECT
ENDPROC
