-> 3d geometry stuff

OPT MODULE

EXPORT OBJECT point
  x,y,z
ENDOBJECT

EXPORT OBJECT opoint OF point		-> point with orientation e.g. a camera
  turn,up,tilt
ENDOBJECT

PROC sin(x,f) IS !Fsin(x!/1800.0*3.14159)*(f!)!		-> replace by tables!
PROC cos(x,f) IS !Fcos(x!/1800.0*3.14159)*(f!)!
PROC atan(x,unit) IS !Fatan(x!/(unit!))/3.14159*1800.0!
PROC atanf(x,u) IS IF u>0 THEN atan(x,u) ELSE (IF u=0 THEN (IF x>0 THEN 900 ELSE -900) ELSE 1800-atan(x,-u))

EXPORT PROC project3d(camera:PTR TO opoint,p:PTR TO point,midx,midy,persp)
  DEF x,y,z,t,old,sx,sy
  x:=p.x-camera.x
  y:=p.y-camera.y
  z:=camera.z-p.z
  IF t:=camera.turn*10
    x:=cos(t,old:=x)-sin(t,y)
    y:=sin(t,old)+cos(t,y)
  ENDIF
  IF t:=camera.up*10
    z:=cos(t,old:=z)-sin(t,y)
    y:=sin(t,old)+cos(t,y)
  ENDIF
  IF t:=camera.tilt*10
    x:=cos(t,old:=x)-sin(t,z)
    z:=sin(t,old)+cos(t,z)
  ENDIF
  sx:=atanf(x,y)*(midx*2)/persp/10+midx
  sy:=atanf(z,y)*(midy*2)/persp/10+midy
ENDPROC sx,sy
