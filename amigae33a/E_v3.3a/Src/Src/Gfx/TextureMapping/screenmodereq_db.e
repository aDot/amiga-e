-> asl screenmode module for db

OPT OSVERSION=38, MODULE
OPT EXPORT

MODULE 'asl', 'libraries/asl', 'intuition/screens', 'tools/scrbuffer'

PROC openreqscreen(xs,ys,depth,title) HANDLE
  DEF fr:PTR TO screenmoderequester,scr:PTR TO screen
  IF (aslbase:=OpenLibrary('asl.library',37))=NIL THEN Raise("ASL")
  IF (fr:=AllocAslRequest(ASL_SCREENMODEREQUEST,NIL))=NIL THEN Raise("REQ")
  IF AslRequest(fr, NIL)
    IF (scr:=sb_OpenScreen(
      [SA_WIDTH,      xs,
       SA_HEIGHT,     ys,
       SA_DEPTH,      depth,
       SA_TYPE,       CUSTOMSCREEN,
       SA_QUIET,      TRUE,
       SA_OVERSCAN,   OSCAN_TEXT,
       SA_AUTOSCROLL, TRUE,
       SA_DISPLAYID,  fr.displayid,
       SA_TITLE,      title,
       NIL],0))=NIL THEN Raise("SCR")
    RETURN scr
  ENDIF
EXCEPT DO
  IF fr THEN FreeAslRequest(fr)
  IF aslbase THEN CloseLibrary(aslbase)
  ReThrow()
ENDPROC NIL

PROC closereqscreen(scr)
  IF scr THEN sb_CloseScreen(scr)
ENDPROC
