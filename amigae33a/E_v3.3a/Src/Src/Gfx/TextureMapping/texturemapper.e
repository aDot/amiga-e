-> texturemapper

OPT MODULE

CONST FP=256,FPS=8,MAXROUND=2500

PROC scanline(x1,x2,tx1,tx2,ty1,ty2,thisy,cbuf,maxx,tmap)
  DEF p,numpix,c:REG,stx:REG,sty:REG,cx:REG,cy,misx=0
  IF (x1>=maxx) OR (x2<0) THEN RETURN
  numpix:=x2-x1+1
  IF x1<0 THEN (misx:=0-x1) BUT x1:=0
  IF x2>=maxx THEN x2:=maxx-1
  p:=thisy*maxx+x1+cbuf
  ->IF numpix=0 THEN Raise("nump")
  stx:=tx2-tx1*FP/numpix
  sty:=ty2-ty1*FP/numpix
  cx:=tx1*FP+(misx*stx)
  cy:=ty1*FP+(misx*sty)
  c:=x2-x1
  MOVE.L p,A0
  MOVE.L tmap,A1
  ROR.W #8,cx
  ROR.W #8,stx
  SUB.B stx,cx
  ADD.W stx,cx
  MOVE.L cy,A2			-> A2 is copy of cy
inner:
  MOVE.L A2,D0			-> p[]++:=tmap[(cy/FP)*tmaxx+(cx/FP)]
  ADD.L sty,A2			-> cy:=cy+sty
  MOVE.B cx,D0			-> assumes FPS and tmaxx are 256
  ADDX.W stx,cx			-> cx:=cx+stx
  MOVE.B 0(A1,D0.L),(A0)+
  DBRA c,inner
ENDPROC

EXPORT PROC tmap(coords:PTR TO LONG,cbuf,maxx,maxy,tmap,tmaxx=256,tmaxy=256)
  DEF rx[MAXROUND]:ARRAY OF LONG,ry[MAXROUND]:ARRAY OF LONG,trx[MAXROUND]:ARRAY OF LONG,
      try[MAXROUND]:ARRAY OF LONG,curr=0,a,b,dist,q,
      x1,y1,x2,y2,tmx,tmy,f,stx,sty,outm1,outm2,outmi1=10000,outmi2=-10000,sides=0
  FOR a:=0 TO 6 STEP 2				-> for all 4 sides of the square
    y1:=coords[a+1]
    y2:=coords[a+3 AND 7]
    IF dist:=Abs(y1-y2)
      sides++
      x1:=coords[a]
      x2:=coords[a+2 AND 7]
      IF Abs(x1-x2)+dist>2000 THEN RETURN	-> temp check
      tmx:=IF a AND 2 THEN 0 ELSE tmaxx
      tmy:=IF a AND 2 THEN tmaxy ELSE 0
      f:=IF a>=4 THEN -1 ELSE 1
      stx:=IF a+2 AND 4 THEN tmaxx ELSE 0
      sty:=IF a>=4 THEN tmaxy ELSE 0
      ->IF dist=0 THEN Raise("dist")
      FOR q:=1 TO dist				-> for each y pixel of a side
        rx[curr]:=x2-x1*q/dist+x1
        ry[curr]:=y1+(q*Sign(y2-y1))
        trx[curr]:=stx+(q*tmx/dist*f)
        try[curr]:=sty+(q*tmy/dist*f)
        curr++
      ENDFOR
      IF y2<outm1 THEN (outm1:=y2) BUT outmi1:=curr-1
      IF y2>outm2 THEN (outm2:=y2) BUT outmi2:=curr-1
    ENDIF
  ENDFOR
  IF sides<1 THEN RETURN
  IF outmi1>outmi2
    a:=outmi2
    outmi2:=outmi1
    outmi1:=a
  ENDIF
  FOR a:=outmi1 TO outmi2			-> for each scanline found
    b:=IF a-outmi1<=outmi1 THEN outmi1-(a-outmi1) ELSE outmi2-(a-outmi2)
    IF (ry[a]>=0) AND (ry[a]<maxy)
      IF rx[b]>rx[a]
        scanline(rx[a],rx[b],trx[a],trx[b],try[a],try[b],ry[a],cbuf,maxx,tmap)
      ELSE
        scanline(rx[b],rx[a],trx[b],trx[a],try[b],try[a],ry[a],cbuf,maxx,tmap)
      ENDIF
    ENDIF
  ENDFOR
ENDPROC
