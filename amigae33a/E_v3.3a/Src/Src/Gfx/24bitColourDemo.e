/* 24bit color demo. works only on AGA machines with kick3 

A small demo demonstrating the new 24bit palette of the newer AA
Amigas. You'll need such an amiga and kick3 to run this demo, ofcourse.
The demo will draw lines in 256 tints and less on the screen, and then
lets you manipulate the colour-registers by pressing the leftMB on some
(x,y) spot on the screen. These coordinates will serve as RGB values.
To quit, press the rightMB.

*/

OPT OSVERSION=39

CONST X=319, Y=255

PROC main()
  DEF s,win,x,y,z
  IF s:=OpenS(X+1,Y+1,8,0,'bla')
    IF win:=OpenW(0,0,X+1,Y+1,0,0,'bla',s,15,0)
      FOR x:=0 TO 255 DO SetColour(s,x,x,x,x)
      FOR y:=0 TO Y DO Line(0,y,63,y,y AND $FF)
      FOR y:=0 TO Y DO Line(64,y,127,y,y AND $FE)
      FOR y:=0 TO Y DO Line(128,y,191,y,y AND $FC)
      FOR y:=0 TO Y DO Line(192,y,255,y,y AND $F8)
      FOR y:=0 TO Y DO Line(256,y,319,y,y AND $F0)
      SetDrMd(stdrast,0)
      SetTopaz(8)
      TextF(0,30,' Press leftMB on some (x,y) spot')
      TextF(0,40,' Press (0,0) to leave.')
      TextF(0,50,' #of colours:')
      TextF(0,60,'   256     128     64      32      16   ')
      TextF(0,70,' #bits colour:')
      TextF(0,80,'   24      21      18      15      12   ')
      TextF(0,90,'   AGA     AGA     AGA     AGA     ECS  ')
      WHILE TRUE
        WaitLeftMouse(win)
        y:=MouseX(win)*4/5
        z:=MouseY(win)
      EXIT y+z<4
        FOR x:=0 TO 255 DO SetColour(s,x,y,x,z)
      ENDWHILE
      CloseW(win)
    ENDIF
    CloseS(s)
  ENDIF
ENDPROC
