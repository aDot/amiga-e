-> silly little simulation

OPT REG=5

MODULE 'intuition/intuition', 'exec/nodes', 'exec/lists', 'tools/constructors'

OBJECT creature OF mln
  x,y
  loc:PTR TO LONG
ENDOBJECT

DEF creatures=NIL:PTR TO LONG,creaturelist=NIL:PTR TO mlh,num_creatures=100

CONST SPACE_XS=64,
      SPACE_YS=64,
      SPACE_X=13,
      SPACE_Y=28,
      XBL=2,
      YBL=2,
      AND_X=$3f,
      AND_Y=$3f

CONST XSI=XBL*SPACE_XS,
      YSI=YBL*SPACE_YS

PROC main() HANDLE
  DEF w=NIL:PTR TO window,m:PTR TO intuimessage,c=0,a,cc=0
  IF (a:=Val(arg))>1 THEN num_creatures:=a
  IF w:=OpenW(20,20,XSI+42,YSI+42,IDCMP_CLOSEWINDOW OR IDCMP_MOUSEBUTTONS,$F,
         arg,->'BacteriaMania',
         NIL,1,NIL)
    Box(SPACE_X-1,SPACE_Y-1,SPACE_XS*XBL+1+SPACE_X,SPACE_YS*YBL+1+SPACE_Y,1)
    Box(SPACE_X,SPACE_Y,SPACE_XS*XBL+SPACE_X,SPACE_YS*YBL+SPACE_Y,0)
    FOR a:=1 TO num_creatures DO new_creature()
    REPEAT
      move_creatures()
      WHILE m:=GetMsg(w.userport)
        IF (c:=m.class)=IDCMP_MOUSEBUTTONS
        ENDIF
      ENDWHILE
    UNTIL c=IDCMP_CLOSEWINDOW
  ENDIF
EXCEPT DO
  CloseW(w)
ENDPROC

PROC move_creatures()
  DEF cr:PTR TO creature,d:PTR TO LONG,x,y,a,b,c,e,aa,bb,nbl=NIL:PTR TO LONG,nb:PTR TO creature,numnb
  cr:=creaturelist.head
  WHILE cr.succ
    a:=0					-> check were to move
    numnb:=0
    c:=-1
    FOR a:=-1 TO 1
      FOR b:=-1 TO 1
        aa:=a+cr.x; bb:=b+cr.y
        d:=addrxy(aa,bb)
        IF (d[]=NIL) AND (d[]<>cr)
          IF (e:=countcrowd(aa,bb))>=c
          ->IF (e:=countcrowd(d))>=c
            IF IF e=c THEN Rnd(3)=0 ELSE TRUE
              c:=e; x:=aa; y:=bb
            ENDIF
          ENDIF
        ENDIF
      ENDFOR
    ENDFOR
    d:=IF c>=0 THEN addrxy(x,y) ELSE 1
    IF d[]=NIL					-> if possible, move
      d[]:=cr
      cr.loc[]:=NIL
      cr.loc:=d
      plotxy(x,y,2)
      plotxy(cr.x,cr.y,0)
      cr.x:=x
      cr.y:=y
    ENDIF
    cr:=cr.succ
  ENDWHILE
ENDPROC

PROC countcrowd(x,y)
  DEF n=0,a,b,d:PTR TO LONG
  FOR a:=-1 TO 1 DO FOR b:=-1 TO 1 DO IF (d:=addrxy(x+a,y+b)) BUT d[] THEN n++
ENDPROC n

PROC new_creature()
  DEF d:PTR TO LONG,a,x,y,cr:PTR TO creature
  IF creatures=NIL
    NEW creatures[SPACE_YS]
    FOR a:=0 TO SPACE_YS-1 DO creatures[a]:=NEW d[SPACE_XS]
    creaturelist:=newlist()
  ENDIF
  REPEAT
    d:=addrxy(x:=Rnd(SPACE_XS),y:=Rnd(SPACE_YS))
  UNTIL d[]=NIL
  d[]:=NEW cr
  AddHead(creaturelist,cr)
  cr.loc:=d
  cr.x:=x
  cr.y:=y
  plotxy(x,y,3)
ENDPROC

PROC addrxy(x,y) IS x AND AND_X*SIZEOF LONG+creatures[y AND AND_Y]

PROC plotxy(x,y,c)
  DEF xx,yy
  xx:=x AND AND_X*XBL+SPACE_X
  yy:=y AND AND_Y*YBL+SPACE_Y
  IF XBL+YBL=2 THEN Plot(xx,yy,c) ELSE Box(xx,yy,XBL-1+xx,YBL-1+yy,c)
ENDPROC
