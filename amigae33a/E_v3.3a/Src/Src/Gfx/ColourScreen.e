/* _Very_ simple customscreen-demo */

PROC main()
  DEF screen,sx,sy
  IF screen:=OpenS(321,257,5,0,'My Screen')
    REPEAT
      FOR sx:=0 TO 320
        FOR sy:=0 TO 256 DO Plot(sx,sy,sx*sy+1)
        EXIT Mouse()=1
      ENDFOR
    UNTIL Mouse()=1
    CloseS(screen)
  ENDIF
ENDPROC
