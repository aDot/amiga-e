-> "Rude Racing" or "Wreckless Driving" or "The Need For Traffic Jams"
-> see main() below for the controls.

OPT OSVERSION=39, PREPROCESS

CONST CWIDTH=256,CHEIGHT=128
CONST SCHEIGHT=CHEIGHT+20
CONST TEMPBUFS=CWIDTH*CHEIGHT/2,BUFS=CWIDTH*CHEIGHT

MODULE '*c2p4', '*screenmodereq_db', 'tools/exceptions', 'tools/scrbuffer',
       'intuition/screens', 'graphics/rastport', 'graphics/gfx',
       'intuition/intuition'

/*------------------------------------------------------------------------*/

OBJECT rseg
  bend:INT		-> in degrees, often between -10 and +10, 0=straight
  steepness:INT		-> in degrees, often between -40 and +40, 0=flat
ENDOBJECT

CONST SEGX=256,SEGY=64,		->  in pixels
      LOOKAWAY=3,
      SPEED=3,
      WIDTHSTEP=5,
      HORIZON=21

DEF road:PTR TO rseg,
    roadlen,			-> #of segments
    cp=0,			-> current pos in segments*pixels
    turn=0,			-> in degrees
    rpos=0,			-> in pixels, -128..127
    sintab[271]:ARRAY OF INT,
    tmap

-> both in range -90 to 90

#define sin(x) sintab[(x)+90]
#define cos(x) sintab[(x)+180]

PROC ahead() IS cp:=cp+(SEGY/SPEED)
PROC back() IS cp:=Bounds(cp-(SEGY/SPEED),0,1000000)
PROC tleft() IS turn:=Bounds(turn-3,-60,60)
PROC tright() IS turn:=Bounds(turn+3,-60,60)
PROC pleft() IS rpos:=Bounds(rpos+3,-128,127)
PROC pright() IS rpos:=Bounds(rpos-3,-128,127)
PROC center() IS (turn:=0) BUT (rpos:=0)

PROC render(cbuf)
  DEF cpos,cpospix		-> in segments, pixels in cur segment
  DEF cangle,width=550,middle,ypos,pixstep=100,vp,vpos,vpospix,cbend=0,ccbend,lvpos,lvpospix,cmiddle
  DEF omiddle,nval=0,nposr=0,posadj,te=0,tt=0
  clearmem(cbuf,CWIDTH*HORIZON,$0E0E0E0E)
  clearmem(cbuf+(CWIDTH*HORIZON),BUFS-(CWIDTH*HORIZON),$0F0F0F0F)
  cpospix,cpos:=Mod(cp,SEGY); cpos:=Mod(cpos,roadlen)
  cangle:=road[cpos].steepness
  middle:=CWIDTH/2+(rpos*2)
  ypos:=CHEIGHT-1
  vp:=cp+(LOOKAWAY*SEGY)
  lvpospix,lvpos:=Mod(vp,SEGY); lvpos:=Mod(lvpos,roadlen)
  WHILE width>15
    vpospix,vpos:=Mod(vp,SEGY); vpos:=Mod(vpos,roadlen)
    IF lvpos<>vpos
      cbend:=cbend+(road[lvpos].bend*256)
      IF tt++=0 THEN cbend:=cbend*(SEGY-lvpospix-1)/SEGY
    ENDIF
    ccbend:=cbend+(road[vpos].bend*256*(SEGY-vpospix-1)/SEGY)
    nval,omiddle:=Mod(sin(Bounds(ccbend/256,-90,90))+nval,20000/(vp-cp))
    nposr,posadj:=Mod(WIDTHSTEP*rpos+nposr,256)
    middle:=middle+omiddle-posadj
    cmiddle:=middle+(sin(-turn)*32/ypos)
    rline(cbuf,width,cmiddle,ypos,vpos,vpospix*CWIDTH+tmap)
    ypos--

    width:=width-WIDTHSTEP
    vp:=vp+(pixstep/256)

    ->fake hills
    ->width:=width-WIDTHSTEP+(road[vpos].steepness/10)
    ->vp:=vp+Div(Mul((pixstep/256),100),(100-road[vpos].steepness))

    pixstep:=pixstep/20+pixstep
    lvpos:=vpos
  ENDWHILE
ENDPROC

PROC rline(cbuf,width,middle,ypos,vpos,tm)
  DEF x,pstep,pcur=0,end
  x:=middle-(width/2)
  IF (x>=CWIDTH) OR (x+width<=0) THEN RETURN
  cbuf:=ypos*CWIDTH+cbuf
  pstep:=256*1024/width
  IF x<0
    pcur:=-x*pstep
    x:=0
  ENDIF
  end:=Min(CWIDTH-x,256*1024-pcur/pstep)+cbuf+x
  MOVE.L tm,A0
  MOVE.L cbuf,A1
  ADDA.L x,A1
  MOVE.L end,A2
  MOVE.L pcur,D1
  MOVE.L pstep,D2
  MOVEQ #10,D3
tmloop:
  MOVE.L D1,D0
  LSR.L D3,D0
  MOVE.B 0(A0,D0.L),(A1)+
  ADD.L D2,D1
  CMPA.L A2,A1
  BMI.S tmloop
ENDPROC

PROC init()
  DEF a
  road:=[0,0,0,0,0,0,0,0,0,0,0,0,
         2,0,4,0,
         6,0,8,0,10,0,12,0,15,0,15,0,15,0,15,0,12,0,10,0,8,0,6,0,
         4,0,2,0,
         0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
         -2,0,-4,0,
         -6,0,-8,0,-10,0,-10,0,-8,0,-6,0,
         -4,0,-2,0,
         0,0,0,0,0,0,0,0,0,0,0,0,
         0,10,0,15,0,20,0,30,0,20,0,15,0,10,
         0,0,
         0,0,0,15,0,20,0,-20,0,-15,0,0,0,0,0,0
        ]:rseg
  roadlen:=ListLen(road)/2   -> rseg elems
  FOR a:=-90 TO 180 DO sintab[a+90]:=!Fsin(a!/180.0*3.14159)*255.0!
  tmap:={tmaplab}
ENDPROC

tmaplab: INCBIN 'e:rr/TEXTUREMAP_ROAD.br.chunky'

/*------------------------------------------------------------------------*/

PROC main() HANDLE
  DEF dbs,scr=NIL:PTR TO screen,bm:PTR TO bitmap,win=NIL:PTR TO window,
      tbuf2,tbuf3,tbuf2b,tbuf3b,cbuf,dbuf,dbuf2,sigbit,sig,safe=TRUE,a,temp,
      imsg:PTR TO intuimessage,key,frames=0,dframes,fsec,ssec,smic,esec,emic
  IF (dbs:=openreqscreen(CWIDTH,SCHEIGHT,4,'bla'))=NIL THEN Raise()
  scr:=sb_GetScreen(dbs)
  IF (win:=OpenW(0,0,CWIDTH-1,SCHEIGHT-1,
    IDCMP_MOUSEBUTTONS OR IDCMP_VANILLAKEY,
    WFLG_REPORTMOUSE OR WFLG_BORDERLESS OR WFLG_SIMPLE_REFRESH OR WFLG_BACKDROP OR WFLG_ACTIVATE,
    '',scr,15,NIL))=NIL THEN Raise("WIN")
  tbuf2:=NewM(TEMPBUFS+TEMPBUFS,2)
  tbuf3:=tbuf2+TEMPBUFS
  tbuf2b:=NewM(TEMPBUFS+TEMPBUFS,2)
  tbuf3b:=tbuf2b+TEMPBUFS
  NEW cbuf[BUFS]
  NEW dbuf[BUFS]
  NEW dbuf2[BUFS]
  FOR a:=0 TO BUFS-1
    dbuf[a]:=-1
    dbuf2[a]:=-1
  ENDFOR
  FOR a:=1 TO 13 DO SetColour(scr,15-a-1,a*19,a*19,a*14)
  SetColour(scr,0,212,0,9)
  SetColour(scr,14,171,203,255)
  SetColour(scr,15,156,194,0)
  SetRast(scr.rastport,13)
  bm:=sb_NextBuffer(dbs)
  SetRast(scr.rastport,13)
  init()
  IF (sigbit:=AllocSignal(-1))<>-1
    sig:=Shl(1,sigbit)
    CurrentTime({ssec},{smic})
    REPEAT
      render(cbuf)
      IF safe=FALSE
        Wait(sig)
        safe:=TRUE
      ENDIF
      bm:=sb_NextBuffer(dbs)
      c2p4(tbuf3,tbuf2,cbuf,dbuf,bm.planes,FindTask(NIL),sig,gfxbase)
      frames++
      temp:=dbuf; dbuf:=dbuf2; dbuf2:=temp
      temp:=tbuf2; tbuf2:=tbuf2b; tbuf2b:=temp
      temp:=tbuf3; tbuf3:=tbuf3b; tbuf3b:=temp
      IF imsg:=GetMsg(win.userport)
        IF imsg.class=IDCMP_VANILLAKEY
          key:=imsg.code
          SELECT key
            CASE "a"; ahead()
            CASE "b"; back()
            CASE "z"; tleft()
            CASE "x"; tright()
            CASE "n"; pleft()
            CASE "m"; pright()
            CASE "c"; center()
            ->   "q" quits
          ENDSELECT
        ENDIF
        ReplyMsg(imsg)
      ENDIF
      safe:=FALSE
    UNTIL key="q"
    CurrentTime({esec},{emic})
    IF safe=FALSE THEN Wait(sig)
    FreeSignal(sigbit)
  ENDIF
EXCEPT DO
  IF win THEN CloseWindow(win)
  closereqscreen(dbs)
  dframes:=esec-ssec*100+(emic-smic/10000)
  fsec:=frames*10000/dframes
  IF frames THEN WriteF('calculated \d pictures \d.\z\d[2] seconds, giving \d.\z\d[2] f/s\n',frames,Div(dframes,100),Mod(dframes,100),Div(fsec,100),Mod(fsec,100))
  SELECT exception
    CASE "SCR"; WriteF('no screen!\n')
    CASE "REQ"; WriteF('Error: Could not allocate ASL request\n')
    CASE "ASL"; WriteF('Error: Could not open ASL library\n')
  ENDSELECT
  report_exception()
ENDPROC

PROC clearmem(mem,size,pat)
  DEF e:REG,a:REG,b:REG,c:REG,d:REG
  e:=size/16-1
  a:=b:=c:=d:=pat
  MOVE.L mem,A0
  ADD.L size,A0
clloop:
  MOVEM.L a/b/c/d,-(A0)
  DBRA e,clloop
ENDPROC
