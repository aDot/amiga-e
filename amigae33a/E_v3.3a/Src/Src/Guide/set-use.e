MODULE '*set'

PROC main() HANDLE
  DEF s=NIL:PTR TO set
  NEW s.create(20)
  s.add(1)
  s.add(-13)
  s.add(91)
  s.add(42)
  s.add(-76)
  IF s.member(1) THEN WriteF('1 is a member\n')
  IF s.member(11) THEN WriteF('11 is a member\n')
  WriteF('s = ')
  s.print()
  WriteF('\n')
EXCEPT DO
  END s
  SELECT exception
  CASE "NEW"
    WriteF('Out of memory\n')
  CASE "full"
    WriteF('Set is full\n')
  ENDSELECT
ENDPROC
