MODULE 'workbench/startup'

PROC main()
  DEF startup:PTR TO wbstartup, args:PTR TO wbarg, i, oldlock, len
  IF (startup:=wbmessage)=NIL
    WriteF('Started from Shell/CLI\n   Arguments: "\s"\n', arg)
  ELSE
    WriteF('Started from Workbench\n')
    args:=startup.arglist
    FOR i:=1 TO startup.numargs  /* Loop through the arguments */
      IF args[].lock=NIL
        WriteF('  Argument \d: "\s" (no lock)\n', i, args[].name)
      ELSE
        oldlock:=CurrentDir(args[].lock)
        len:=FileLength(args[].name)  /* Do something with file */
        IF len=-1
          WriteF('  Argument \d: "\s" (file does not exist)\n',
                 i, args[].name)
        ELSE
          WriteF('  Argument \d: "\s", file length is \d bytes\n',
                 i, args[].name, len)
        ENDIF
        CurrentDir(oldlock) /* Important: restore current dir */
      ENDIF
      args++
    ENDFOR
  ENDIF
ENDPROC
