/* A very small util to dump memory in a shell.
   usage: MEM <adr>                    

  simply dumps memory in a shell, usefull for hardcore-kamikaze
  debugging and the like.

  try:
   1> mem $f80000        ; only if you have a non-moved kick2.0 or better

*/

PROC main()
  DEF adr,a,b,radr:PTR TO LONG,c,r
  adr,r:=Val(arg)
  IF r=0
    WriteF('Usage: MEM <adr>\n')
  ELSE
    adr:=adr AND -2     /* no odd adr */
    FOR a:=0 TO 7
      radr:=a*16+adr
      WriteF('$\r\z\h[8]:   ',radr)
      FOR b:=0 TO 3 DO WriteF('\r\z\h[8] ',radr[b])
      WriteF('  "')
      c:=radr
      FOR b:=0 TO 15 DO Out(stdout,IF (c[b]<32) OR (c[b]>126) THEN "." ELSE c[b])
      WriteF('"\n')
    ENDFOR
  ENDIF
ENDPROC
