Short: Intelligent AmigaGuide splitter
Type: text/hyper
Uploader: jason@fsel.com (Jason R. Hulance)
Author: jason@fsel.com (Jason R. Hulance)

AGSplit
=======
AGSPlit is an intelligent AmigaGuide file splitter.  By "intelligent"
I mean that each of the resulting pieces is a complete, stand-alone
AmigaGuide file.  Needless to say, the pieces are all interlinked and
behave as if they were in one big AmigaGuide file.

Why would you want to split an AmigaGuide file?  Well, the smaller the
file is the faster it loads into AmigaGuide/Multiview.  The trade-off
is that it is marginally slower to go between links that are not in
the same AmigaGuide file (but this is hardly noticeable if the pieces
are small!).

In particular, I wrote this program to split up my "Beginner's Guide
to Amiga E" which I write in TeXinfo format and so produce using the
excellent MakeGuide utility (and the version of MakeGuide I use can
only make one large file).

Usage
-----
The template is:

  GUIDEFILE/A,SPLITFILE/A,DESTDIR

"GUIDEFILE" is the file to split.
"SPLITFILE" is a short file to show the split positions (see below).
"DESTDIR" is an optional directory name to store the pieces (default
          is the current directory).

Split Files
-----------
A split file consists of node/filename pairs which describe the
positions where the AmigaGuide file is to be split.  The nodes must be
specified in the order in which they appear in the AmigaGuide file.
For example:

Main
Contents.guide
FirstNode
Chapter1.guide
AnotherNode
Appendix.guide

This split file specifies that *all* nodes from "Main" up to
"FirstNode" (but not including "FirstNode") will be split into the
file "Contents.guide".

All nodes from "FirstNode" up to "AnotherNode" (but not including
"AnotherNode") will be split into "Chapter1.guide".

All nodes from "AnotherNode" to the end of the original AmigaGuide
file will be split into "Appendix.guide".

If this split file were called "mysplitfile" and the AmigaGuide file
to be split were called "myagfile.guide", then:

   AGSplit myagfile.guide mysplitfile ram:

would split "myagfile.guide" into the specified pieces
("Contents.guide", "Chapter1.guide" and "Appendix.guide") and put the
pieces into the "ram:" directory.

Examples
--------
Two supplied example split files can be used to split my "Beginner's
Guide to Amiga E" and the "E Reference Guide" (both available in the
current Amiga E distribution in dev/e).

To Do
-----
I might make a better interface than "split files", but they are
simple enough to use...

It's a bit slow at the moment (I'm using a quite general engine), but
it's not something you'd do very often...
