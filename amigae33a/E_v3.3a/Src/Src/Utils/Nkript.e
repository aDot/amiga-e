/* Nkript.e, very simple file (de)coder
   USAGE: nkript <file>

   nkript asks for a 4 letter key, and a 3 letter pincode.
   as nkript uses EOR, you may use this program to code and
   decode. the key and pincode are not stored anywhere, so it
   _relatively_ safe. this has the effect that if you type the
   wrong key, no error is given, but the file is simply
   decoded wrong.

*/

MODULE 'tools/file'

ENUM ER_NONE,ER_FILE,ER_MEM,ER_USAGE,ER_OUT,ER_ILLEGAL,ER_NONUM

PROC main() HANDLE
  DEF flen,mem=NIL,key,keyadd,file[200]:STRING,p
  WriteF('Nkript (c) 1992 $#%!\n')
  IF StrCmp(arg,'',1) OR StrCmp(arg,'?',2) THEN Raise(ER_USAGE)
  mem,flen:=readfile(arg)
  key:=readpass('key',4,FALSE)
  keyadd:=readpass('pin',3,TRUE) OR 3
  WriteF('Now (de)coding "\s".\n',arg)
  MOVE.L flen,D7
  LSR.L  #2,D7		/* D7 = #of LONGs */
  MOVE.L key,D6
  MOVE.L keyadd,D4
  MOVE.L mem,A0
  loop:
  MOVE.L D4,D5
  SUB.L  D6,D5
  LSL.L  #3,D6		/* random alg.  D6*7+keyadd (11) */
  ADD.L  D5,D6
  EOR.L  D6,(A0)+
  DBRA   D7,loop
  SUB.L  #$10000,D7
  BCC.S  loop		/* DBRA.L emulation */
  p:=InStr(arg,'.',0)
  StrCopy(file,arg,p)
  IF StrCmp(arg+p,'.nkr',ALL)=FALSE THEN StrAdd(file,'.nkr',ALL)
  writefile(file,mem,flen)
EXCEPT DO
  IF mem THEN freefile(mem)
  SELECT exception
    CASE ER_NONE;    WriteF('Done.\n')
    CASE "OPEN";     WriteF('Could not access file "\s" !\n',exceptioninfo)
    CASE "IN";       WriteF('Could not read from file "\s" !\n',exceptioninfo)
    CASE "OUT";      WriteF('Could not write to file "\s" !\n',exceptioninfo)
    CASE "MEM";      WriteF('No memory for loading file!\n')
    CASE ER_USAGE;   WriteF('USAGE: Nkript <file>\n')
    CASE ER_ILLEGAL; WriteF('Wrong #of chars\n')
    CASE ER_NONUM;   WriteF('not a decimal number\n')
  ENDSELECT
ENDPROC

PROC readpass(messy,numchars,decflag)
  DEF s[25]:STRING,a,t,n=0,f=1
  WriteF('\s[\d]: ',messy,numchars)
  ReadStr(stdout,s)
  IF EstrLen(s)<>numchars THEN Raise(ER_ILLEGAL)
  IF decflag
    t:=s
    FOR a:=1 TO numchars
      n:=n+(t[]-"0"*f)
      IF (t[]<"0") OR (t[]++>"9") THEN Raise(ER_NONUM)
      f:=f*10
    ENDFOR
    ^s:=n
  ENDIF
ENDPROC ^s
