/* Watch a file by notification

pops up a requester when a file gets modified.
USAGE: watch <file>
EXAMPLE: run >NIL: watch >NIL: s:startup-sequence 

needs v37

simply "watches" a file, using the new notification system
of kick2.0. note: does not _prevent_ files from being modified,
just tells you. usefull, for example, if you're installing a new
software package, and you want to know wether the installer
does something funny to your startup-sequence or user-startup.

note that the only way to stop watching is asctually modifying
the file. (or rebooting :-)

*/

OPT OSVERSION=37

MODULE 'dos/notify'

PROC main()                       /* make sure file is there: else we'll */
  DEF nreq:PTR TO notifyrequest,sig,task
  IF (FileLength(arg)=-1) OR (arg[0]=0)     /* never be notified */
    WriteF('file "\s" does not exist\n',arg)
    CleanUp(10)
  ENDIF
  nreq:=New(SIZEOF notifyrequest)     /* memory is cleared */
  IF nreq=NIL THEN RETURN 20
  sig:=AllocSignal(-1)                /* we want to be signalled */
  IF sig=-1 THEN RETURN 10
  task:=FindTask(0)
  nreq.name:=arg                      /* fill in structure */
  nreq.flags:=NRF_SEND_SIGNAL
  nreq.port:=task                     /* union port/task */
  nreq.signalnum:=sig
  IF StartNotify(nreq)
    WriteF('Now watching: "\s"\n',arg)
    Wait(Shl(1,sig))
    EasyRequestArgs(0,[20,0,0,'File "\s" modified!','Damn!'],0,[arg])
    EndNotify(nreq)
  ELSE
    WriteF('Could not watch "\s".\n',arg)
  ENDIF
  FreeSignal(sig)
ENDPROC
