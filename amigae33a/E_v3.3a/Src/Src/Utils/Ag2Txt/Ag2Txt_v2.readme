Short: AmigaGuide to text (v2). E & C source.
Type: text/hyper
Uploader: m88jrh@uk.ac.ox.ecs (Jason R. Hulance)
Author: m88jrh@uk.ac.ox.ecs (Jason R. Hulance)

Ag2Txt (v2)
===========
Ag2Txt converts AmigaGuide files to highlighted text.  This text may be
read using a standard text reader, or may be sent to a suitable printer.
(The Amiga printer drivers handle the ANSI codes produced.)

There are three different versions of this program in the archive: two
written in E (one for AmigaDOS 1.3 and one for 2.0+), and one in C. All
three versions have the following syntax:

    ag2txt <amigaguidefile> [ <textfile> ]

The <textfile> is an optional output file.  If it is not specified or
cannot be opened then the output is written to the terminal (stdout).

Version 2 of Ag2Txt knows a lot more AmigaGuide codes and handles them a
lot better than version 1.  Also, version 1 omitted the C version, which
was subsequently requested by a number of people.

You can customise the output by fiddling with the source.  The ansi codes
used are confined to the function 'write_ansi' near the top of the source.
One list is for turning on the appropriate attribute, and the second is
for turning it off.  The C version is particularly dumb because the Xterms
I use at work don't understand the 'turn off' codes so I took them out
(and used just the 'set to plain' code, '\e[0m').

The AmigaDOS 2.0+ version uses the buffered I/O functions to gain a huge
performance increase compared to the unbuffered I/O used in the AmigaDOS
1.3 version.  Also, the 2.0+ version uses Michael Zucchi's excellent
'async' module that comes with E v3.0 to get an extra 1/3 speed up (for
reading, since 'async' doesn't do writes... yet??).

Legal stuff:
The programs in this archive are Copyright (C) 1994, Jason R. Hulance.
Apart from that the GNU Public License (Version 2) holds.
