OPT MODULE

MODULE 'intuition/classes', 'utility/hooks', 'intuition/classusr'

/* example call: domethod(myobj,[METHODID,...]) */

EXPORT PROC domethod(obj:PTR TO object,msg:PTR TO msg)
  DEF h:PTR TO hook,o:PTR TO object,dispatcher
  IF obj
    o:=obj-SIZEOF object     /* instance data is to negative offset */
    h:=o.class
    dispatcher:=h.entry      /* get dispatcher from hook in iclass */
    MOVE.L h,A0
    MOVE.L msg,A1
    MOVE.L obj,A2            /* probably should use CallHookPkt, but the */
    MOVE.L dispatcher,A3     /*   original code (DoMethodA()) doesn't. */
    JSR (A3)                 /* call classDispatcher() */
    MOVE.L D0,o
    RETURN o
  ENDIF
ENDPROC NIL
