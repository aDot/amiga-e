/*
	FilledVDefs.e

	Module for FilledVector.m definitions

	1994 Michael Zucchi
*/

OPT MODULE

/*
 * Polygon Context Flags
 */

EXPORT CONST PCF_ZCLIP = 1	-> perform z clipping?


/*
 * A position object which is used by several function to define
 * a locality and an orientation in one.
 * (i.e. a position and angle)
 */

EXPORT OBJECT position
  ax:INT	-> x,y,z angles
  ay:INT
  az:INT
  px:INT	-> x,y,z position
  py:INT
  pz:INT
ENDOBJECT

/*
 * Each VObject allocated will return one of these.  They can be linked
 * into lists using the VList functions.
 * The position/orientation of the object can also be conveniently
 * stoed and accessed from here.  Note however that there is considerably
 * more PRIVATE data following ...
 */

EXPORT OBJECT vobject
  next:LONG	-> next field for a linked list
  prev:LONG	-> prev field
  ax:INT	-> angles of object (depends on rendering function used)
  ay:INT
  az:INT
  px:INT	-> position of object
  py:INT
  pz:INT
ENDOBJECT	-> more data, PRIVATE

/*
 * Each face of an object uses one of these.  These are used also
 * to define a new object.
 */ 

EXPORT OBJECT face
  cross0:INT	-> the 3 points used for the cross-product visibility test
  cross1:INT
  cross2:INT
  colour:INT	-> the colour of the object, the bottom 8 bits represent the
  		-> base colour.  In the future, the top 8 bits MAY be used
  		-> to represent an alternate colour for stipling.
  facelist:PTR TO INT	-> pointer to the polygon definition, which
		-> includes first:
		-> 	a INT which says how many lines are in this polygon
		->	pairs of INT's which point to the vertices used by
		->	each of the lines of the polygon
  pad00:LONG	-> future expansion maybe (makes the OBJECT 16 bytes long)
ENDOBJECT

/*
 * A matix object, as used by the various matrix manipulation
 * functions.  Currently uses 3x3 matrices, but perhaps
 * some 4x4 matrix routines will be written someday (?)
 */

EXPORT OBJECT matrix
  matdata[10]:ARRAY OF INT
ENDOBJECT
