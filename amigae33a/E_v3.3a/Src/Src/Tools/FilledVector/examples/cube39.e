
/*

  A _simple_ cube example, using Workbench 3.0 code

  FilledVector.m module example code, Michael Zucchi

  This code in the public domain

*/

OPT OSVERSION=39

MODULE 'intuition/intuition', 'intuition/screens',
	'tools/filledvector', 'tools/filledvdefs',
	'tools/scrbuffer',
	'graphics/rastport'

DEF scr,pc,cube:PTR TO vobject, destz=2000,
	rast:rastport;

PROC main()

InitRastPort(rast);

scr:=sb_OpenScreen([SA_DEPTH,4,SA_WIDTH,320,SA_HEIGHT,200,0],0);

pc:=newPolyContext(sb_GetBitMap(scr),20)	-> create context
setPolyFlags(pc,1,1)			-> turn on zclipping

 cube:=newVectorObject(0,	-> basic type
 	8,			-> 8 points
 	6,			-> 6 faces
    [-100,100,-100,	-> 0	-> points array
    100,100,-100,	-> 1
    100,-100,-100,	-> 2
    -100,-100,-100,	-> 3
    -100,100,100,	-> 4
    100,100,100,	-> 5
    100,-100,100,	-> 6
    -100,-100,100]:INT,	-> 7	-> faces array below
	[0,1,2, 1, [4, 0,1,1,2,2,3,3,0]:INT, 0,	-> front
	6,5,4, 2, [4, 4,5,5,6,6,7,7,4]:INT, 0,	-> back
	2,1,5, 3, [4, 1,5,5,6,6,2,2,1]:INT, 0,	-> right
	4,0,3, 4, [4, 4,0,0,3,3,7,7,4]:INT, 0,	-> left
	1,0,4, 5, [4, 0,1,1,5,5,4,4,0]:INT, 0,	-> top
	7,3,2, 6, [4, 3,2,2,6,6,7,7,3]:INT, 0]:face);	-> bottom

cube.pz:=1000;

WHILE Mouse()<>3
  rast.bitmap:=sb_NextBuffer(scr);	-> next screen
  SetRast(rast,0);			-> clear the off-screen
  setPolyBitMap(pc, rast.bitmap);	-> take note of change
  drawVObject(pc, cube);		-> draw to off-screen

  cube.ax:=cube.ax+1			-> move object
  cube.ay:=cube.ay+2
  cube.az:=cube.az+3
  cube.pz:=cube.pz+((destz-cube.pz)/6)
  IF Abs(cube.pz-destz)<6 THEN destz:=Rnd(2000)+10
ENDWHILE

freeVectorObject(cube);
sb_CloseScreen(scr);

ENDPROC
