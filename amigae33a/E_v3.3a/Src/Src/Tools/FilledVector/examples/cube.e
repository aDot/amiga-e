
/*

  A _simple_ cube example, using Workbench 1.3 code

  FilledVector.m module example code, Michael Zucchi

  This code in the public domain

*/

OPT OSVERSION=33

MODULE 'intuition/intuition', 'intuition/screens',
	'tools/filledvector', 'tools/filledvdefs'

DEF s0:PTR TO screen, s1:PTR TO screen, scr:PTR TO screen,
	pc,cube:PTR TO vobject, destz=2000;

PROC main()

s0:=OpenS(320,200,4,0,'3d Cube');	-> 2 screens for doublebuffering
s1:=OpenS(320,200,4,0,'3d Cube');

pc:=newPolyContext(s0.bitmap,20)	-> create context
setPolyFlags(pc,1,1)			-> turn on zclipping

 cube:=newVectorObject(0,	-> basic type
 	8,			-> 8 points
 	6,			-> 6 faces
    [-100,100,-100,	-> 0	-> points array
    100,100,-100,	-> 1
    100,-100,-100,	-> 2
    -100,-100,-100,	-> 3
    -100,100,100,	-> 4
    100,100,100,	-> 5
    100,-100,100,	-> 6
    -100,-100,100]:INT,	-> 7	-> faces array below
	[0,1,2, 1, [4, 0,1,1,2,2,3,3,0]:INT, 0,	-> front
	6,5,4, 2, [4, 4,5,5,6,6,7,7,4]:INT, 0,	-> back
	2,1,5, 3, [4, 1,5,5,6,6,2,2,1]:INT, 0,	-> right
	4,0,3, 4, [4, 4,0,0,3,3,7,7,4]:INT, 0,	-> left
	1,0,4, 5, [4, 0,1,1,5,5,4,4,0]:INT, 0,	-> top
	7,3,2, 6, [4, 3,2,2,6,6,7,7,3]:INT, 0]:face);	-> bottom

scr:=s0
cube.pz:=1000;

WHILE Mouse()<>3
  SetRast(scr.rastport,0);		-> clear the screen
  setPolyBitMap(pc, scr.bitmap);	-> take note of change
  drawVObject(pc, cube);		-> draw to off-screen

  ScreenToFront(scr);			-> bring it to the front

  cube.ax:=cube.ax+1			-> move object
  cube.ay:=cube.ay+2
  cube.az:=cube.az+3
  cube.pz:=cube.pz+((destz-cube.pz)/6)
  IF Abs(cube.pz-destz)<6 THEN destz:=Rnd(2000)+10

  IF scr=s0 THEN scr:=s1 ELSE scr:=s0;	-> swap screens
ENDWHILE

freeVectorObject(cube);
CloseS(s0)
CloseS(s1)

ENDPROC
