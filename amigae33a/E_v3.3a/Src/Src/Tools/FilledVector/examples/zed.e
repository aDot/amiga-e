/*
	More complex demonstration of filledvector.m, with
	a multiple object display, and using the matrix
	functions to modify an object after its been made

	Michael Zucchi, 1994, This code in the public domain
 */

OPT OSVERSION=39

MODULE 'tools/filledvector', 'tools/filledvdefs', 'exec/lists', 'intuition/screens',
	'tools/scrbuffer', 'graphics/rastport'

DEF list:PTR TO mlh,screen,pc,
	zed:PTR TO vobject,eee:PTR TO vobject,dee:PTR TO vobject,
	scr:PTR TO screen

PROC main()
  /* create objects, and add to list */
  IF createobjects()
    warpobjects()
    IF screen:=sb_OpenScreen([SA_DEPTH,4,SA_WIDTH,320,SA_HEIGHT,256,SA_DISPLAYID,$0000,0],0)
      IF pc:=newPolyContext(sb_GetBitMap(screen),50)
        demo()
        freePolyContext(pc)
      ENDIF
      sb_CloseScreen(screen)
    ENDIF
    freeobjects()
  ENDIF
ENDPROC

/* just demos basic rendering until the user chooses to exit */

PROC demo()
  DEF p:position,bitm,myrp:rastport

  p.ax:=0;p.ay:=0;p.az:=0;p.px:=80;p.py:=0;p.pz:=8000;

  setPolyFlags(pc,1,1)	-> z clipping on

  bitm:=sb_NextBuffer(screen)
  sb_NextBuffer(screen)
  scr:=sb_GetScreen(screen);
  InitRastPort(myrp);

  WHILE Mouse()<>3
    bitm:=sb_NextBuffer(screen)
    myrp.bitmap:=bitm
    SetRast(myrp,0);
    setPolyBitMap(pc, bitm)

    moveDrawVList(pc, list, p)
    p.az:=p.az+1;
    p.ax:=p.ax-2;
    p.ay:=p.ay+3;

    IF Mouse()=1 THEN p.pz:=p.pz+35
    IF Mouse()=2 THEN p.pz:=p.pz-35
  ENDWHILE


ENDPROC

PROC createobjects()
  DEF stat=-1

  /* allocate a list, and add them to it */
  list:=newVList()

  /* the letter z */
  zed:=newVectorObject(0,20,12,
	[-178*3,98*3,20*4,	/* points */
	-34*3,98*3,20*4,
	-34*3,66*3,20*4,
	-146*3,-50*3,20*4,
	-34*3,-50*3,20*4,
	-34*3,-82*3,20*4,
	-178*3,-82*3,20*4,
	-178*3,-50*3,20*4,
	-66*3,66*3,20*4,
	-178*3,66*3,20*4,
	-178*3,98*3,-20*4,	/* lower side */
	-34*3,98*3,-20*4,
	-34*3,66*3,-20*4,
	-146*3,-50*3,-20*4,
	-34*3,-50*3,-20*4,
	-34*3,-82*3,-20*4,
	-178*3,-82*3,-20*4,
	-178*3,-50*3,-20*4,
	-66*3,66*3,-20*4,
	-178*3,66*3,-20*4]:INT,
	/* since no 'depth' sorting is done - ensure innermost surfaces drawn first */
	[3,4,14,1,	/* bottom inside edge */
		[4,3,4,4,14,14,13,13,3]:INT,0,
	8,9,19,2,	/* top inside edge */
		[4,8,9,9,19,19,18,18,8]:INT,0,

	2,3,13,3,	/* sloping inside edge */
		[4,2,3,3,13,13,12,12,2]:INT,0,
	7,8,18,4,	/* sloping inside edge-left */
		[4,7,8,8,18,18,17,17,7]:INT,0,

	2,1,0,5,	/* front face */
		[10,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,0]:INT,0,
	10,11,12,6,	/* back face */
		[10,10,11,11,12,12,13,13,14,14,15,15,16,16,17,17,18,18,19,19,10]:INT,0,
	0,1,11,7,	/* top bar of z */
		[4,0,1,1,11,11,10,10,0]:INT,0,
	5,6,16,8,	/* bottom bar of z */
		[4,5,6,6,16,16,15,15,5]:INT,0,
	1,2,12,9,	/* first back end */
		[4,1,2,2,12,12,11,11,1]:INT,0,
	4,5,15,10,	/* next back end */
		[4,4,5,5,15,15,14,14,4]:INT,0,
	6,7,17,11,	/* left lower end */
		[4,6,7,7,17,17,16,16,6]:INT,0,
	9,0,10,12,	/* upper left end */
		[4,9,0,0,10,10,19,19,9]:INT,0]:face);

  /* the letter e */
  eee:=newVectorObject(0,24,14,
	[0*3,0*3,20*4,
	 66*3,0*3,20*4,
	 66*3,-18*3,20*4,
	 18*3,-18*3,20*4,
	 18*3,-34*3,20*4,
	 50*3,-34*3,20*4,
	 50*3,-50*3,20*4,
	 18*3,-50*3,20*4,
	 18*3,-66*3,20*4,
	 66*3,-66*3,20*4,
	 66*3,-82*3,20*4,
	 0*3,-82*3,20*4,
	 0*3,0*3,-20*4,
	 66*3,0*3,-20*4,
	 66*3,-18*3,-20*4,
	 18*3,-18*3,-20*4,
	 18*3,-34*3,-20*4,
	 50*3,-34*3,-20*4,
	 50*3,-50*3,-20*4,
	 18*3,-50*3,-20*4,
	 18*3,-66*3,-20*4,
	 66*3,-66*3,-20*4,
	 66*3,-82*3,-20*4,
	 0*3,-82*3,-20*4]:INT,

	/* since no 'depth' sorting is done - ensure innermost surfaces drawn first */
	[3,4,16,13,	/* inside top right of E */
		[4,3,4,4,16,16,15,15,3]:INT,0,
	7,8,20,14,	/* inside lower right of E */
		[4,7,8,8,20,20,19,19,7]:INT,0,
	2,3,15,15,	/* upper inner E */
		[4,2,3,3,15,15,14,14,2]:INT,0,
	8,9,21,16,	/* lower inner */
		[4,8,9,9,21,21,20,20,8]:INT,0,
	4,5,17,17,	/* top of bar */
		[4,4,5,5,17,17,16,16,4]:INT,0,
	6,7,19,18,	/* bottom of bar */
		[4,6,7,7,19,19,18,18,6]:INT,0,
	5,6,18,19,	/* front of bar */
		[4,5,6,6,18,18,17,17,5]:INT,0,
	/* all outside surfaces */
	1,2,14,20,	/* top front cap */
		[4,1,2,2,14,14,13,13,1]:INT,0,
	9,10,22,21,	/* bottom front cap */
		[4,9,10,10,22,22,21,21,9]:INT,0,

	0,1,13,22,	/* top */
		[4,0,1,1,13,13,12,12,0]:INT,0,
	10,11,23,23,	/* bottom */
		[4,10,11,11,23,23,22,22,10]:INT,0,
	11,0,12,24,	/* back */
		[4,11,0,0,12,12,23,23,11]:INT,0,

	2,1,0,25,	/* front face */
		[12,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,0]:INT,0,
	12,13,14,26,	/* rear face */
		[12,12,13,13,14,14,15,15,16,16,17,17,18,18,19,19,20,20,21,21,22,22,23,23,12]:INT,0]:face);


  /* the letter d */
  dee:=newVectorObject(0,24,14,
	[-34*3,42*3,20*4,
	18*3,42*3,20*4,
	34*3,26*3,20*4,
	34*3,-26*3,20*4,
	18*3,-42*3,20*4,
	-34*3,-42*3,20*4,
	-18*3,26*3,20*4,
	0,26*3,20*4,
	18*3,10*3,20*4,
	18*3,-10*3,20*4,
	0,-26*3,20*4,
	-18*3,-26*3,20*4,

	-34*3,42*3,-20*4,
	18*3,42*3,-20*4,
	34*3,26*3,-20*4,
	34*3,-26*3,-20*4,
	18*3,-42*3,-20*4,
	-34*3,-42*3,-20*4,
	-18*3,26*3,-20*4,
	0,26*3,-20*4,
	18*3,10*3,-20*4,
	18*3,-10*3,-20*4,
	0,-26*3,-20*4,
	-18*3,-26*3,-20*4]:INT,

	[19,7,6,27,	/* top inner */
		[4,6,7,7,19,19,18,18,6]:INT,0,
	20,8,7,28,	/* top right inner */
		[4,7,8,8,20,20,19,19,7]:INT,0,
	21,9,8,29,	/* right inner */
		[4,8,9,9,21,21,20,20,8]:INT,0,
	22,10,9,30,	/* lower right inner */
		[4,9,10,10,22,22,21,21,9]:INT,0,
	23,11,10,31,	/* lower inner */
		[4,10,11,11,23,23,22,22,10]:INT,0,
	18,6,11,32,	/* inner left */
		[4,11,6,6,18,18,23,23,11]:INT,0,

	0,1,13,33,	/* top */
		[4,0,1,1,13,13,12,12,0]:INT,0,
	1,2,14,34,	/* top right */
		[4,1,2,2,14,14,13,13,1]:INT,0,
	2,3,15,35,	/* right */
		[4,2,3,3,15,15,14,14,2]:INT,0,
	3,4,16,36,	/* lower right */
		[4,3,4,4,16,16,15,15,3]:INT,0,
	4,5,17,37,	/* bottom */
		[4,4,5,5,17,17,16,16,4]:INT,0,

	5,0,12,38,	/* back */
		[4,5,0,0,12,12,17,17,5]:INT,0,

	2,1,0,39,	/* front face */
		[12,0,1,1,2,2,3,3,4,4,5,5,0,6,7,7,8,8,9,9,10,10,11,11,6]:INT,0,
	12,13,14,40,	/* rear face */
		[12,12,13,13,14,14,15,15,16,16,17,17,12,18,19,19,20,20,21,21,22,22,23,23,18]:INT,0]:face);

  dee.px:=130*3;dee.py:=-42*3;	-> real position

  addVObject(list,zed)
  addVObject(list,eee)
  addVObject(list,dee)

ENDPROC stat

PROC freeobjects()
  freeVList(list,1)       /* also free nodes */
ENDPROC

/*
	Use the matrix functions to modify the objects a little bit
 */

PROC warpobjects()
DEF mat:matrix

setMatRotate(mat, 0,40,0);
matApply3(mat,24,getVObjectPoints(dee),getVObjectPoints(dee))
setMatRotate(mat, 0,-40,0);
matApply3(mat,20,getVObjectPoints(zed),getVObjectPoints(zed))

ENDPROC

