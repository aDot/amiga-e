
/*

	Complex Workbench2.0+ Example of FilledVector.m

	This program creates a spinning "A1200" logo that spins
	around on the back of Workbench.  On a 1200 without
	fastmem, it can hapily be run in the background without
	causing noticeable delays.

	On the command line, specify the number of columns,
	followed by the number of rows of images you want,
	it defaults to 2x2.

	Run it with Workbench NOT inbackdrop mode, and to
	remove it, run it again.

	Michael Zucchi, 1994.  Program in public domain.

*/



OPT OSVERSION=37

MODULE 'tools/filledvector', 'tools/filledvdefs',
	'exec/lists', 'exec/ports', 'exec/nodes', 'exec/libraries',
	'intuition/screens', 'intuition/intuition', 'intuition/intuitionbase',
	'graphics/gfx', 'graphics/rastport', 'graphics/layers'

DEF list:PTR TO mlh,pc,
	let_0:PTR TO vobject,
	let_1:PTR TO vobject,
	let_2:PTR TO vobject,
	let_A:PTR TO vobject,
	scr:PTR TO screen,abitm:PTR TO bitmap,
	win:PTR TO window,
	myrast:rastport,
	mport:PTR TO mp,
	width,height,depth,
	rows,cols

DEF dx,dy,dz

/********************************************************************

	main

 */

PROC main()
DEF rdargs, args:PTR TO LONG,p:PTR TO LONG, i, gfx:PTR TO lib

cols:=2;
rows:=2;

-> check arguments
args:=[0,0];
IF rdargs:=ReadArgs('cols/N,rows/N', args, 0)
  IF p:=args[0] THEN cols:=p[0]
  IF p:=args[1] THEN rows:=p[0]
  FreeArgs(rdargs);
ENDIF  

-> see if we're already running, if yes, signal the other version
Forbid();
mport:=FindPort('backdrop');
IF mport
  Signal(mport.sigtask, Shl(1,mport.sigbit))
ENDIF
Permit();

-> if we are not already running, start doing our thing
IF mport=0

  -> create a public port where we can be found later
  mport:=CreateMsgPort();
  mport::ln.name:='backdrop';
  mport::ln.pri:=-128;
  AddPort(mport);

  -> open the backdrop window
  win:=OpenWindowTagList(0, [WA_FLAGS, WFLG_BACKDROP OR WFLG_BORDERLESS OR WFLG_SIMPLE_REFRESH OR WFLG_NOCAREREFRESH,
	WA_BACKFILL, LAYERS_NOBACKFILL, 0]);

  -> calculate size of each tile
  width:=win.width/cols
  height:=win.height/rows
  IF height AND 1 THEN height++
  IF width AND 31 THEN width:=width+(32-(width AND 31))

  -> find the depth of workbench, and open our rendering bitmap
  scr:=LockPubScreen(0);

  depth:=scr.rastport::rastport.bitmap::bitmap.depth
  gfx:=gfxbase;

  IF gfx.version<39
    abitm:=New(SIZEOF bitmap+(8*4));
    p:=abitm.planes
    FOR i:=0 TO depth-1 DO p[i]:=AllocRaster(width,height);
    InitBitMap(abitm,depth,width,height);
  ELSE
    abitm:=AllocBitMap(width,height,depth,
  	BMF_CLEAR+BMF_INTERLEAVED,0);
  ENDIF

  UnlockPubScreen(0, scr);

  -> finish of setting up, then goto the main loop
  SetTaskPri(FindTask(0), -128);
  InitRastPort(myrast);
  myrast.bitmap:=abitm;

  IF win
    IF createobjects()
      IF pc:=newPolyContext(abitm,50)
        demo()
        freePolyContext(pc)
      ENDIF
      freeobjects()
    ENDIF
    CloseWindow(win);
  ENDIF

  IF gfx.version<39
    p:=abitm.planes
    FOR i:=0 TO depth-1 DO FreeRaster(p[i], width,height);
    InitBitMap(abitm,depth,width,height);
  ELSE
    FreeBitMap(abitm);
  ENDIF

  RemPort(mport);
  DeleteMsgPort(mport);

ENDIF -> IF mport=0

ENDPROC

/********************************************************************

  Main loop.  Render/move the object until we get told to stop

 */

PROC demo()
  DEF p:position,x,y,c, sigbit, destz

  c:=0;
  p.ax:=0;p.ay:=0;p.az:=0;p.px:=0;p.py:=0;p.pz:=750;

  destz:=Rnd(1000)+400;
  sigbit:=Shl(1, mport.sigbit);
  setPolyFlags(pc,1,1)

  -> main loop
  WHILE (SetSignal(0, sigbit) AND sigbit)=0	-> see if we've been told to stop?

    cls()
    setPolyBitMap(pc, abitm)

    IF c--<0
      setangles()
      c:=60;
    ENDIF

    -> pri back to 0, this will stop us losing cpu while blitter owned (hang!)
    SetTaskPri(FindTask(0), 0);

    moveDrawVList(pc, list, p)
    FOR x:=0 TO cols-1
    FOR y:=0 TO rows-1
      BltBitMapRastPort(abitm, 0,0, win.rport, x*width,y*height,width,height,$c0);
    ENDFOR
    ENDFOR

    -> its safe to go back to low pri again
    SetTaskPri(FindTask(0), -128);

    -> move object
    p.pz:=p.pz+((destz-p.pz)/6)
    IF Abs(p.pz-destz)<6 THEN destz:=Rnd(4000)+200
    p.az:=p.az+dz;
    p.ax:=p.ax-dx;
    p.ay:=p.ay+dy;

  ENDWHILE
ENDPROC

PROC setangles()
dx:=Rnd(9)-4
dy:=Rnd(9)-4
dz:=Rnd(9)-4
ENDPROC


/********************************************************************

 Clears the bitmap we're rendering into, then fills it with the
 stipple pattern

 */

PROC cls()
 SetRast(myrast, 0);
 WaitBlit()

 MOVE.L abitm,A2
 MOVE.L 8(A2),A1
 MOVE.L	height,D3
 LSR.L	#1,D3
 SUBQ.L	#1,D3
 MOVE.L #$55555555,D0
 MOVE.L #$AAAAAAAA,D1
d_lp0:
 MOVE.L A1,A0
 ADDA.W (A2),A1
 MOVE.L width,D2
 LSR.W	#5,D2
 SUBQ.W	#1,D2
d_lp1:
 MOVE.L D0,(A0)+
 DBF    D2,d_lp1
 MOVE.L A1,A0
 ADDA.W (A2),A1
 MOVE.L	width,D2
 LSR.W	#5,D2
 SUBQ.W	#1,D2
d_lp2:
 MOVE.L D1,(A0)+
 DBF    D2,d_lp2
 DBF	D3,d_lp0
ENDPROC

/********************************************************************

 	Create all of the objects

 */ 

PROC createobjects()
  DEF stat=-1,
  	tmp:PTR TO vobject

  /* allocate a list, and add them to it */
  list:=newVList()

  let_0:=newVectorObject(0,16,10,
	[8*3,126*3,-20*4,
	37*3,30*3,-20*4,
	83*3,0*3,-20*4,
	134*3,0*3,-20*4,
	153*3,31*3,-20*4,
	111*3,144*3,-20*4,
	79*3,165*3,-20*4,
	20*3,165*3,-20*4,
	8*3,126*3,20*4,
	37*3,30*3,20*4,
	83*3,0*3,20*4,
	134*3,0*3,20*4,
	153*3,31*3,20*4,
	111*3,144*3,20*4,
	79*3,165*3,20*4,
	20*3,165*3,20*4]:INT,
	[0,1,9,1,
		[4,0,1,1,9,9,8,8,0]:INT,0,
	0+1,1+1,9+1,2,
		[4,0+1,1+1,1+1,9+1,9+1,8+1,8+1,0+1]:INT,0,
	0+2,1+2,9+2,3,
		[4,0+2,1+2,1+2,9+2,9+2,8+2,8+2,0+2]:INT,0,
	0+3,1+3,9+3,4,
		[4,0+3,1+3,1+3,9+3,9+3,8+3,8+3,0+3]:INT,0,
	0+4,1+4,9+4,5,
		[4,0+4,1+4,1+4,9+4,9+4,8+4,8+4,0+4]:INT,0,
	0+5,1+5,9+5,6,
		[4,0+5,1+5,1+5,9+5,9+5,8+5,8+5,0+5]:INT,0,
	0+6,1+6,9+6,7,
		[4,0+6,1+6,1+6,9+6,9+6,8+6,8+6,0+6]:INT,0,
	7,0,8,8,
		[4,7,0,0,8,8,15,15,7]:INT,0,
	-> front/back
	4,1,0,9,
		[8,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,0]:INT,0,
	8,9,12,10,
		[8,8,9,9,10,10,11,11,12,12,13,13,14,14,15,15,8]:INT,0]:face);



  let_1:=newVectorObject(0,8,6,
	[1*3,165*3,-20*4,
	22*3,165*3,-20*4,
	105*3,0*3,-20*4,
	84*3,0*3,-20*4,
	1*3,165*3,20*4,
	22*3,165*3,20*4,
	105*3,0*3,20*4,
	84*3,0*3,20*4]:INT,
  	[0,1,2,1,
  		[4,1,2,2,3,3,0,0,1]:INT,0,
  	6,5,4,2,
		[4,7,6,6,5,5,4,4,7]:INT,0,
  	5,6,2,3,
		[4,6,2,6,5,5,1,1,2]:INT,0,
  	7,4,0,4,
		[4,3,7,7,4,4,0,0,3]:INT,0,
  	5,1,0,5,
		[4,4,5,5,1,1,0,0,4]:INT,0,
  	6,7,3,6,
		[4,6,7,7,3,3,2,2,6]:INT,0]:face);

  let_A:=newVectorObject(0,22,13,
	[2*3,165*3,-20*4,
	128*3,0*3,-20*4,
	148*3,0*3,-20*4,
	148*3,165*3,-20*4,
	127*3,165*3,-20*4,
	127*3,100*3,-20*4,
	76*3,100*3,-20*4,
	27*3,165*3,-20*4,
	127*3,84*3,-20*4,
	127*3,33*3,-20*4,
	88*3,84*3,-20*4,
	2*3,165*3,20*4,
	128*3,0*3,20*4,
	148*3,0*3,20*4,
	148*3,165*3,20*4,
	127*3,165*3,20*4,
	127*3,100*3,20*4,
	76*3,100*3,20*4,
	27*3,165*3,20*4,
	127*3,84*3,20*4,
	127*3,33*3,20*4,
	88*3,84*3,20*4]:INT,
	[
	9,10,21,2,	-> left inside A
		[4,9,10,10,21,21,20,20,9]:INT,0,
	8,9,20,1,	-> inside right A
		[4,8,9,9,20,20,19,19,8]:INT,0,
	4,5,16,4,	-> right inside A
		[4,4,5,5,16,16,15,15,4]:INT,0,
	5,6,17,5,	-> inside top leg
		[4,5,6,6,17,17,16,16,5]:INT,0,
	6,7,18,6,	-> left inside leg
		[4,6,7,7,18,18,17,17,6]:INT,0,
	10,8,19,3,	-> bottom inside A
		[4,10,8,8,19,19,21,21,10]:INT,0,
	0,1,12,7,	-> left out
		[4,0,1,1,12,12,11,11,0]:INT,0,
	1,2,13,8,	-> top
		[4,1,2,2,13,13,12,12,1]:INT,0,
	2,3,14,9,	-> right out
		[4,2,3,3,14,14,13,13,2]:INT,0,
	3,4,15,10,	-> bottom leg right
		[4,3,4,4,15,15,14,14,3]:INT,0,
	7,0,11,11,	-> bottom leg left
		[4,7,0,0,11,11,18,18,7]:INT,0,
	3,2,0,12,	-> front
		[11,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,0,8,9,9,10,10,8]:INT,0,
	11,13,14,13,	-> back
		[11,11,12,12,13,13,14,14,15,15,16,16,17,17,18,18,11,19,20,20,21,21,19]:INT,0]:face);


  let_2:=newVectorObject(0,24,14,
	[54*3,40*3,-20*4,
	27*3,34*3,-20*4,
	92*3,-2*3,-20*4,
	159*3,22*3,-20*4,
	141*3,74*3,-20*4,
	52*3,149*3,-20*4,
	137*3,149*3,-20*4,
	137*3,165*3,-20*4,
	2*3,165*3,-20*4,
	119*3,63*3,-20*4,
	125*3,31*3,-20*4,
	93*3,21*3,-20*4,

	54*3,40*3,20*4,
	27*3,34*3,20*4,
	92*3,-2*3,20*4,
	159*3,22*3,20*4,
	141*3,74*3,20*4,
	52*3,149*3,20*4,
	137*3,149*3,20*4,
	137*3,165*3,20*4,
	2*3,165*3,20*4,
	119*3,63*3,20*4,
	125*3,31*3,20*4,
	93*3,21*3,20*4]:INT,

	/* since no 'depth' sorting is done - ensure innermost surfaces drawn first */
	[
	5,6,18,19,	-> top of base
		[4,5,6,6,18,18,17,17,5]:INT,0,
	9,10,22,21,	-> inside top left
		[4,9,10,10,22,22,21,21,9]:INT,0,
	10,11,23,23,	-> inside top mid
		[4,10,11,11,23,23,22,22,10]:INT,0,
	11,0,12,24,	-> inside top left
		[4,11,0,0,12,12,23,23,11]:INT,0,
	8,9,21,16,	-> top of rasier
		[4,8,9,9,21,21,20,20,8]:INT,0,
	0,1,13,22,	-> front edge
		[4,0,1,1,13,13,12,12,0]:INT,0,
	4,5,17,17,	-> bottom of raiser
		[4,4,5,5,17,17,16,16,4]:INT,0,
	3,4,16,13,	-> right side
		[4,3,4,4,16,16,15,15,3]:INT,0,
	6,7,19,18,	-> right side of base
		[4,6,7,7,19,19,18,18,6]:INT,0,
	7,8,20,14,	-> base
		[4,7,8,8,20,20,19,19,7]:INT,0,
	2,3,15,15,	-> top right
		[4,2,3,3,15,15,14,14,2]:INT,0,
	1,2,14,20,	-> top left
		[4,1,2,2,14,14,13,13,1]:INT,0,

	2,1,0,25,	/* front face */
		[12,0,1,1,2,2,3,3,4,4,5,5,6,6,7,7,8,8,9,9,10,10,11,11,0]:INT,0,
	12,13,14,26,	/* rear face */
		[12,12,13,13,14,14,15,15,16,16,17,17,18,18,19,19,20,20,21,21,22,22,23,23,12]:INT,0]:face);


  /* add objects to the list, and position them accordingly */
  addVObject(list,let_A)
  let_A.px:=-1275-100
  addVObject(list,let_1)
  let_1.px:=-735-100
  addVObject(list,let_2)
  let_2.px:=-345-100
  tmp:=cloneVectorObject(let_0);
  addVObject(list,let_0)
  let_0.px:=195-100
  addVObject(list,tmp);
  tmp.px:=735-100

ENDPROC stat

PROC freeobjects()
  freeVList(list,1)       /* also free nodes */
ENDPROC



