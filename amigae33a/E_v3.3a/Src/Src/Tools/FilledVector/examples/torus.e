
/*

	Complex Workbench2.0+ Example of FilledVector.m

	This program creates a spinning Torus that spins
	around on the back of Workbench.  On a 1200 without
	fastmem, it can hapily be run in the background without
	causing noticeable delays.  This object is a bit
	more 'chunky'  than the a1200 logo in wb1200, and
	it can make the system feel a bit jerky.

	On the command line, specify the number of columns,
	followed by the number of rows of images you want,
	it defaults to 2x2.

	Run it with Workbench NOT inbackdrop mode, and to
	remove it, run it again (it will interact with
	the other wbbackdrop code in this archive)


	The Torus was created in Imagine 2.0, and then saved.
	The program tddd2nff (from t3dlib) was then used to
	decompose the object into NFF.  ReadNFF was then
	used to convert the object to FilledVector.m format,
	and following this, some hand editing (quite a lot ...)
	was used to create a useable object.


	Michael Zucchi, 1994.  Program in public domain.

	(this is identical to wb1200.e, but has a different
	object)
*/



OPT OSVERSION=37

MODULE 'tools/filledvector', 'tools/filledvdefs',
	'exec/lists', 'exec/ports', 'exec/nodes', 'exec/libraries',
	'intuition/screens', 'intuition/intuition', 'intuition/intuitionbase',
	'graphics/gfx', 'graphics/rastport', 'graphics/layers'

DEF list:PTR TO mlh,pc,
	ball:PTR TO vobject,
	scr:PTR TO screen,abitm:PTR TO bitmap,
	win:PTR TO window,
	myrast:rastport,
	mport:PTR TO mp,
	width,height,depth,
	rows,cols

DEF dx,dy,dz

/********************************************************************

	main

 */

PROC main()
DEF rdargs, args:PTR TO LONG,p:PTR TO LONG, i, gfx:PTR TO lib

cols:=2;
rows:=2;

-> check arguments
args:=[0,0];
IF rdargs:=ReadArgs('cols/N,rows/N', args, 0)
  IF p:=args[0] THEN cols:=p[0]
  IF p:=args[1] THEN rows:=p[0]
  FreeArgs(rdargs);
ENDIF  

-> see if we're already running, if yes, signal the other version
Forbid();
mport:=FindPort('backdrop');
IF mport
  Signal(mport.sigtask, Shl(1,mport.sigbit))
ENDIF
Permit();

-> if we are not already running, start doing our thing
IF mport=0

  -> create a public port where we can be found later
  mport:=CreateMsgPort();
  mport::ln.name:='backdrop';
  mport::ln.pri:=-128;
  AddPort(mport);

  -> open the backdrop window
  win:=OpenWindowTagList(0, [WA_FLAGS, WFLG_BACKDROP OR WFLG_BORDERLESS OR WFLG_SIMPLE_REFRESH OR WFLG_NOCAREREFRESH,
	WA_BACKFILL, LAYERS_NOBACKFILL, 0]);

  -> calculate size of each tile
  width:=win.width/cols
  height:=win.height/rows
  IF height AND 1 THEN height++
  IF width AND 31 THEN width:=width+(32-(width AND 31))

  -> find the depth of workbench, and open our rendering bitmap
  scr:=LockPubScreen(0);

  depth:=scr.rastport::rastport.bitmap::bitmap.depth
  gfx:=gfxbase;

  IF gfx.version<39
    abitm:=New(SIZEOF bitmap+(8*4));
    p:=abitm.planes
    FOR i:=0 TO depth-1 DO p[i]:=AllocRaster(width,height);
    InitBitMap(abitm,depth,width,height);
  ELSE
    abitm:=AllocBitMap(width,height,depth,
  	BMF_CLEAR+BMF_INTERLEAVED,0);
  ENDIF

  UnlockPubScreen(0, scr);

  -> finish of setting up, then goto the main loop
  SetTaskPri(FindTask(0), -128);
  InitRastPort(myrast);
  myrast.bitmap:=abitm;

  IF win
    IF createobjects()
      IF pc:=newPolyContext(abitm,50)
        demo()
        freePolyContext(pc)
      ENDIF
      freeobjects()
    ENDIF
    CloseWindow(win);
  ENDIF

  IF gfx.version<39
    p:=abitm.planes
    FOR i:=0 TO depth-1 DO FreeRaster(p[i], width,height);
    InitBitMap(abitm,depth,width,height);
  ELSE
    FreeBitMap(abitm);
  ENDIF

  RemPort(mport);
  DeleteMsgPort(mport);

ENDIF -> IF mport=0

ENDPROC

/********************************************************************

  Main loop.  Render/move the object until we get told to stop

 */

PROC demo()
  DEF p:position,x,y,c, sigbit, destz

  c:=0;
  p.ax:=0;p.ay:=0;p.az:=0;p.px:=0;p.py:=0;p.pz:=750;

  destz:=Rnd(1000)+400;
  sigbit:=Shl(1, mport.sigbit);
  setPolyFlags(pc,1,1)

  -> main loop
  WHILE (SetSignal(0, sigbit) AND sigbit)=0	-> see if we've been told to stop?

    cls()
    setPolyBitMap(pc, abitm)

    IF c--<0
      setangles()
      c:=60;
    ENDIF

    -> pri back to 0, this will stop us losing cpu while blitter owned (hang!)
    SetTaskPri(FindTask(0), 0);

    moveDrawVList(pc, list, p)
    FOR x:=0 TO cols-1
    FOR y:=0 TO rows-1
      BltBitMapRastPort(abitm, 0,0, win.rport, x*width,y*height,width,height,$c0);
    ENDFOR
    ENDFOR

    -> its safe to go back to low pri again
    SetTaskPri(FindTask(0), -128);

    -> move object
    p.pz:=p.pz+((destz-p.pz)/6)
    IF Abs(p.pz-destz)<6 THEN destz:=Rnd(4000)+200
    p.az:=p.az+dz;
    p.ax:=p.ax-dx;
    p.ay:=p.ay+dy;

  ENDWHILE
ENDPROC

PROC setangles()
dx:=Rnd(9)-4
dy:=Rnd(9)-4
dz:=Rnd(9)-4
ENDPROC


/********************************************************************

 Clears the bitmap we're rendering into, then fills it with the
 stipple pattern

 */

PROC cls()
 SetRast(myrast, 0);
 WaitBlit()

 MOVE.L abitm,A2
 MOVE.L 8(A2),A1
 MOVE.L	height,D3
 LSR.L	#1,D3
 SUBQ.L	#1,D3
 MOVE.L #$55555555,D0
 MOVE.L #$AAAAAAAA,D1
d_lp0:
 MOVE.L A1,A0
 ADDA.W (A2),A1
 MOVE.L width,D2
 LSR.W	#5,D2
 SUBQ.W	#1,D2
d_lp1:
 MOVE.L D0,(A0)+
 DBF    D2,d_lp1
 MOVE.L A1,A0
 ADDA.W (A2),A1
 MOVE.L	width,D2
 LSR.W	#5,D2
 SUBQ.W	#1,D2
d_lp2:
 MOVE.L D1,(A0)+
 DBF    D2,d_lp2
 DBF	D3,d_lp0
ENDPROC

/********************************************************************

 	Create all of the objects

 */ 

PROC createobjects()
  DEF stat=-1,
  	tmp:PTR TO vobject

  /* allocate a list, and add them to it */
  list:=newVList()

 ball:=newVectorObject(0,32,64, ->64,
	[141,-142,100,
	200,0,100,
	277,-115,0,
	277,114,0,
	141,141,100,
	114,277,0,
	0,200,100,
	-115,277,0,
	-142,141,100,
	-278,114,0,
	-200,0,100,
	-278,-115,0,
	-142,-142,100,
	-115,-278,0,
	0,-200,100,
	114,-278,0,
	200,0,-100,
	141,141,-100,
	0,200,-100,
	-142,141,-100,
	-200,0,-100,
	-142,-142,-100,
	0,-200,-100,
	141,-142,-100,
	92,38,0,
	38,92,0,
	-39,92,0,
	-93,38,0,
	-93,-39,0,
	-39,-93,0,
	38,-93,0,
	92,-39,0,
	0,0,0]:INT,
[24,17,16,32,[3,16,17,17,24,24,16]:INT,0,	->  all of these lines had their 
24,25,17,33,[3,24,25,25,17,17,24]:INT,0,
25,18,17,34,[3,17,18,18,25,25,17]:INT,0,	->  cross product numbers swapped
25,26,18,35,[3,25,26,26,18,18,25]:INT,0,
26,19,18,36,[3,18,19,19,26,26,18]:INT,0,	->
26,27,19,37,[3,26,27,27,19,19,26]:INT,0,
27,20,19,38,[3,19,20,20,27,27,19]:INT,0,	->
27,28,20,39,[3,27,28,28,20,20,27]:INT,0,
28,21,20,40,[3,20,21,21,28,28,20]:INT,0,	->
28,29,21,41,[3,28,29,29,21,21,28]:INT,0,
29,22,21,42,[3,21,22,22,29,29,21]:INT,0,	->
29,30,22,43,[3,29,30,30,22,22,29]:INT,0,
30,23,22,44,[3,22,23,23,30,30,22]:INT,0,	->
30,31,23,45,[3,30,31,31,23,23,30]:INT,0,
31,16,23,46,[3,23,16,16,31,31,23]:INT,0,	->
31,24,16,47,[3,31,24,24,16,16,31]:INT,0,
4,25,24,48,[3,24,25,25,4,4,24]:INT,0,	->
4,6,25,49,[3,4,6,6,25,25,4]:INT,0,
6,26,25,50,[3,25,26,26,6,6,25]:INT,0,	->
6,8,26,51,[3,6,8,8,26,26,6]:INT,0,
8,27,26,52,[3,26,27,27,8,8,26]:INT,0,	->
8,10,27,53,[3,8,10,10,27,27,8]:INT,0,
10,28,27,54,[3,27,28,28,10,10,27]:INT,0,	->
10,12,28,55,[3,10,12,12,28,28,10]:INT,0,
12,29,28,56,[3,28,29,29,12,12,28]:INT,0,	->
12,14,29,57,[3,12,14,14,29,29,12]:INT,0,
14,30,29,58,[3,29,30,30,14,14,29]:INT,0,	->
14,0,30,59,[3,14,0,0,30,30,14]:INT,0,
0,31,30,60,[3,30,31,31,0,0,30]:INT,0,	->
0,1,31,61,[3,0,1,1,31,31,0]:INT,0,
1,24,31,62,[3,31,24,24,1,1,31]:INT,0,	->
1,4,24,63,[3,1,4,4,24,24,1]:INT,0,

16,3,2,16,[3,2,3,3,16,16,2]:INT,0,	->
16,17,3,17,[3,16,17,17,3,3,16]:INT,0,
17,5,3,18,[3,3,5,5,17,17,3]:INT,0,	->
17,18,5,19,[3,17,18,18,5,5,17]:INT,0,
18,7,5,20,[3,5,7,7,18,18,5]:INT,0,	->
18,19,7,21,[3,18,19,19,7,7,18]:INT,0,
19,9,7,22,[3,7,9,9,19,19,7]:INT,0,	->
19,20,9,23,[3,19,20,20,9,9,19]:INT,0,
20,11,9,24,[3,9,11,11,20,20,9]:INT,0,	->
20,21,11,25,[3,20,21,21,11,11,20]:INT,0,
21,13,11,26,[3,11,13,13,21,21,11]:INT,0,	->
21,22,13,27,[3,21,22,22,13,13,21]:INT,0,
22,15,13,28,[3,13,15,15,22,22,13]:INT,0,	->
22,23,15,29,[3,22,23,23,15,15,22]:INT,0,
23,2,15,30,[3,15,2,2,23,23,15]:INT,0,	->
23,16,2,31,[3,23,16,16,2,2,23]:INT,0,

2,1,0,0,[3,0,1,1,2,2,0]:INT,0,	->
2,3,1,1,[3,2,3,3,1,1,2]:INT,0,
3,4,1,2,[3,1,4,4,3,3,1]:INT,0,	->
3,5,4,3,[3,3,5,5,4,4,3]:INT,0,
5,6,4,4,[3,4,6,6,5,5,4]:INT,0,	->
5,7,6,5,[3,5,7,7,6,6,5]:INT,0,
7,8,6,6,[3,6,8,8,7,7,6]:INT,0,	->
7,9,8,7,[3,7,9,9,8,8,7]:INT,0,
9,10,8,8,[3,8,10,10,9,9,8]:INT,0,	->
9,11,10,9,[3,9,11,11,10,10,9]:INT,0,
11,12,10,10,[3,10,12,12,11,11,10]:INT,0,	->
11,13,12,11,[3,11,13,13,12,12,11]:INT,0,
13,14,12,12,[3,12,14,14,13,13,12]:INT,0,	->
13,15,14,13,[3,13,15,15,14,14,13]:INT,0,
15,0,14,14,[3,14,0,0,15,15,14]:INT,0,	->
15,2,0,15,[3,15,2,2,0,0,15]:INT,0]:face)


  ball.pz:=0
  addVObject(list,ball)


ENDPROC stat

PROC freeobjects()
  freeVList(list,1)       /* also free nodes */
ENDPROC



