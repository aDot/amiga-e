-> protracker player

MODULE 'tools/file', 'tools/pt', 'tools/exceptions'

PROC main() HANDLE
  DEF m,l
  WriteF('Protracker player in E (now playing: "\s", Ctrl-C to stop).\n',arg)
  m,l:=readfile(arg,0,2)
  pt_play(m)
  REPEAT
    Delay(10)      -> instead, we could be doing something very usefull here
  UNTIL CtrlC()
  pt_stop()
EXCEPT DO
  report_exception()
ENDPROC
