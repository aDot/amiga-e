/*

	picwindow.e                          Michael Zucchi 1994

	Loads a picture into a window on workbench (without colours ...)
	Demonstrates use of the ILBM MODULE - loading into bitmaps, and obtaining
	picture information.  It also demonstrates asl file requester

	This program may be freely distributed only among registered
	AmigaE owners.
 */


MODULE 'tools/ilbm', 'tools/ilbmdefs',
	'intuition/intuition',
	'asl', 'libraries/ASL'

DEF bm,win:PTR TO window,
	buffer[256]:ARRAY

PROC main()
DEF ilbm,filename,width,height,bmh:PTR TO bmhd,pi:PTR TO picinfo

IF filename:=requestfile('Select picture')
	IF ilbm:=ilbm_New(filename,0)
		ilbm_LoadPicture(ilbm,[ILBML_GETBITMAP,{bm},0])

		-> get a pointer TO the images picture-info, we extract the bitmap header,
		-> and read the picture's size.
		pi:=ilbm_PictureInfo(ilbm)
		bmh:=pi.bmhd;
		width:=bmh.w;
		height:=bmh.h;

		-> the ilbm-handle is no longer needed, we can free it
		ilbm_Dispose(ilbm)

		-> if a bitmap actually opened, open a window, and blit it in
		IF bm
			IF win:=OpenWindowTagList(0,[WA_INNERWIDTH,width,WA_INNERHEIGHT,height,
				WA_AUTOADJUST,-1,
				WA_IDCMP,IDCMP_CLOSEWINDOW,
				WA_FLAGS,WFLG_CLOSEGADGET+WFLG_DRAGBAR+WFLG_DEPTHGADGET,
				WA_TITLE,filename,
				WA_SCREENTITLE,'Pic-Window 0.1 1994 Michael Zucchi',0])

				-> bit into actual dimensions the OS could give us (the window might not be as big as the picture)
				BltBitMapRastPort(bm,0,0,win.rport,
					win.borderleft,win.bordertop,
					win.width-win.borderright-win.borderleft,
					win.height-win.borderbottom-win.bordertop,$c0);

				WaitPort(win.userport)
				CloseWindow(win)

			ENDIF
			ilbm_FreeBitMap(bm)
		ENDIF
	ENDIF
ENDIF

ENDPROC

/*
	Presents an ASL load-file requester.  If the user selected a file, it is
	expanded to a full path-name.
 */
PROC requestfile(title)
DEF name=0,fr:PTR TO filerequester

IF aslbase:=OpenLibrary('asl.library',36)
	IF fr:=AllocAslRequest(ASL_FILEREQUEST,[ASLFR_TITLETEXT,title,0])
		IF AslRequest(fr,0)

			-> sorry, a bit of ASM here.  Well ... how ELSE?
			-> this does a strcpy() ...
			MOVE.L	fr,A0
			MOVE.L	8(A0),A0	-> directory pointer from 'filerequester'
			MOVE.L	buffer,A1
		cp:	MOVE.B	(A0)+,(A1)+
			BNE.S	cp

			AddPart(buffer,fr.file,256)
			name:=buffer
		ENDIF
		FreeAslRequest(fr)
	ENDIF
	CloseLibrary(aslbase)
ENDIF

ENDPROC name

