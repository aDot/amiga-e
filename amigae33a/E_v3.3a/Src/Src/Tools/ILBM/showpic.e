/*

	showpic.e                          Michael Zucchi 1994

	Simple IFF ILBM picture viewer
	Demonstrates use of the ILBM module, and the asl file requester

	This program may be freely distributed only among registered
	AmigaE owners.
 */


MODULE 'tools/ilbm', 'tools/ilbmdefs',
	'intuition/intuition',
	'asl', 'libraries/ASL'

DEF scr,
	buffer[256]:ARRAY

PROC main()
DEF ilbm,filename

IF filename:=requestfile('Select picture')
	IF ilbm:=ilbm_New(filename,0)
		ilbm_LoadPicture(ilbm,[ILBML_GETSCREEN,{scr},0])
		ilbm_Dispose(ilbm)	-> no longer needed ...

		-> this is only an example!  In a real application, always use IDCMP ports,
		-> and windows

		IF scr			-> only if one was created.
			WHILE Mouse()<>1
				Delay(4)
			ENDWHILE
			CloseScreen(scr)
		ENDIF
	ENDIF
ENDIF

ENDPROC

/*
	Presents an ASL load-file requester.  If the user selected a file, it is
	expanded to a full path-name.
 */
PROC requestfile(title)
DEF name=0,fr:PTR TO filerequester

IF aslbase:=OpenLibrary('asl.library',36)
	IF fr:=AllocAslRequest(ASL_FILEREQUEST,[ASLFR_TITLETEXT,title,0])
		IF AslRequest(fr,0)

			-> sorry, a bit of ASM here.  Well ... how ELSE?
			-> this does a strcpy() ...
			MOVE.L	fr,A0
			MOVE.L	8(A0),A0	-> directory pointer from 'filerequester'
			MOVE.L	buffer,A1
		cp:	MOVE.B	(A0)+,(A1)+
			BNE.S	cp

			AddPart(buffer,fr.file,256)
			name:=buffer
		ENDIF
		FreeAslRequest(fr)
	ENDIF
	CloseLibrary(aslbase)
ENDIF

ENDPROC name

