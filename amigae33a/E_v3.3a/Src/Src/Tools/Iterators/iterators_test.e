-> iterators test

MODULE 'tools/exceptions', 'tools/constructors', '*iterators',
       'exec/lists', 'exec/nodes'

OBJECT bla OF ln
  v:LONG
ENDOBJECT

PROC main() HANDLE
  DEF l,n:PTR TO bla,x=10
  l:=newlist()
  WHILE x>0 DO AddHead(l,NEW n.n(x--))
  iterate_exec_list({n},l,`WriteF('v = \d\n',n.v))
EXCEPT
  report_exception()
ENDPROC

PROC n(v) OF bla
  self.v:=v
ENDPROC
