/*

idea: Piet Molenaar

calculates (a*a-4)*3 for a = 2 to .. 
finds the "a"'s (integers, a,a+1,a+2 are the lenghts of the sides
of a triangle (with almost equals sides)), such that the surface is
a whole number! how remarkable!

*/

MODULE 'tools/longrealtiny'

PROC main()
  DEF a:longreal, b:longreal, c:longreal, d:longreal, e:longreal, s[100]:STRING, x
  dInit()
  dFloat(4,b)
  dFloat(3,c)
  FOR x:=2 TO 40000
    IF (x AND $FFF)=0 THEN WriteF('busy doing: \d\n',x)
    IF CtrlC() THEN RETURN
    dRound(dCopy(e,dSqrt(dMul(dSub(dMul(dFloat(x,a),a,d),b),c))))
    IF dCompare(e,d)=0 THEN WriteF('\d <=> \s\n',x,dFormat(s,d,20))
  ENDFOR
  dCleanup()
ENDPROC
