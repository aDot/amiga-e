/* Example program for longreal module */
/* By EA van Breemen 1994              */


/* !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! */
/* In order to use the conversion functions use STRING type */
/* for the buffers, not ARRAY. Otherwise the functions will */
/* produce unexpected results                               */

/* Note some roundingerrors may occur due to IEEE maths     */
/* In futher releases this will be fixed                    */


/* Some notes on the functions in longreal                  */

/* Most functions are described in the ROMKernals. See the  */
/* chapters on IEEE functions                               */
/* There are 3 ascii-longreal conversion functions          */

/* dFormat(buffer,x,num)  -> converts fractional number x with
                             num digits to a string in buffer

   dLFormat(buffer,x,num) -> same as dFormat, but now also large
                             numbers i.e. 1.2e250           

   a2d(buffer,x)          -> the inverse of dLFormat: converting
                             from asciistring in buffer to 
                             longreal x                      */


/* Include this module for using longreals */

MODULE 'tools/longreal'


/* Our small main program */

PROC main()
  DEF buffer[256]:STRING     /* Very important: use STRING for buffer !!!! */
  DEF a:longreal             /* Our dummy longreal for results     */
  DEF i                      /* A simple counter                   */

  dInit()                    /* Init the module before using */

  WriteF('First some conversions:\n')
  WriteF('Reading 1.234567      -> gives:')

  a2d('1.234567',a)          /* Convert from ascii to longreal */
  dFormat(buffer,a,6)        /* And back again (6 digits)      */
  WriteF('\s\n',buffer)      /* Print it                       */

  WriteF('Reading +1.234567e-2  -> gives:')

  a2d('+1.234567e-2',a)      /* Convert from ascii to longreal */
  dFormat(buffer,a,6)        /* And back again (6 digits)      */
  WriteF('\s\n',buffer)      /* Print it                       */


  WriteF('Reading -1.234567E100 -> gives:')

  a2d('-1.234567E100',a)     /* Convert from ascii to longreal */

/* Now the number is too large for dFormat, use dLFormat instead */

  dLFormat(buffer,a,6)       /* And back again (6 digits)      */
  WriteF('\s\n',buffer)      /* Print it                       */

  WriteF('Now some other stuff\n')

  FOR i:=1 TO 16
    WriteF('PI=\s \n',dFormat(buffer,dPi(a),i))
  ENDFOR

  WriteF('A sinus table\n')
  FOR i:=0 TO 360 STEP 45
    dFloat(i,a)              /* Convert an int to a longreal */
    dSin(dRad(a))
    WriteF('Sin(\d)=\s \n',i,dLFormat(buffer,a,15))
  ENDFOR
 
  WriteF('End of longdemo\n')

  dCleanup()                 /* Cleanup the module after using */

ENDPROC
