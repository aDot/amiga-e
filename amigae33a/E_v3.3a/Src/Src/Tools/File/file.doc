FILE.M: general file handling functions.

	mem,len:=readfile(filename,trailbyte="\n",memflags=0)

reads file "filename", trailing both at begin and end with 4 bytes each.
The purpose of trailing is to enable fast searching; "\n" makes sense
if filename is a text file, 0 for binary files.
returns: ptr file was loaded to, and length.
raises: "NEW", "OPEN" filename, "IN"

	freefile(mem)

frees memory allocated by readfile(). if you don't call freefile(),
memory is deallocated at the end of the program.

	writefile(filename,mem,len)

writes a block of memory as file.
raises: "OPEN" filename, "OUT"

	num:=countstrings(mem,len)

counts strings in memory block. mem must be "\n" trailed.
returns: #of strings

	list:=stringsinfile(mem,len,max)

builds a LIST of nil-terminated strings, gathered from mem.
max is maxsize of list, which can either be obtained from countstrings()
(slow but accurate) or from your own guess (fast but inaccurate)
returns: LIST of C-string pointers
raises: "MEM"
notes: - the contents of mem will be altered (i.e. "\n" bytes changed to 0)
       - mem must be "\n" trailed.
       - if ListLen(list) is equal to max, most likely max was too small
