-> test files

MODULE 'tools/file', 'tools/exceptions'

PROC main() HANDLE
  DEF m,l,n,list,x,y=1
  m,l:=readfile(arg)
  WriteF('file has \d lines:\n\n',n:=countstrings(m,l))
  list:=stringsinfile(m,l,n)
  ForAll({x},list,`WriteF('\d\t\s\n',y++,x))
EXCEPT
  report_exception()
ENDPROC
