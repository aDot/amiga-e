OPT MODULE

MODULE 'other/sendrexx'

CONST MAXLINE=512

EXPORT PROC sendExplorer(addr,obj=NIL,repPort=NIL,message=NIL,quiet=FALSE)
  DEF s[MAXLINE]:STRING
  StringF(s, '''DISPLAY \s$\h OBJECT "\s" MESSAGE "\s"''',
          IF quiet THEN 'QUIET ' ELSE '', addr,
          IF obj THEN obj ELSE '', IF message THEN message ELSE '')
ENDPROC rx_SendMsg('EXPLORER',s,repPort)

EXPORT PROC quitExplorer(repPort=NIL) IS rx_SendMsg('EXPLORER','''QUIT''',repPort)

EXPORT PROC isExplorerRunning() IS FindPort('EXPLORER')<>NIL
