OPT MODULE

EXPORT DEF battclockbase

EXPORT PROC resetBattClock()
  MOVE.L battclockbase, A6
  JSR -6(A6)  -> Offset of real ResetBattClock
ENDPROC

EXPORT PROC readBattClock()
  MOVE.L battclockbase, A6
  JSR -12(A6)  -> Offset of real ReadBattClock
ENDPROC D0

EXPORT PROC writeBattClock(time)
  MOVE.L time, D0
  MOVE.L battclockbase, A6
  JSR -18(A6)  -> Offset of real WriteBattClock
ENDPROC
