OPT MODULE, PREPROCESS

MODULE 'exec/ports',
       'exec/nodes',
       'rexx/rxslib',
       'rexx/storage',
       'rexxsyslib',
       'amigalib/ports'

ENUM ERR_NONE, ERR_LIB, ERR_RMSG, ERR_RSTR

RAISE ERR_LIB  IF OpenLibrary()=NIL,
      ERR_RMSG IF CreateRexxMsg()=NIL,
      ERR_RSTR IF CreateArgstring()=NIL

EXPORT PROC rx_SendMsg(portname, s, repPort=NIL) HANDLE
  DEF port=NIL, msg=NIL:PTR TO rexxmsg, myrep=NIL, lib=NIL, success=FALSE
  IF rexxsysbase=NIL
    rexxsysbase:=lib:=OpenLibrary(RXSNAME, 0)
  ENDIF
  IF repPort=NIL
    IF NIL=(repPort:=myrep:=createPort(NIL,0)) THEN Raise("PORT")
  ENDIF
  msg:=CreateRexxMsg(repPort, NIL, portname)
  msg.action:=RXCOMM
  msg.args[]:=CreateArgstring(s, StrLen(s))
  msg.mn.ln.name:=RXSDIR
  Forbid()
  IF port:=FindPort(RXSDIR) THEN PutMsg(port, msg)
  Permit()
  IF port
    WaitPort(repPort)
    msg:=GetMsg(repPort)
    success:=(msg.result1=0)
  ENDIF
EXCEPT DO
  IF msg
    IF msg.args[] THEN DeleteArgstring(msg.args[])
    DeleteRexxMsg(msg)
  ENDIF
  IF myrep THEN deletePort(myrep)
  IF lib
    CloseLibrary(lib)
    rexxsysbase:=NIL
  ENDIF
ENDPROC success
