OPT MODULE

EXPORT PROC addICRVector(resource, iCRBit, interrupt)
  MOVE.L iCRBit, D0
  MOVE.L interrupt, A1
  MOVE.L resource, A6
  JSR -6(A6)  -> Offset of real AddICRVector
ENDPROC D0

EXPORT PROC remICRVector(resource, iCRBit, interrupt)
  MOVE.L iCRBit, D0
  MOVE.L interrupt, A1
  MOVE.L resource, A6
  JSR -12(A6)  -> Offset of real RemICRVector
ENDPROC

EXPORT PROC ableICR(resource, mask)
  MOVE.L mask, D0
  MOVE.L resource, A6
  JSR -18(A6)  -> Offset of real AbleICR
ENDPROC D0

EXPORT PROC setICR(resource, mask)
  MOVE.L mask, D0
  MOVE.L resource, A6
  JSR -24(A6)  -> Offset of real SetICR
ENDPROC D0
