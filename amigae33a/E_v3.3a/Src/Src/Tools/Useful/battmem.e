OPT MODULE

EXPORT DEF battmembase

EXPORT PROC obtainBattSemaphore()
  MOVE.L battmembase, A6
  JSR -6(A6)  -> Offset of real ObtainBattSemaphore
ENDPROC

EXPORT PROC releaseBattSemaphore()
  MOVE.L battmembase, A6
  JSR -12(A6)  -> Offset of real ReleaseBattSemaphore
ENDPROC

EXPORT PROC readBattMem(buffer, offset, length)
  MOVE.L buffer, A0
  MOVE.L offset, D0
  MOVE.L length, D1
  MOVE.L battmembase, A6
  JSR -18(A6)  -> Offset of real ReadBattMem
ENDPROC D0

EXPORT PROC writeBattMem(buffer, offset, length)
  MOVE.L buffer, A0
  MOVE.L offset, D0
  MOVE.L length, D1
  MOVE.L battmembase, A6
  JSR -24(A6)  -> Offset of real WriteBattMem
ENDPROC D0
