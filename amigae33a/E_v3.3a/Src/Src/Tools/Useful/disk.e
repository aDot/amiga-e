OPT MODULE

EXPORT DEF diskbase

EXPORT PROC allocUnit(unitNum)
  MOVE.L unitNum, D0
  MOVE.L diskbase, A6
  JSR -6(A6)  -> Offset of real AllocUnit
ENDPROC D0

EXPORT PROC freeUnit(unitNum)
  MOVE.L unitNum, D0
  MOVE.L diskbase, A6
  JSR -12(A6)  -> Offset of real FreeUnit
ENDPROC

EXPORT PROC getUnit(unitPointer)
  MOVE.L unitPointer, A1
  MOVE.L diskbase, A6
  JSR -18(A6)  -> Offset of real GetUnit
ENDPROC D0

EXPORT PROC giveUnit()
  MOVE.L diskbase, A6
  JSR -24(A6)  -> Offset of real GiveUnit
ENDPROC

EXPORT PROC getUnitID(unitNum)
  MOVE.L unitNum, D0
  MOVE.L diskbase, A6
  JSR -30(A6)  -> Offset of real GetUnitID
ENDPROC D0

EXPORT PROC readUnitID(unitNum)
  MOVE.L unitNum, D0
  MOVE.L diskbase, A6
  JSR -36(A6)  -> Offset of real ReadUnitID
ENDPROC D0
