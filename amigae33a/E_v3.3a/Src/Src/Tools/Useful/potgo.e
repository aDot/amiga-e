OPT MODULE

EXPORT DEF potgobase

EXPORT PROC allocPotBits(bits)
  MOVE.L bits, D0
  MOVE.L potgobase, A6
  JSR -6(A6)  -> Offset of real AllocPotBits
ENDPROC D0

EXPORT PROC freePotBits(bits)
  MOVE.L bits, D0
  MOVE.L potgobase, A6
  JSR -12(A6)  -> Offset of real FreePotBits
ENDPROC

EXPORT PROC writePotgo(word, mask)
  MOVE.L word, D0
  MOVE.L mask, D1
  MOVE.L potgobase, A6
  JSR -18(A6)  -> Offset of real WritePotgo
ENDPROC
