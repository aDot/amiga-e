OPT MODULE

EXPORT DEF miscbase

EXPORT PROC allocMiscResource(unitNum, name)
  MOVE.L unitNum, D0
  MOVE.L name, A1
  MOVE.L miscbase, A6
  JSR -6(A6)  -> Offset of real AllocMiscResource
ENDPROC D0

EXPORT PROC freeMiscResource(unitNum)
  MOVE.L unitNum, D0
  MOVE.L miscbase, A6
  JSR -12(A6)  -> Offset of real FreeMiscResource
ENDPROC
