OPT MODULE

MODULE 'exec/memory',
       'hardware/custom'
  
-> Wraps an E function so it can still access globals, even from other tasks.
EXPORT PROC eCode(func) IS setup(func,{start},{end}-{start})

-> Wraps an E function as above, but also preserves the non-scratch registers.
EXPORT PROC eCodePreserve(func) IS setup(func,{pStart},{pEnd}-{pStart})

-> Wraps an E function for use with createTask()
EXPORT PROC eCodeTask(func) IS eCode(func)

-> Wraps an E function for use as an ASL hook
EXPORT PROC eCodeASLHook(func) IS setup(func,{aStart},{aEnd}-{aStart})

-> Wraps an E function for use as an CX custom function
EXPORT PROC eCodeCxCustom(func) IS setup(func,{cStart},{cEnd}-{cStart})

-> Wraps an E function for use as a GEL collision function
EXPORT PROC eCodeCollision(func) IS eCodeCxCustom(func)

-> Wraps an E function for use as an interrupt handler
EXPORT PROC eCodeIntHandler(func) IS setup(func,{hStart},{hEnd}-{hStart})

-> Wraps an E function for use as an interrupt server
EXPORT PROC eCodeIntServer(func) IS setup(func,{sStart},{sEnd}-{sStart})

-> Wraps an E function for use as a software interrupt
EXPORT PROC eCodeSoftInt(func) IS setup(func,{iStart},{iEnd}-{iStart})

-> Wraps an E function as eCode(), but swaps the order of two args
EXPORT PROC eCodeSwapArgs(func) IS setup(func,{oStart},{oEnd}-{oStart})

EXPORT PROC eCodeDispose(mem) IS IF mem THEN Dispose(mem-8) ELSE NIL

PROC setup(func, addr, len) HANDLE
  DEF mem:PTR TO LONG, a4
  mem:=NewM(len, MEMF_PUBLIC)
  -> Fully relocatable code can be copied to another memory location
  CopyMem(addr, mem, len)
  mem[]++:=func
  MOVE.L A4, a4
  mem[]++:=a4
  IF KickVersion(36) THEN CacheClearU()  -> Write out the cache (68040 especially bad...)
  RETURN mem
EXCEPT
  RETURN NIL
ENDPROC

start:
function:
  LONG 0
storeA4:
  LONG 0
entry:
  MOVE.L storeA4(PC), A4      -> Restore A4
  MOVE.L function(PC), -(A7)
  RTS                         -> Call real function
end:
  NOP

pStart:
pFunction:
  LONG 0
pStoreA4:
  LONG 0
pEntry:
  MOVEM.L D2-D7/A2-A6, -(A7)  -> Preserve registers
  MOVE.L pStoreA4(PC), A4     -> Restore A4
  MOVE.L pFunction(PC), A5
  JSR (A5)                    -> Call real function
  MOVEM.L (A7)+, D2-D7/A2-A6  -> Restore registers
  RTS
pEnd:
  NOP

aStart:
aFunction:
  LONG 0
aStoreA4:
  LONG 0
aEntry:
  MOVE.L (A7)+, A1            -> Remember the caller
  MOVE.L (A7), A0             -> Swap 3 arguments
  MOVE.L 8(A7), (A7)
  MOVE.L A0, 8(A7)
  LEA aStack(PC), A0
  MOVEM.L D2-D7/A1-A6, (A0)   -> Preserve registers
  MOVE.L aStoreA4(PC), A4     -> Restore A4
  MOVE.L aFunction(PC), A0
  JSR (A0)                    -> Call real function
  LEA aStack(PC), A0
  MOVEM.L (A0), D2-D7/A1-A6   -> Restore registers
  MOVE.L A1, -(A7)            -> Restore caller
  RTS
aStack:
  LONG 2,3,4,5,6,7,1,2,3,4,5,6,0
aEnd:
  NOP

cStart:
cFunction:
  LONG 0
cStoreA4:
  LONG 0
cEntry:
  MOVE.L (A7)+, A1            -> Remember the caller
  MOVE.L (A7), A0             -> Swap 2 arguments
  MOVE.L 4(A7), (A7)
  MOVE.L A0, 4(A7)
  LEA cStack(PC), A0
  MOVEM.L D2-D7/A1-A6, (A0)   -> Preserve registers
  MOVE.L cStoreA4(PC), A4     -> Restore A4
  MOVE.L cFunction(PC), A0
  JSR (A0)                    -> Call real function
  LEA cStack(PC), A0
  MOVEM.L (A0), D2-D7/A1-A6   -> Restore registers
  MOVE.L A1, -(A7)            -> Restore caller
  RTS
cStack:
  LONG 2,3,4,5,6,7,1,2,3,4,5,6,0
cEnd:
  NOP

hStart:
hFunction:
  LONG 0
hStoreA4:
  LONG 0
hEntry:
  LEA hStack(PC), A0
  MOVEM.L D2-D7/A2-A4, (A0)  -> Preserve registers
  MOVE.L hStoreA4(PC), A4    -> Restore A4
  MOVE.L hFunction(PC), A0
  MOVEM.L D1/A1, -(A7)       -> Push D1 and A1 as arguments
  JSR (A0)                   -> Call real function
  LEA 8(A7), A7              -> Remove arguments
  LEA hStack(PC), A0
  MOVEM.L (A0), D2-D7/A2-A4  -> Restore registers
  RTS
hStack:
  LONG 2,3,4,5,6,7,2,3,4,0
hEnd:
  NOP

sStart:
sFunction:
  LONG 0
sStoreA4:
  LONG 0
sEntry:
  LEA sStack(PC), A6
  MOVEM.L D2-D7/A2-A4, (A6)  -> Preserve registers
  MOVE.L sStoreA4(PC), A4    -> Restore A4
  MOVE.L sFunction(PC), A6
  MOVE.L A1, -(A7)           -> Push A1 as an argument
  JSR (A6)                   -> Call real function
  LEA 4(A7), A7              -> Remove argument
  LEA sStack(PC), A6
  MOVEM.L (A6), D2-D7/A2-A4  -> Restore registers
  MOVE.L #CUSTOMADDR, A0     -> Reset A0 to custom base
  TST.L D0                   -> Set Z flag according to func result
  RTS
sStack:
  LONG 2,3,4,5,6,7,2,3,4,0
sEnd:
  NOP

iStart:
iFunction:
  LONG 0
iStoreA4:
  LONG 0
iEntry:
  LEA iStack(PC), A0
  MOVEM.L D2-D7/A2-A4/A6, (A0)  -> Preserve registers
  MOVE.L iStoreA4(PC), A4       -> Restore A4
  MOVE.L iFunction(PC), A0
  MOVE.L A1, -(A7)              -> Push A1 as an argument
  JSR (A0)                      -> Call real function
  LEA 4(A7), A7                 -> Remove argument
  LEA iStack(PC), A0
  MOVEM.L (A0), D2-D7/A2-A4/A6  -> Restore registers
  RTS
iStack:
  LONG 2,3,4,5,6,7,2,3,4,6,0
iEnd:
  NOP

oStart:
oFunction:
  LONG 0
oStoreA4:
  LONG 0
oEntry:
  MOVE.L 4(A7), A0             -> Swap 2 arguments
  MOVE.L 8(A7), 4(A7)
  MOVE.L A0, 8(A7)
  MOVE.L oStoreA4(PC), A4      -> Restore A4
  MOVE.L oFunction(PC), -(A7)
  RTS                          -> Call real function
oEnd:
  NOP


