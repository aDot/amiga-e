OPT MODULE, PREPROCESS

MODULE 'intuition/intuition', 'intuition/gadgetclass',
       'libraries/gadtools',
       'tools/textlen',
       'tools/EasyGUI',
       'workbench/startup', 'workbench/workbench',
       'gadtools', 'icon', 'wb'

RAISE "icfy" IF AddAppIconA()=NIL,
      "icfy" IF OpenLibrary()=NIL,
      "icfy" IF CreateMsgPort()=NIL

DEF iconbase  -> Redefine for privateness

-> Share gadtoolbase and workbenchbase with EasyGUI...

#define DEFICON 'env:sys/def_tool'

EXPORT OBJECT iconify OF plugin
  disabled
PRIVATE
  iconify:PTR TO gadget
  iconopen
  label, icon, iconlabel
  resize
ENDOBJECT

PROC iconify(label,icon=NIL,iconlabel=NIL,
             resizex=FALSE,resizey=FALSE,disabled=FALSE) OF iconify
  iconbase:=OpenLibrary('icon.library',37)
  self.iconopen:=TRUE
  self.label:=IF label THEN label ELSE ''
  self.icon:=IF icon THEN icon ELSE DEFICON
  self.iconlabel:=IF iconlabel THEN iconlabel ELSE self.label
  self.disabled:=disabled
  self.resize:=(IF resizex THEN RESIZEX ELSE 0) OR
               (IF resizey THEN RESIZEY ELSE 0)
ENDPROC

PROC end() OF iconify
  IF self.iconopen
    CloseLibrary(iconbase)
    self.iconopen:=FALSE
  ENDIF
ENDPROC

PROC min_size(ta,fh) OF iconify IS textlen(self.label,ta)+16,fh+6

PROC will_resize() OF iconify IS self.resize

-> Don't need to define this:
->PROC render(ta,x,y,xs,ys,w) OF iconify IS EMPTY

PROC gtrender(gl,vis,ta,x,y,xs,ys,w) OF iconify
  -> Or, a gadget in the title bar would have also been nice...
  self.iconify:=CreateGadgetA(BUTTON_KIND,gl,
                 [x,y,xs,ys,self.label,ta,0,
                  PLACETEXT_IN,vis,NIL]:newgadget, [NIL])
  IF self.iconify=NIL THEN Raise("icfy")
ENDPROC self.iconify

-> Don't need to define this:
-> PROC clear_render(win:PTR TO window) OF iconify IS EMPTY

PROC message_test(imsg:PTR TO intuimessage,win:PTR TO window) OF iconify
  IF imsg.class=IDCMP_GADGETUP THEN RETURN imsg.iaddress=self.iconify
ENDPROC FALSE

PROC message_action(class,qual,code,win:PTR TO window) OF iconify HANDLE
  DEF dobj=NIL:PTR TO diskobject, myport=NIL, appicon=NIL,
      appmsg:PTR TO appmessage
  closewin(self.gh)
  -> Fallback to using a default icon if not found...
  IF NIL=(dobj:=GetDiskObjectNew(self.icon)) THEN dobj:=GetDiskObjectNew(DEFICON)
  dobj.type:=NIL
  myport:=CreateMsgPort()
  appicon:=AddAppIconA(0,0,self.iconlabel,myport,NIL,dobj,NIL)
  WaitPort(myport)
EXCEPT DO
  IF appicon THEN RemoveAppIcon(appicon)
  IF myport
    -> Clear away any messages that arrived at the last moment
    WHILE appmsg:=GetMsg(myport) DO ReplyMsg(appmsg)
    DeleteMsgPort(myport)
  ENDIF
  IF dobj THEN FreeDiskObject(dobj)
  openwin(self.gh)
ENDPROC FALSE

PROC setdisabled(disabled=TRUE) OF iconify
  Gt_SetGadgetAttrsA(self.iconify,self.gh.wnd,NIL,[GA_DISABLED,disabled,NIL])
  self.disabled:=disabled
ENDPROC
