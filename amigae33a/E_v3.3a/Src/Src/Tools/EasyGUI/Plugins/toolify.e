OPT MODULE, PREPROCESS

MODULE 'intuition/intuition', 'intuition/gadgetclass',
       'libraries/gadtools',
       'tools/textlen',
       'tools/EasyGUI',
       'workbench/startup', 'workbench/workbench',
       'gadtools', 'wb'

RAISE "tlfy" IF AddAppMenuItemA()=NIL,
      "tlfy" IF CreateMsgPort()=NIL

-> Share gadtoolsbase and workbenchbase with EasyGUI

EXPORT OBJECT toolify OF plugin
  disabled
PRIVATE
  toolify:PTR TO gadget
  label, toollabel
  resize
ENDOBJECT

PROC toolify(label,toollabel=NIL,
             resizex=FALSE,resizey=FALSE,disabled=FALSE) OF toolify
  self.label:=IF label THEN label ELSE ''
  self.toollabel:=IF toollabel THEN toollabel ELSE self.label
  self.disabled:=disabled
  self.resize:=(IF resizex THEN RESIZEX ELSE 0) OR
               (IF resizey THEN RESIZEY ELSE 0)
ENDPROC

PROC end() OF toolify IS EMPTY

PROC min_size(ta,fh) OF toolify IS textlen(self.label,ta)+16,fh+6

PROC will_resize() OF toolify IS self.resize

-> Don't need to define this:
->PROC render(ta,x,y,xs,ys,w) OF toolify IS EMPTY

PROC gtrender(gl,vis,ta,x,y,xs,ys,w) OF toolify
  -> Or, a gadget in the title bar would have also been nice...
  self.toolify:=CreateGadgetA(BUTTON_KIND,gl,
                 [x,y,xs,ys,self.label,ta,0,
                  PLACETEXT_IN,vis,NIL]:newgadget, [NIL])
  IF self.toolify=NIL THEN Raise("tlfy")
ENDPROC self.toolify

-> Don't need to define this:
-> PROC clear_render(win:PTR TO window) OF toolify IS EMPTY

PROC message_test(imsg:PTR TO intuimessage,win:PTR TO window) OF toolify
  IF imsg.class=IDCMP_GADGETUP THEN RETURN imsg.iaddress=self.toolify
ENDPROC FALSE

PROC message_action(class,qual,code,win:PTR TO window) OF toolify HANDLE
  DEF myport=NIL, appitem=NIL, appmsg:PTR TO appmessage
  closewin(self.gh)
  myport:=CreateMsgPort()
  appitem:=AddAppMenuItemA(0,0,self.toollabel,myport,NIL)
  WaitPort(myport)
EXCEPT DO
  IF appitem THEN RemoveAppMenuItem(appitem)
  IF myport
    -> Clear away any messages that arrived at the last moment
    WHILE appmsg:=GetMsg(myport) DO ReplyMsg(appmsg)
    DeleteMsgPort(myport)
  ENDIF
  openwin(self.gh)
ENDPROC FALSE

PROC setdisabled(disabled=TRUE) OF toolify
  Gt_SetGadgetAttrsA(self.toolify,self.gh.wnd,NIL,[GA_DISABLED,disabled,NIL])
  self.disabled:=disabled
ENDPROC
