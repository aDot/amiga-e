MODULE 'tools/exceptions',
       'tools/easygui',
       'other/split',
       'plugins/iconify'

PROC main() HANDLE
  DEF args:PTR TO LONG, p=NIL:PTR TO iconify
  IF args:=argSplit()
    IF args[]
      PrintF('Using the icon for "\s"\n',args[])
    ELSE
      PrintF('Using default icon (try "sys:tools/CMD" as an argument)\n')
    ENDIF
    NEW p.iconify('Iconify',args[],'Hello',TRUE)
    easyguiA('GadTools in EasyGUI!',
      [ROWS,
        [TEXT,'Iconify test...',NIL,TRUE,1],
        [SPACE],
        [PLUGIN,0,p,TRUE],
        [SPACE],
        [BUTTON,{toggle_enabled},'Toggle Enabled',p]
      ])
  ELSE
    PrintF('Bad arguments: specify a filename\n')
  ENDIF
EXCEPT DO
  END p
  report_exception()
ENDPROC

PROC toggle_enabled(p:PTR TO iconify,i)
  p.setdisabled(p.disabled=FALSE)
ENDPROC
