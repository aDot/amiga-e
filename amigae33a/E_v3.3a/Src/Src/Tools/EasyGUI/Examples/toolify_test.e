MODULE 'tools/exceptions',
       'tools/easygui',
       'plugins/toolify'

PROC main() HANDLE
  DEF p=NIL:PTR TO toolify
  NEW p.toolify('Toolify','Hello from Toolify',TRUE)
  easyguiA('GadTools in EasyGUI!',
    [ROWS,
      [TEXT,'Toolify test...',NIL,TRUE,1],
      [SPACE],
      [PLUGIN,0,p,TRUE],
      [SPACE],
      [BUTTON,{toggle_enabled},'Toggle Enabled',p]
    ])
EXCEPT DO
  END p
  report_exception()
ENDPROC

PROC toggle_enabled(p:PTR TO toolify,i)
  p.setdisabled(p.disabled=FALSE)
ENDPROC
