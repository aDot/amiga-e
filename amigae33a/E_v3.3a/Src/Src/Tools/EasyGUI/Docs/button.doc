Button PLUGIN
-------------
A flexible button gadget in three flavours: normal (momentary), toggle and
push (sticky).  Uses the 'button.gadget' which needs to be in the normal
place of libs:gadgets/ (usually libs: is also assigned to sys:classes/, and
.gadget files should be stored in the sys:classes/gadgets/ directory).

Constructors:

    button(text,resizex=FALSE,resizey=FALSE,disabled=FALSE)
        text     -> The button's label
        resizex  -> Whether this button can resize in width
        resizey  -> Whether this button can resize in height
        disabled -> Whether this gadget is disabled

    togglebutton(text,selected=FALSE,resizex=FALSE,resizey=FALSE,
                 disabled=FALSE)
        text     -> The button's label
        selected -> Whether this button is selected (on) initially
        resizex  -> Whether this button can resize in width
        resizey  -> Whether this button can resize in height
        disabled -> Whether this gadget is disabled

    pushbutton(text,selected=FALSE,resizex=FALSE,resizey=FALSE,
               disabled=FALSE)
        text     -> The button's label
        selected -> Whether this button is selected (on) initially
        resizex  -> Whether this button can resize in width
        resizey  -> Whether this button can resize in height
        disabled -> Whether this gadget is disabled

Destructor:

    END *must* be called for each NEWed object.

Data (should be considered read-only):

    OBJECT button OF plugin
      selected  -> Current state of the button (=0 is off, <>0 is on)
      disabled  -> Disabled or enabled
    PRIVATE ...
    ENDOBJECT

    The selected element is useful only for toggle and push buttons.

New methods:

    setselected(selected=TRUE) -> Set the button on or off
    settext(text)              -> Change the button label
    setdisabled(disabled=TRUE) -> Disable/enable the gadget

    Note: setselected() is ignored for normal buttons.
    Note: push buttons cannot be deselected by the user (they're sticky),
          so use setselected() to deselect them when necessary.
    Note: settext() does not check that the new label is not too wide...

Action functions:

    Your action function will be called (or your action value returned by
    easygui()) in the following circumstances:
      o When the button is pressed to select or deselect it.

Exceptions:

    "butt" will be raised by the constructor if the .gadget file can't be
           opened.
    "butt" will be raised by the render() method if the gadget can't be
           created.
