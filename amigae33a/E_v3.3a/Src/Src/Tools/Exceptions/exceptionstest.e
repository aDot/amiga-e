MODULE 'tools/exceptions'

PROC main()
  t(0)		-> NOP
  t(10)
  t("MEM")
  t("OPEN")
  t("^C")
  t("BL")
  t("BLA")
  t("BLAA")
ENDPROC

PROC t(x) HANDLE
  Throw(x,'bla')
EXCEPT
  report_exception()
ENDPROC
