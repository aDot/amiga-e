-> test screen cloning

OPT OSVERSION=37

MODULE 'tools/clonescreen'

PROC main() HANDLE
  DEF screen=NIL,font=NIL,win=NIL,xsize,ysize,depth
  screen,font:=openclonescreen('Workbench','My Cloned Workbench')
  win:=backdropwindow(screen)
  depth,xsize,ysize:=getcloneinfo(screen)
  EasyRequestArgs(win,[20,0,'On My Own Screen + Backdrop Window!',
                            'Screen Dimensions Are \dx\dx\d',
                            'Continue'],0,[xsize,ysize,depth])
EXCEPT DO
  closeclonescreen(screen,font,win)
  SELECT exception
    CASE "SCR"; WriteF('no screen!\n')
    CASE "WIN"; WriteF('no window!\n')
  ENDSELECT
ENDPROC
