-> list Tools!

OPT MODULE
OPT EXPORT

/*------some-typical-lisp-functions--------*/

/* note: these are quite inefficient functions and could be done
   much faster using destructive implementations. They are however
   very nice as LISP-programming examples */

-> appends two lists

PROC append(x,y)
  DEF h,t
  IF x
    x <=> <h|t>
    RETURN <h|append(t,y)>
  ENDIF
ENDPROC y

-> 'naive'-reverses a list. notorious for it's inefficiency.

PROC nrev(x)
  DEF h,t
  IF x
    x <=> <h|t>
    RETURN append(nrev(t),<h>)
  ENDIF
ENDPROC NIL

-> returns a list of results from applying fun to elements of l

PROC map(l,fun)
  DEF h,t
  IF l
    l <=> <h|t>
    RETURN <fun(h)|map(t,fun)>
  ENDIF
ENDPROC NIL

-> returns a list of elements in l for which fun returns true

PROC filter(l,fun)
  DEF h,t,r
  IF l
    l <=> <h|t>
    r:=filter(t,fun)
    RETURN IF fun(h) THEN <h|r> ELSE r
  ENDIF
ENDPROC NIL

-> returns two lists of elements in l for which fun returns true and false

PROC partition(l,fun)
  DEF h,t,rt,rf
  IF l
    l <=> <h|t>
    rt,rf:=partition(t,fun)
    IF fun(h) THEN RETURN <h|rt>,rf ELSE RETURN rt,<h|rf>
  ENDIF
ENDPROC NIL,NIL

-> folds function through list, i.e.:
-> foldr(<1,2,3>,{add},0) = add(1,add(2,add(3,0)))

PROC foldr(l,fun,end)
  DEF h,t
  IF l
    l <=> <h|t>
    RETURN fun(h,foldr(t,fun,end))
  ENDIF
ENDPROC end

-> zip combines two lists into one list of pairs.

PROC zip(x,y)
  DEF a,b,c,d
  IF x
    IF y
      x <=> <a|b>
      y <=> <c|d>
      RETURN <<a|c>|zip(b,d)>
    ENDIF
  ENDIF
ENDPROC NIL

-> length of a list

PROC length(x) IS IF x THEN length(Cdr(x))+1 ELSE 0


/*--------universal-cell-printing---------*/

/* prints any cell structure in memory, proc(v) is called whenever
   a value is not a cell. below predefined functions for lists/trees
   of ints and strings    */

EXPORT PROC showcell(cell,proc)
  DEF a,c
  IF cell
    IF Cell(cell)
      WriteF('<')
      cell <=> <a|c>
      showcell(a,proc)
      IF c
        WHILE Cell(c) AND (c<>0)
          WriteF(',')
          c <=> <a|c>
          showcell(a,proc)
        ENDWHILE
        IF c
          WriteF('|')
          showcell(c,proc)
        ENDIF
        WriteF('>')
      ELSE
        WriteF('>')
      ENDIF
    ELSE
      proc(cell)
    ENDIF
  ELSE
    WriteF('<>')
  ENDIF
ENDPROC

PROC showcellint(cell) IS showcell(cell,{showint})
PROC showcellstr(cell) IS showcell(cell,{showstr})

PROC showint(x) IS WriteF('\d',x)
PROC showstr(x) IS WriteF('\s',x)

