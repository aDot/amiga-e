/* trapguru.m

   installs a trap-handler in your task-structure, to catch
   processor exceptions like zero-divide etc.
   When handler gets invoked, it will throw an E-exception
   "GURU" with alertnum as info.

   I don't guarantee this is 100% reliable for all CPU's etc.

*/


OPT MODULE

MODULE 'exec/tasks'

EXPORT PROC trapguru()
  DEF mytask:PTR TO tc
  mytask:=FindTask(NIL)
  mytask.trapcode:={tcode}
  LEA store(PC),A0
  MOVE.L A4,(A0)
ENDPROC

tcode:
  MOVE.L (A7)+,D0
  LEA store(PC),A0
  MOVE.L D0,4(A0)
  MOVEQ #3,D1
  CMP.L D1,D0
  BGT.S noadjust
  MOVE.L $4.W,A6
  BTST #0,297(A6)
  BNE.S noadjust
  ADDQ.L #8,A7
noadjust:
  LEA continue(PC),A0
  MOVE.L A0,2(A7)
  RTE
continue:
  LEA store(PC),A0
  MOVE.L (A0),A4
  Throw("GURU",Long({store}+4))
store:
  LONG 0,0
