/* shows ho to catch processor exceptions from own programs
   I don't guarantee what the module trapguru.m does is 100% ok
   for all CPU's    */

MODULE 'tools/trapguru'

PROC main()
  DEF a
  trapguru()                       -> install trap handler
  FOR a:=1 TO 10 DO bla(a)
ENDPROC

PROC bla(x) HANDLE
  DEF a=0
  a:=a/a             -> cause processor exception
EXCEPT
  IF exception="GURU"
    WriteF('guru \d occured: $\z\h[8]\n',x,exceptioninfo)
  ENDIF
ENDPROC
