Short: E versions of RKRM examples (Part One and Two)
Author: m88jrh@ecs.ox.ac.uk (Jason R. Hulance)

JRH's RKRM Examples (Part One and Two)
======================================

This archive contains translations of all the examples from the following
chapters of the RKRM (Libraries):  ASL, Commodities, Exec, GadTools,
Intuition, Preferences, Workbench, Expansion, Graphics (all the corresponding
chapters), IFFParse, Keymap, Math (minus the FFP examples) and Utility.  Also
included are translations of all the Resource, Clipboard and Console examples
from the RKRM (Devices).

The examples retain most of the original comments, with comments specific to
these E versions beginning "-> E-Note:".  A lot of effort has been put into
showing how to effectively use E features such as exceptions and lists.
A number of bugs in the original examples have also been eliminated.

If you are new to E you should pay special attention to the effect of using
exceptions: error handling is and clean-up is neatly separated from the main
code, and the code is generally much less indented.  You should also notice
the special care taken to initialise variables appropriately so that
clean-up is made much more simple in the handlers.

If you wish to recompile the sources (I expect you might...), you will need
my Typed Modules (V40), my AmigaLib modules and my Useful modules.  These
archives should all be available where you found this archive (Aminet? In
'dev/e'?). [note: these are all included in the 'Modules' directory of
this distribution -wouter]

These executables, E sources and E modules are all Copyright (C) 1995,
Jason R. Hulance.
The original RKRM examples are Copyright (C) 1992, Commodore-Amiga Inc.

You are free to use the files in this archive to help create your own
programs (whether they are freeware or commercial), but if you wish to
distribute any part of this archive you must include it all, unmodified,
and with this file.

(Hopefully, there will be a Part Three which will contain the remainder of the
RKRM examples.)
