OPT MODULE

MODULE  'oomodules/library'

OBJECT exec OF library
ENDOBJECT

PROC name() OF exec IS 'Exec'

PROC open(no_1=NIL,no_2=NIL,no_3=NIL) OF exec IS NIL

PROC close() OF exec IS NIL

PROC end() OF exec IS NIL
