OPT MODULE

MODULE  'oomodules/object',

        'exec/ports',
        'exec/nodes',

        'gadtools'

EXPORT OBJECT port OF object
/****** object/port ******************************

    NAME
        port of object -- Message port object.

    FUNCTION

    ATTRIBUTES
        mp:PTR TO mp -- Pointer to message port. Ready to use after new().

        lastMessage:PTR TO mn -- Pointer to last message received. The usual
            restrictions about messages apply here, too -- don't access it
            when you've replied.

    SEE ALSO
        object, exec.library system documentation

********/
  mp:PTR TO mp
  lastMessage:PTR TO mn
ENDOBJECT

PROC init() OF port
/****** port/init ******************************

    NAME
        init() of port -- Initialization of the object.

    SYNOPSIS
        port.init()

    FUNCTION
        Creates an unnamed message port. Note that the name can be
        set with the "name" tag in select().

    EXCEPTIONS
        Raises "port", text when creation failed.

    EXAMPLE

       /*
        * Add a port named 'testport' to the system
        */

        NEW port.new(["name", 'testport', "add"])



    SEE ALSO
        port

********/

  self.mp := CreateMsgPort()

  IF self.mp = NIL THEN Throw("port",'Unable to create message port.')

ENDPROC

PROC select(opts,i) OF port
/****** port/select ******************************

    NAME
        select() of port -- Selection of action upon initialization.

    SYNOPSIS
        port.select(LONG, LONG)

        port.select(opts, i)

    FUNCTION
        Recognized tags are:
            "name" -- set port's name
            "add" -- add the port to the system

    INPUTS
        opts:LONG -- List of options

        i:LONG -- Index of option list

    SEE ALSO
        port

********/
DEF item

  item:=ListItem(opts,i)


  SELECT item

    CASE "name"

      INC i
      self.mp::ln.name := ListItem(opts,i)

    CASE "add"

      self.addToSystem()

  ENDSELECT

ENDPROC i

PROC getSignalMask() OF port IS Shl(1,self.mp.sigbit)
/****** port/getSignalMask ******************************

    NAME
        getSignalMask() of port -- Get signal mask for Wait()

    SYNOPSIS
        port.getSignalMask()

    FUNCTION
        Gets the signal mask from an existing port. This mask is used
        in Wait().

    RESULT
        LONG -- Signal mask of the port.

    EXAMPLE

       /*
        * Wait for a message to arrive
        */

        mask := port.getSignalMask()
        port.waitForSignalMask()

    SEE ALSO
        port, exec/Wait(), waitForSignalMask()

********/

PROC getMsg(mode="exec") OF port
/****** port/getMsg ******************************

    NAME
        getMsg() of port -- Get message from port.

    SYNOPSIS
        port.getMsg(LONG="exec")

        port.getMsg(mode)

    FUNCTION
        Get a message from the port. This message can either be one of
        exec's or a gadtools message, the only argument sets this mode.

    INPUTS
        mode:LONG -- "exec" for exec, "gadt" for gadtools message. Default
            is "exec"

    RESULT
        PTR TO mn -- Message pointer or NIL if no message was at the port.

    EXAMPLE

       /*
        * Wait for a message to arrive at exec's port.
        */

        mask := port.getSignalMask()
        IF (port.waitForSignalMask() AND mask)

         /*
          * Get it.
          */

          message := port.getMsg()

        ENDIF

    SEE ALSO
        port, getSignalMask(), exec/Wait()

********/

  SELECT mode

    CASE "gadt"

      IF gadtoolsbase

        self.lastMessage := Gt_GetIMsg(self.mp)
        RETURN self.lastMessage

      ENDIF

    DEFAULT

      self.lastMessage := GetMsg(self.mp)
      RETURN self.lastMessage

  ENDSELECT

ENDPROC

PROC replyMsg(mode="exec") OF port
/****** port/replyMsg ******************************

    NAME
        replyMsg() of port -- Reply a message.

    SYNOPSIS
        port.replyMsg(LONG="exec")

        port.replyMsg(mode)

    FUNCTION
        Reply a message that was received at the port. For multi-tasking
        reasons this has to be done as soon as possible after receiving it.

    INPUTS
        mode:LONG -- "exec" for exec message or "gadt" for gadtools.

    EXAMPLE

       /*
        * Wait for a message to arrive at exec's port.
        */

        mask := port.getSignalMask()
        IF (port.waitForSignalMask() AND mask)



         /*
          * Get it.
          */

          message := port.getMsg()



         /*
          * Copy the important data so we can reply at once.
          */

          port.replyMsg()

        ENDIF


    SEE ALSO
        port, exec/Wait(), exec.library system documentation about messages

********/

  IF self.lastMessage = NIL THEN RETURN

  SELECT mode

    CASE "gadt"

      IF gadtoolsbase THEN Gt_ReplyIMsg(self.lastMessage)

    DEFAULT

      ReplyMsg(self.lastMessage)

  ENDSELECT

ENDPROC

PROC waitForSignalMask(mask) OF port IS Wait(mask)
/****** port/waitForSignalMask ******************************

    NAME
        waitForSignalMask() of port -- Wait for signal mask.

    SYNOPSIS
        port.waitForSignalMask(LONG)

        port.waitForSignalMask(mask)

    FUNCTION
        Wait until a signal according to the mask is sent to the port.

    INPUTS
        mask:LONG -- Mask to wait for. The should be the one from
            getSignalMask().

    RESULT
        LONG -- Signal mask received.

    SEE ALSO
        port, getSignalMask()

********/

PROC wait() OF port IS Wait(self.getSignalMask())
/****** port/wait ******************************

    NAME
        wait() of port -- Wait for a message to arrive.

    SYNOPSIS
        port.wait()

    FUNCTION
        Waits until a message arrives at the port. Use this function when
        there is only one port in your program.

    RESULT
        LONG -- Signal mask received.

    NOTES
        See portList for waiting on any number of ports.

    SEE ALSO
        port, portList

********/

PROC end() OF port
/****** port/end ******************************

    NAME
        end() of port -- Global destructor.

    SYNOPSIS
        port.end()

    FUNCTION
        Removes the message port from the system and deletes it. Be sure you
        have received all your messages.

    SEE ALSO
        port

********/

  IF self.mp = NIL THEN RETURN

  self.removeFromSystem()
  DeleteMsgPort(self.mp)

ENDPROC

PROC addToSystem() OF port
/****** port/addToSystem ******************************

    NAME
        addToSystem() of port -- Add port to the system.

    SYNOPSIS
        port.addToSystem()

    FUNCTION
        Adds the port to the system, i. e. you can receive messages from
        other programs if they know the name of your port. Provide the
        "add" tag when newing the port object and it will be added when
        it's initialized.

    SEE ALSO
        port

********/

  AddPort(self.mp)

ENDPROC

PROC removeFromSystem() OF port
/****** port/removeFromSystem ******************************

    NAME
        removeFromSystem() of port -- Remove port.

    SYNOPSIS
        port.removeFromSystem()

    FUNCTION
        Removes the port from the system. No other task will be able to send
        you messages any more. Called in END automatically, DO NOT call it
        unless you know what you do.

    SEE ALSO
        port

********/

  RemPort(self.mp)

ENDPROC

EXPORT PROC wrapPort(p:PTR TO mp)
/****** port/wrapPort ******************************

    NAME
        wrapPort() -- Wrap a port system structure in an object.

    SYNOPSIS
        wrapPort(PTR TO mp)

        wrapPort(p)

    FUNCTION
        Wraps the system structure port in an object of that type. When
        you have a port from, say, a window this can be quite useful. The
        window port can then be used as if it was a normal port object.

    INPUTS
        p:PTR TO mp -- Port to wrap.

    RESULT
        PTR TO port -- Port object that was just created.

    SEE ALSO
        portList

********/
DEF port:PTR TO port

  NEW port.new()
  DeleteMsgPort(port.mp)
  port.mp := p

  RETURN port

ENDPROC

