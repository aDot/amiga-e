/*

Library template for any library. This object can be used to open other
libraries. See it as an example how to build own library derived objects.

*/

OPT MODULE



/*

Export everything defined in here.

*/
OPT EXPORT



/*

Include the basic library object and normal E modules of the library to
implement.

*/

MODULE 'oomodules/library', 'anylibrary', 'libraries/anylibrary'



OBJECT anylibrary OF library
/****** anylibrary/--background-- ******************************************

    NAME 
        anylibrary of library

    PURPOSE
        Basic implementation of a simple library module.

    ATTRIBUTES
        No special attributes since it's used for demonstration.

    CREATION
        September 9 1996 Gregor Goldbach

******************************************************************************

History


*/
ENDOBJECT



PROC init() OF anylibrary
/****** anylibrary/init ******************************************

    NAME 
        init() -- Initialization of the object.

    SYNOPSIS
        anylibrary.init()

    FUNCTION
        Sets the library's name and the version to 0. After that the library
        is opened.

        This menas that any version found be opened if the version isn't
        changed in select().

    SEE ALSO
        open()
******************************************************************************

The object attributes identifier and version are set to initial values.
The version attribute may very well be changed by the "vers" tag of select()
if you want to open a specific version of the library.

*/
 self.identifier:='anylibrary.library'
 self.version:=0
 self.open()
ENDPROC



PROC open() OF anylibrary
/****** anylibrary/open ******************************************

    NAME 
        open() -- Open anylibrary.library

    SYNOPSIS
        anylibrary.open()

    FUNCTION
        Opens the anylibrary.library and sets the global library base to
        the value returned by OpenLibrary().

    EXCEPTIONS
        "lib",{anylibOpen} will be raised if the opening fails. You should
        provide at least one exception handler to receive the exception and
        act on it.

******************************************************************************

History


*/
 IF (anylibrarybase:=OpenLibrary(self.identifier,self.version)) = NIL THEN Throw("lib",{reqtoolOpen})
ENDPROC


PROC close() OF anylibrary
/****** anylibrary/close ******************************************

    NAME 
        close() -- Close the library.

    SYNOPSIS
        anylibrary.close()

    FUNCTION
        Closes the library.

    SEE ALSO
        open()
******************************************************************************

History


*/
  CloseLibrary(anylibrarybase)
ENDPROC


anylibOpen:
 CHAR 'Unable to open anylibrary.library.',0
