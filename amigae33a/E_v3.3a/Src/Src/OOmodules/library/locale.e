OPT MODULE

MODULE  'oomodules/library',
        'oomodules/object',
        'oomodules/sort/string',

        'locale',
        'libraries/locale'

EXPORT OBJECT locale OF library
/****** library/locale ******************************

    NAME
        locale of library

    PURPOSE
        Object wrapper for the locale.library

    ATTRIBUTES
        catalog:LONG -- Catalog as returned from OpenCatalog()

        builtinLanguage:LONG -- String that says which language we want.
            NIL is for english.

    SEE ALSO
        library

********/
  catalog -> the current catalog
  builtinLanguage -> if NIL it's english
ENDOBJECT

EXPORT PROC init() OF locale
/****** locale/init ******************************

    NAME
        init() of locale -- Initilization of the object.

    SYNOPSIS
        locale.init()

    FUNCTION
        Opens any version of the library.

    SEE ALSO
        locale

********/

  self.version:=0
  self.open()

ENDPROC

PROC open() OF locale
/****** locale/open ******************************

    NAME
        open() of locale -- Open locale.

    SYNOPSIS
        locale.open()

    FUNCTION
        Opens the locale.library and throws an exceptions if opening failed.

    SEE ALSO
        locale, library/open

********/

  IF localebase THEN RETURN

  IF (localebase := OpenLibrary('locale.library', self.version))=NIL THEN Throw("lib", 'Unable to open locale.library')

ENDPROC

PROC close() OF locale
/****** locale/close ******************************

    NAME
        close() of locale -- Close locale.library

    SYNOPSIS
        locale.close()

    FUNCTION
        Closes the library if it was open.

    SEE ALSO
        locale, library/close

********/

  IF localebase THEN CloseLibrary(localebase)

ENDPROC

PROC openCatalog(locale, name:PTR TO CHAR, builtinLanguage=NIL:PTR TO CHAR) OF locale
/****** locale/openCatalog ******************************

    NAME
        openCatalog() of locale -- Open catalog.

    SYNOPSIS
        locale.openCatalog(LONG, PTR TO CHAR, PTR TO CHAR=NIL:PTR TO CHAR)

        locale.openCatalog(locale, name, builtinLanguage)

    FUNCTION
        Opens the specified catalog. If the catalog of this object is open
        when you call this function it is closed first.

    INPUTS
        locale:LONG -- Pointer to locale structure, normally left NIL.

        name:PTR TO CHAR -- Name o catalog to open.

        builtinLanguage:PTR TO CHAR -- 'Native' language of your program.
            Used if the desired catalog couldn't be opened. If NIL, your
            program speaks english.

    RESULT
        LONG -- Opended catalog or NIL if opening failed.

    EXAMPLE
       /*
        * Try to open term's catalog.
        */

        NEW locale.new(["ctlg", 'term.catalog'])



       /*
        * Success?
        */

        IF locale.catalog

         /*
          * do anything with it
          */

        ENDIF



       /*
        * Try to open the Prefs catalog. If term.catalog is still open,
        * it will be closed first.
        */

        locale.openCatalog(NIL, 'prefs.catalog', NIL)

       /*
        * do anything with it
        */

    SEE ALSO
        locale

********/

  IF self.catalog -> if there's a catalog open

    self.closeCatalog(self.catalog)

    self.catalog := NIL

  ENDIF

  self.builtinLanguage := builtinLanguage

  self.catalog := OpenCatalogA(locale,name, IF self.builtinLanguage=NIL THEN 'english' ELSE self.builtinLanguage)

  RETURN self.catalog

ENDPROC

PROC closeCatalog(catalog) OF locale
/****** locale/closeCatalog ******************************

    NAME
        closeCatalog() of locale -- Close catalog.

    SYNOPSIS
        locale.closeCatalog(LONG)

        locale.closeCatalog(catalog)

    FUNCTION
        Closes a catalog that was opened with openCatalog().

    INPUTS
        catalog:LONG -- Catalog to close.

    SEE ALSO
        locale

********/

  IF catalog THEN CloseCatalog(catalog)

ENDPROC

PROC getString(number, default=NIL:PTR TO CHAR) OF locale
/****** locale/getString ******************************

    NAME
        getString() of locale -- Get string from open catalog.

    SYNOPSIS
        locale.getString(LONG, PTR TO CHAR=NIL:PTR TO CHAR)

        locale.getString(number, default)

    FUNCTION
        Gets a string from an opened catalog and returns it. If it fails,
        the passed default string will be returned.

    INPUTS
        number:LONG -- Number of string to get from the catalog.

        default:PTR TO CHAR -- Default string to return on failure.

    RESULT
        PTR TO CHAR -- string from catalog or default string.
        LONG -- "NoCa" if no catalog is open.

    SEE ALSO
        locale

********/
-> gets a string from the current catalog
-> if no catalog is open returns "NoCa" (no catalog)

  IF self.catalog THEN RETURN GetCatalogStr(self.catalog,number,default) ELSE RETURN "NoCa"

ENDPROC

PROC end() OF locale
/****** locale/end ******************************

    NAME
        end() of locale -- Global destructor.

    SYNOPSIS
        locale.end()

    FUNCTION
        Closes open catalog and locale.library.

    SEE ALSO
        locale

********/

  self.closeCatalog(self.catalog)

  self.close()

ENDPROC

PROC select(optionlist, index) OF locale
/****** locale/select ******************************

    NAME
        select() of locale -- Selection of action upon initialisation.

    SYNOPSIS
        locale.select(LONG, LONG)

        locale.select(optionlist, index)

    FUNCTION
        The following tags are recognized:
            "ctlg" -- Name of catalog to open. The catalog will be opened
                at once via openCatalog().
            "lang" -- Sets the builtin language. Any string or NIL for
                'english'

    INPUTS
        optionlist:LONG -- List of options.

        index:LONG -- Index of optionlist.

    SEE ALSO
        locale

********/
DEF item

  item:=ListItem(optionlist,index)

  SELECT item

    CASE "ctlg" -> open catalog

      INC index
      self.openCatalog(NIL, ListItem(optionlist,index), self.builtinLanguage)

    CASE "lang" -> set builtin language

      INC index
      self.builtinLanguage := ListItem(optionlist,index)

  ENDSELECT

ENDPROC index

PROC getObjectString(object:PTR TO object, container:PTR TO string,number, default=NIL:PTR TO CHAR) OF locale
/****** locale/getObjectString ******************************************

    NAME
        getLocalizedObjectString() -- Get string from object catalog

    SYNOPSIS
        locale.getObjectString(PTR TO object, PTR TO string, LONG, PTR TO CHAR=NIL:PTR TO CHAR)

        locale.getObjectString(object, container, number, default)

    FUNCTION
        Gets a string from the object's catalog.

    INPUTS
        object:PTR TO object -- Object to get the string for

        container:PTR TO string -- String object to store the string in

        number:LONG -- Number of string to get

        default=NIL:PTR TOF CHAR -- The default string to return if there is no string
            of that number.

    RESULT
        "NoCa" if the locale.library couldn't be opened.
        The default string if there's no entry of that number in the catalog,
        otherwise the requested string.

    NOTES
        The returned string from the catalog is READ-ONLY.

******************************************************************************/

DEF string:PTR TO string,
    oldCatalog,
    stringToReturn:PTR TO CHAR

  END container
  NEW container.new()

  NEW string.new()
  string.cat('oomodules/')
  string.cat(object.name())
  string.cat('.catalog')

 /*
  * 'save' the old catalog
  */

  oldCatalog := self.catalog

 /*
  * Set it to NIL so self.openCatalog() won't close it
  */

  self.catalog:=NIL

 /*
  * Open the object's catalog
  */

  self.openCatalog(NIL, string.write(), self.builtinLanguage)

  stringToReturn :=  self.getString(number,default)
  container.cat(stringToReturn)

  self.closeCatalog(self.catalog)

 /*
  * Restore the old catalog
  */

  self.catalog := oldCatalog

  END string

ENDPROC


PROC name() OF locale IS 'Locale'
