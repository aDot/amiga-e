/*

Example program for the catalogList object.

*/



MODULE  'oomodules/library/locale/catalogList'

PROC main()
DEF cl:PTR TO catalogList, index

 /*
  * Open the catalogs with builtin language english
  */

  NEW cl.new(["ctlg", 'term.catalog', "ctlg", 'oomodules/object.catalog'])
  WriteF('number of catalogs open: \d\n', cl.length())



 /*
  * Set the current catalog. If the catalog attribute is non-NIL the
  * setting was successful.
  */

  cl.setCurrentCatalog(NIL,'term.catalog')
  IF cl.catalog

    WriteF('\s\n\s\n\s\n', cl.getString(1,'bla'),
           cl.getString(2,'bla'),
           cl.getString(3,'bla'))

  ENDIF



 /*
  * Switch to the other catalog. Note that if we hadn't opened the
  * object.catalog yet this proc would open it now.
  */

  cl.setCurrentCatalog(NIL,'oomodules/object.catalog')
  IF cl.catalog

    WriteF('\s\n\s\n\s\n', cl.getString(1,'bla'),
           cl.getString(2,'bla'),
           cl.getString(3,'bla'))

  ENDIF



 /*
  * Switch back to term
  */

  cl.setCurrentCatalog(NIL,'term.catalog')
  IF cl.catalog

    WriteF('\s\n\s\n\s\n', cl.getString(1,'bla'),
           cl.getString(2,'bla'),
           cl.getString(3,'bla'))

  ENDIF



  END cl

ENDPROC
