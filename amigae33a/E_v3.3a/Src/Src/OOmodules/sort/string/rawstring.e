OPT MODULE
OPT EXPORT

-> This module is for handling blocks of memory in general.
-> The quirky thing about this is that it can compare
-> 'string' objects as well as rawstring.

MODULE 'oomodules/sort/string'

OBJECT rawstring OF string
ENDOBJECT

