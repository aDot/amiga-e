/*

Object template.

Replace 'new_object' with your object name and 'parent_object' with the
object name you inherit from.

You definitely have to modify select(), init() and end(). The methods
open( and close() are just for your convenience, you may delete them if you
don't need them.

First template version

October 31 1996 Gregor Goldbach

*/

OPT MODULE

MODULE  'oomodules/'

EXPORT OBJECT new_object OF parent_object
/****** library/new_object ******************************

    NAME
        new_object() of parent_object --

    SYNOPSIS
        parent_object.new_object()

    PURPOSE
        Describe what the object's purpose is.

    ATTRIBUTES
        List the attributes with their type and a description. If a default
        value is set in init() you should write this down.

        attribute:type -- description

    SEE ALSO
        parent_object

********/
ENDOBJECT






EXPORT PROC init() OF new_object
/****** new_object/init ******************************

    NAME
        init() of new_object -- Initialization of the new new_object object
            instance.

    SYNOPSIS
        new_object.init()

    FUNCTION
        Sets attribute foobar to default value xxx.

    RESULT

    EXAMPLE

    CREATION

    HISTORY

    NOTES
        Calls parental object's init() method.

    SEE ALSO
        new_object

********/

 /*
  * Call parental object's init() method if needed.
  */

  SUPER self.init()

ENDPROC






PROC open() OF new_object
/****** new_object/open ******************************

    NAME
        open() of new_object --

    SYNOPSIS
        new_object.open()

    FUNCTION

    RESULT

    EXAMPLE

    CREATION

    HISTORY

    NOTES

    SEE ALSO
        new_object

********/

ENDPROC






PROC close() OF new_object
/****** new_object/close ******************************

    NAME
        close() of new_object --

    SYNOPSIS
        new_object.close()

    FUNCTION

    RESULT

    EXAMPLE

    CREATION

    HISTORY

    NOTES

    SEE ALSO
        new_object

********/

ENDPROC






PROC select(optionlist, index) OF new_object
/****** new_object/close ******************************

    NAME
        select() of new_object -- Selection of action upon initialization.

    SYNOPSIS
        new_object.select()

    FUNCTION
        The following tags are recognized:
            "tag" -- description

    RESULT

    EXAMPLE

    CREATION

    HISTORY

    NOTES
        Calls parental object's select() method.

    SEE ALSO
        new_object

********/

DEF item

  item:=ListItem(optionlist,index)

  SELECT item

    CASE -> "open"

      INC index
      self.your method(ListItem(optionlist,index))

    DEFAULT

     /*
      * Call parental object's select() method if we don't know the tag.
      */

      index := SUPER self.select(optionlist, index)

   ENDSELECT

ENDPROC index






PROC end() OF new_object
/****** new_object/end ******************************

    NAME
        end() of new_object -- Global destructor for object instance.

    SYNOPSIS
        new_object.end()

    FUNCTION

    RESULT

    EXAMPLE

    CREATION

    HISTORY

    NOTES
        Calls parental object's end() method.

    SEE ALSO
        new_object

********/

 /*
  * Call parental object's end() method if needed.
  */

  SUPER self.end()

ENDPROC






PROC name() OF new_object IS -> insert name constant here
