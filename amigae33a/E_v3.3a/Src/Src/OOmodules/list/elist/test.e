/*

Small example to test the elist features.

*/

MODULE  'oomodules/list/elist'

PROC main()
DEF l:PTR TO elist,
    index


 /*
  * Allocate the object and initialize it.
  */

  NEW l.new()



 /*
  * Dump some information on the list. It should have the size of the
  * initial hunk size that is defined as a constant in the elist module.
  */

  WriteF('Created an elist; ')
  WriteF('list at \d, ', l.list)
  WriteF('hunk size is \d\n', l.hunkSize)

  WriteF('list size is \d\n', ListLen(l.list))
  WriteF('count is \d\n', l.itemCount)



 /*
  * Add twenty items and set it to anything (in this case the number of
  * the element. The add() method takes anything and put it in the list.
  */

  WriteF('\nAdding twenty items.\n')
  FOR index := 0 TO 20
    l.add(index)
  ENDFOR



 /*
  * If the hunk size is smaller than twenty the list should be grown now.
  */

  WriteF('list size is \d\n', ListLen(l.list))



 /*
  * Play around with the items. The itemCount attribute points to the next
  * free slot of the list, i.e. when we want to get rid of the last 10
  * elements of the list we can set this attribute to itemCount-10. The
  * items will be overwritten.
  */

  WriteF('\nPut 26 at slot 24.\n')
  l.putAt(26,24)
  WriteF('count is \d\n', l.itemCount)
  l.setNextFreeSlotAt(25)
  WriteF('count is \d\n', l.itemCount)

  WriteF('list size is \d\n', ListLen(l.list))



 /*
  * Dump the contents of the list.
  */

  WriteF('\nDump all items.\n')
  FOR index := 0 TO l.itemCount-1
    WriteF('\d\n', l.getFrom(index))
  ENDFOR



 /*
  * Remove some items.
  */

  WriteF('\nRemove items 10 and 19.\n')
  l.remove(10)
  l.remove(19)

  WriteF('list size is \d\n', ListLen(l.list))
  WriteF('count is \d\n', l.itemCount)



 /*
  * Dump it again to see if removing was successful.
  */

  WriteF('\nDump all items.\n')
  FOR index := 0 TO l.itemCount-1
    WriteF('\d\n', l.getFrom(index))
  ENDFOR


 /*
  * END the object and free allocated resources. This does *not* include
  * the elements in the list. If you collect allocated objects in such a
  * list you would have to call the kill() method to END them as well.
  * This END does only free the list, not it's contents.
  */

  END l

ENDPROC
