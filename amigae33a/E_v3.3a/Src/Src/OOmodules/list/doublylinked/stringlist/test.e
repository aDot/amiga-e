/*

Small test program for the stringList object.

Allocate a stringList, insert some strings alpha-sorted and print it.

*/

MODULE 'oomodules/list/doublylinked/stringlist'

PROC main()

/*
 * These are the vars we need: the stringList object, a var to hold the
 * list we want to have and the index counter for the list.
 */

DEF stringList:PTR TO stringList,
    sortedList,
    index


 /*
  * Allocate and initialize the object
  */

  NEW stringList.new()



 /*
  * Insert some words and get an list. The elist will contain pointers
  * to the alpha-sorted strings.
  */

  stringList.fromList(['fire','walk','with','me'])
  sortedList := stringList.asList()



 /*
  * Dump the list.
  */

  FOR index := 0 TO ListLen(sortedList)-1

    WriteF('\s\n', ListItem(sortedList,index))

  ENDFOR



 /*
  * We have to dispose the sorted list we got from asList() since that
  * method allocated it. E's runtime system would free that memory at the
  * end of the program but it is good style...
  */

  Dispose(sortedList)

ENDPROC
