/*

Sample program for the execlist object. It allocates an execlist and
displays its contents in a Listview gadgets via EasyGUI.

*/

MODULE  'oomodules/list/execlist',
        'tools/easygui'

PROC main() HANDLE

 /*
  * Call the proc that opens a windows with the title 'huhu'
  */

  showList(NIL,'huhu')



 /*
  * If something went wrong we are here
  */

EXCEPT
  WriteF('exception raised\n')
ENDPROC



PROC showList(list, title)
DEF execlist:PTR TO execlist

 /*
  * Allocate the object and set the list two the four entries.
  * Since the "list" tag takes any E list we could use the elist
  * object when we want to have a dynamic list (see there).
  */

  NEW execlist.new(["list", ['1','two','tres','vier']])



 /*
  * Tell EasyGUI to open a window with a listview gadget. A listview
  * gadget takes the entries to display from an exec list - each node
  * of the list is an entry. That exec list can be retrieved from the
  * execlist object be specifying the list attribute.
  */

  easygui(title,
            [EQROWS,
              [LISTV,NIL,NIL,30,10,execlist.list,0,0,0],
              [BUTTON,NIL,'None']
            ])


 /*
  * Be friendly and END the object.
  */

  END execlist

ENDPROC
