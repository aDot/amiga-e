/* Use readargs() to get parameters instead of using the string 'arg'.
   This is more convenient if more than one arg is needed. Uses kick 2.0 */

OPT OSVERSION=37

PROC main()
  DEF myargs:PTR TO LONG,rdargs
  myargs:=[0,0,0]
  IF rdargs:=ReadArgs('UNIT/N,DISK/A,NEW/S',myargs,NIL)
    WriteF('UNIT=\d\n',Long(myargs[0]))      /* integer */
    WriteF('DISK=\s\n',myargs[1])            /* string */
    WriteF('NEW=\d\n',myargs[2])             /* boolean */
    FreeArgs(rdargs)
  ELSE
    WriteF('Bad Args!\n')
  ENDIF
ENDPROC
