/* get command line args. note that `arg' is a predefined E-var */

PROC main()
  WriteF(IF arg[]=0 THEN 'No Args!\n' ELSE 'You wrote: \s\n',arg)
ENDPROC
