/* reading the arguments when started from wb.
   give this program a .info file, and shift-select some icons with it. */

MODULE 'workbench/startup'

PROC main()
  DEF wb:PTR TO wbstartup, args:PTR TO wbarg, a
  IF wbmessage=NIL				/* we got from cli */
    WriteF('args = "\s"\n',arg)
  ELSE						/* from wb */
    wb:=wbmessage
    args:=wb.arglist
    FOR a:=1 TO wb.numargs DO WriteF('wbarg #\d = "\s"\n',a,args[].name++)
  ENDIF
ENDPROC
