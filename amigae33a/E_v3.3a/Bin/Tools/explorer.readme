Short: Amazing System Browser and Debugger
Type: dev/e
Author: jason@fsel.com (Jason R. Hulance)


Explorer 2.2j
=============
Allows you to browse around memory as E objects (which are like C structs).


Basic Function
--------------
You enter an address and then select the object which is (supposed to be?)
at that address.  You can then see the value that each element of the
object takes, and follow pointers etc. by double-clicking on the element (a
single click changes the address to the element's address, which is an
offset from the address you specified for the object).

A double-click may therefore change the object being browsed, so there is a
way of returning to the original object via the "Back Up" button.  As you
double-click and select new objects your choices are recorded.  The current
depth of this path is shown as the 'Depth:' text.  The action of the "Back
Up" button is duplicated by the "Back Up" menu item on the "Selections"
menu.  There is also an option to clear the current path on this menu.

The list of objects may be created in several ways (these are found on the
"Project" menu):

 1) "Open" Opens a previously saved config file, containing any number of
           objects.  By default, when Explorer starts it tries to load
           objects from "explorer.config" in the current directory, and
           then "s:explorer.config" if that fails.  The supplied
           "explorer.config" file contains all the standard Amiga OS
           objects.  (This file was created by using "Load Modules" on the
           "Emodules:" directory and then saving the config using the next
           option.)
 2) "Save" Saves the current list of objects as an Explorer config file.
 3) "Load Modules" Scan the selected module or all the modules in the
           selected directory and its sub-directories.  The found objects
           will replace any current list of objects.  This operation can
           take a long time, especially if you try to load all the modules
           in "Emodules:".  You can interrupt it at any time by closing the
           status window.
 4) "Merge Modules" Same as 3) but any objects found are added to the
           current list of objects rather than replacing them.
 5) "Load Cache" Same as 3) but the current E module cache is scanned.
           This is most useful when Explorer is used to help debug your
           programs.

The address and object can also be specified via ARexx, making Explorer an
extremely useful debugging tool as well as a system browser!  In fact, EDBG
v3.3a+ makes use of Explorer to analyse typed variables.  (A module is
supplied to show how easy it is to integrate the appropriate ARexx call
into your program.)


Object Layout
-------------
The elements of the selected object are listed in order.  If you choose to
see the value of element it will be displayed in hex in parenthesis after
the element, and if you choose to see the element address (an offset from
the object address) it will be displayed after an '@'.  (Use the
"Selections" menu to decide which, if any, you want to see.)


Where to Start
--------------
Explorer starts off with the address of an internal hail and copyright
string.  The items on the "Presets" menu give some other, interesting
starting points:

 1) "Dos"  Views the address stored in the 'dosbase' global variable as a
           'doslibrary' object.  The dos.library is one of the fundamental
           Amiga system libraries, and 'dosbase' is its corresponding
           library base object.
 2) "Exec" Same as 1) but for the 'execbase' variable and object for the
           exec.library (the most fundamental Amiga system library).
 3) "Graphics" Same as 1) but for the 'gfxbase' variable and object for the
           graphics.library.
 4) "Intuition."  Same as 1) but for the 'intuitionbase' variable and
           object for the intuition.library.
 5) "This Process."  Views the 'process' object for the running Explorer
           program!

For a typical Explorer session, you would probably start at one of these
points and then double-click on elements and use the "Back Up" button to
browse around.  You never really need to set the address explicitly on the
GUI (at worst you'd do it by ARexx), but the option is there if you really
want to...


Simple Types
------------
The "CHAR", "INT" and "LONG" buttons view the contents of the address as if
it were a pointer to the corresponding type, and fills in the 'Hex:',
'Decimal:' and 'String:' texts with the value(s) being pointed at.

"BSTR" is similar to "CHAR" but it first converts the address from a BCPL
pointer and then skips over the length byte.  "BADDR" just converts the
address to a BCPL pointer.

"--" and "++" treat the address as if it were pointing to an array of the
type last requested (i.e., an object or simple type).  "--" moves the
address back to the previous element in the array, and "++" moves it on to
the next.  The size of the last requested type is shown as the 'Size:'
text.  (Note: "--" can be activated also by the "p" key and "++" by the "n"
key.)


Display Beeps!
--------------
The screen will flash (or you'll get a beep or whatever your Workbench is
set up to do) if you do one of the following:

 1) Select something when the address is NIL (zero).  A NIL address is not
    valid (although Explorer will behave as if it were).
 2) Select an element which is a NIL pointer or a LONG that is NIL.
 3) Select an element with object or PTR TO object type, where the object
    is not in the current list of objects.
 4) Try to backup past a depth of zero.


Command Line Options
--------------------
The command line template is:

    OBJECT,ADDR,ALL/S,CACHE/S,NONE/S,CONFIG/K,SCREEN/K,NOAUTOREPLY/S

 1) OBJECT Specify an initial object to be displayed (to be useful, the
           appropriate module must also be loaded using 'ALL' or 'CACHE').
 2) ADDR   Specify an initial address to be displayed (NIL is ignored!).
 3) ALL    Load all objects from the modules in Emodules: and its
           sub-directories (overrides 'CACHE' and 'CONFIG').
 4) CACHE  Load all objects from the modules in the current E module cache
           (overrides 'CONFIG').
 5) NONE   Start with no objects (overrides 'ALL', 'CACHE' and 'CONFIG').
 6) CONFIG Specify the config file to load.  If this load fails then
           Explorer will try "s:explorer.config".
 7) SCREEN Specify the public screen to open on.  Normally Explorer will
           open on the default public screen.
 8) NOAUTOREPLY Specifies that the "Auto Reply" option (described below)
           should start as off rather than on.

By default, Explorer will try to load the "explorer.config" in the current
directory (as if you'd used the CONFIG option -- so it will fall back to
trying "s:explorer.config").  The starting address will be that of an
internal hail and copyright string (which will be displayed as the
'String:' text).


ARexx
-----
The "Reply" menu item on the "ARexx" menu manually replies to an ARexx
message that may have been received.  You can also let Explorer
automatically reply to these messages by checking the "Auto Reply" option,
but the manual method is best since you can be sure that the pointers
remain valid while you browse (since the calling program is halted).  Once
you've replied to a message the calling program is free to scribble all
over the memory at the address it just gave you...

A module 'sendexplorer' is supplied to simplify the debugging facilities of
Explorer.  It defines several functions:

1) PROC sendExplorer(addr,obj=NIL,repPort=NIL,message=NIL,quiet=FALSE)

  'addr'   is the address you wish to use.
  'obj'    is the name of the object you are interested in.
  'repPort' is a message port for replies.  If you don't supply this then a
           temporary port is created for the duration of the function call.
           (The purpose of this parameter is to allow you to re-use the
           same port during your program if you call 'sendExplorer' a lot.)
  'message' is a short string to be displayed in the 'Message:' part of
           Explorer.  Use this to communicate some information about the
           address and object (EDBG uses this to give the name, type and
           scope of the variable being explored).
  'quiet'  is a boolean to say whether Explorer should complain if the
           object is not loaded.

The result is TRUE if the message was successfully delivered and did
not cause an ARexx error, and FALSE otherwise.

An example call is:

  sendExplorer(packet, 'dospacket')

To browse using your own objects you'd separate them out into modules and
load these modules in your program.  Compiling your program would add these
to the E module cache, which can then be used to set-up Explorer (using the
"Load Cache" menu item).  Alternatively, you could "Load" or "Merge" the
appropriate module directly, or use a config file if the objects don't
change very often.

2) PROC quitExplorer(repPort=NIL)

  'repPort' is as above.

This sends a 'QUIT' message to Explorer.  Again, it returns TRUE if
everything succeeded.

3) PROC isExplorerRunning()

Returns TRUE if Explorer is running.  It works this out by testing for
the presence of Explorer's ARexx port, so it may be fooled..


Altering Data
-------------
By popular demand, if you hold down the shift key when you click on an
element of an object or on the 'CHAR', 'INT' or 'LONG' buttons, then a
small window pops up which allows you to edit the appropriate value.  It's
not very safe to randomly edit data (especially system data), so use of
this feature is discouraged unless you really know what you're doing...


Example Program
---------------
To show how useful Explorer can be, there is a modified version of the
'empty' handler supplied.  This uses the above call to 'sendExplorer' to
communicate the value of the packet currently being processed.  Since the
handler is not a full process (it's just a task) there would normally be no
simple way of debugging it or examining the packets as they arrive (none of
the DOS functions, such as printing text, are available to simple tasks).
However, the handler can send messages to ARexx and thus to Explorer.

To try it out:

 1) Make sure ARexx is up (by running RexxMast, if necessary).
 2) Start up Explorer using,
      run explorer all
 3) Now copy the 'empty-handler' to L:,
      copy empty-handler L:
 4) Mount it using,
      mount empty: from empty.mountlist
 5) Use the handler,
      type empty:10
 6) Look at Explorer.  It will be displaying the first packet.  Browse
    around it, remembering that most elements will be BCPL strings.
 7) Select "Reply" from the ARexx menu (or press RAmiga-R) to reply to this
    message.
 8) Another packet arrives and the message is sent to Explorer.
 9) Repeat 6)-8) until the 'type' command finishes and you get the Shell
    prompt back.


EDBG
----
EDBG v3.3a+ communicates data about a variable if you hold down the shift
key and double-click on a variable that's within the current scope (EDBG
also adds it to the list of watched variables, if necessary).
